require 'irb/completion'
require 'active_record'
require 'mysql2'
ActiveRecord::Base.establish_connection( :adapter => "mysql2", :host => "localhost", :username => "pi", :password => "secret", :database => "lemur" )

class Group < ActiveRecord::Base
  has_many :admins
end

class User < ActiveRecord::Base
  has_many :admins
  has_many :books, inverse_of: 'author'
  has_many :books, inverse_of: 'patron'
end

class Admin < ActiveRecord::Base
  belongs_to :user
  belongs_to :group
end

class Book < ActiveRecord::Base
  belongs_to :author, class_name: 'User'
  belongs_to :patron, class_name: 'User'
end

# В принципі, це не обов'язково - це лише шаблон для створення. Створювати таблицю можна через mariadb
class CreateBooks < ActiveRecord::Migration[6.0]
  def change
    create_table :books do |t|
      t.datetime   :published_at
      t.string     :book_number
      t.references :author
      t.references :patron
    end
  end
end

admin = Admin.all.first
user = admin.user
book = Book.new
book.patron = user
book.author = user
