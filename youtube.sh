#!/bin/bash
secret=${1} 1>&2
directory="/tmp/youtube-${secret}" 1>&2
#mkdir "${directory}" 1>&2
#cd "${directory}" 1>&2

youtube-dl -x --embed-thumbnail --audio-format mp3 --add-metadata ${secret} 1>&2
file=$(ls -1 | grep $secret | grep mp3) 1>&2
new_file=$(echo $file | sed "s/-${secret}//") 1>&2
mv "$file" "$new_file" 1>&2
echo -n $new_file
