#!/bin/bash
user='pi'
host='localhost'
database='lemur'

text=$(sudo mariadb -N ${database} << EOF
show tables;
EOF
)

for table in ${text}
	do
	if [ ${table} == users ]
		then
			continue
		else
			sudo mariadb ${database} << EOF
				DROP USER IF EXISTS '${user}'@'${host}';
				#DROP DATABASE IF EXISTS ${database};
				DROP TABLE IF EXISTS ${database}.${table};
EOF
	fi
done

#sudo mariadb << EOF
#DROP USER IF EXISTS '${user}'@'${host}';
#DROP DATABASE IF EXISTS ${database};
#EOF
