#!/usr/bin/ruby
def roulette_parse(bot, message)
	disabled = false
#=begin
	if message.from.id != $admin_id and disabled == true
		case message
			when Telegram::Bot::Types::CallbackQuery
				case message.data
					when /^roulette/
						bot.api.answerCallbackQuery(callback_query_id: message.id, text: "Рулетка тимчасово відключена") rescue raise BotSkipMessage
						return false
				end
			when Telegram::Bot::Types::Message
				case message.text
					when /^(\/ruletka|рулетка|hektnrf)/i
						text = "Рулетка тимчасово відключена"
						bot.api.sendMessage(chat_id: message.chat.id, reply_to_message_id: message.message_id, text: text)
				end
				return false
		end
	end
#=end
	case message
		when Telegram::Bot::Types::CallbackQuery
			case message.data
				when /^roulette_log/
					roulette_show_log(bot, message)
				when /^roulette_bet/
					roulette_parse_bet(bot, message)
				when /^roulette_new_game/
					roulette_start(bot, message)
				when /^roulette/
					roulette_parse_callback(bot, message)
					end
		when Telegram::Bot::Types::Message
			case message.text
				when /^(\/ruletka|рулетка|hektnrf)/i
					roulette_start(bot, message)
				when /^(\/go|крутити|го|rhenbnb)$/i
					roulette_go(bot, message)
				when /^(\/log|лог|kju)$/i
					roulette_show_log(bot, message)
			end
			roulette_parse_bet(bot, message)
	end
	roulette_check_timeout(bot, message)
end

def krokodile_parse(bot, message)
	chat_id = get_chat_id(message)
	# if chat_id > 0
	# 	disable = true
	# end
	chat_id > 0 ? disable = true : disable = false
	case message
		when Telegram::Bot::Types::CallbackQuery
			case message.data
				when 'krokodile_show'
					return if disable
					krokodile_callback_show(bot, message)
				when 'krokodile_stop'
					return if disable
					krokodile_callback_stop(bot, message)
				when 'krokodile_restart'
					return if disable
					krokodile_callback_restart(bot, message)
				when 'krokodile_new_word'
					return if disable
					krokodile_callback_new_word(bot, message)
			end
		when Telegram::Bot::Types::Message
			case message.text
				when /^(\/game|крокодил|rhjrjlbk)/i
					return if disable
					krokodile_start(bot, message)
				when /^(\/stop|стоп|cnjg)/i
					return if disable
					krokodile_force_stop(bot, message)
				when /^(\/status|статус|cnfnec)/i
					return if disable
					krokodile_status(bot, message)
			end
			return if disable
			krokodile_check(bot, message)
	end
end

def change_currency_emoji_parse(bot, message)
	chat_id = get_chat_id(message)
	user_id = get_author_id(message)
	# return false unless test_groups(chat_id)
	user = User.create_or_find_by({id: user_id})
	member = get_chat_member(chat_id, user_id)
	level = member.level.id
	min_level = change_chat_currency_emoji_min_level(bot, message)
	if level < min_level
		🍯 = "Щоб змінювати символ валюти, досягніть як мінімум рівня #{min_level}"
		bot.api.sendMessage(chat_id: message.chat.id, reply_markup: delete_kb,  reply_to_message_id: message.message_id, text: 🍯)
		return
	end
	price = change_chat_currency_emoji_price(bot, message)
	if user_enought_carma(user_id, price)
		emoji = new_currency_parse(bot, message)
		if emoji
			🥃 = ["Символ валюти встановлений як \"#{emoji}\"",
						"Ви заплатили #{emoji}<code>#{price}</code>"]
			unless test_groups(chat_id)
				user.carma -= price
				bot.api.sendMessage(chat_id: message.chat.id, reply_markup: delete_kb,  parse_mode: "HTML", reply_to_message_id: message.message_id, text: 🥃.join("\n"))
			else
				bot.api.sendMessage(chat_id: message.chat.id, reply_markup: delete_kb, parse_mode: "HTML", reply_to_message_id: message.message_id, text: 🥃[0])
			end
		end
	else
		🍯 = ["Встановлення нового символу валюти коштує #{symbol_carma(chat_id, user_id)}<code>#{price}</code>.",
					"У вас лише #{symbol_carma(chat_id, user_id)}<code>#{user.carma}</code>"]
		bot.api.sendMessage(chat_id: message.chat.id, reply_markup: delete_kb, parse_mode: "HTML", reply_to_message_id: message.message_id, text: 🍯.join("\n"))
	end
end

def other_parse(bot, message)
	emoji_parse(bot, message)
	case message
		when Telegram::Bot::Types::CallbackQuery
			case message.data
			when 'delete_this_message'
				# bot.api.deleteMessage(chat_id: message.message.chat.id, message_id: message.message.message_id)
				# raise BotSkipMessage
				user = message.from.id
				# if message.message.chat.id > 0
				begin
					bot.api.deleteMessage(chat_id: message.message.chat.id, message_id: message.message.message_id)
					return
				end
				admins = get_admins(bot, message)
				if message.message.reply_to_message.nil?
					replyid = nil
				else
					replyid = message.message.reply_to_message.from.id
				end
				level = get_member_level(get_chat_id(message), message.from.id).to_i
				if admins.include?(user) or user == $admin_id or replyid == user or level > 10
					bot.api.deleteMessage(chat_id: message.message.chat.id, message_id: message.message.message_id)
				else
					respond(bot, message, "Функція тільки для адмінів")
				end
				raise BotSkipMessage
			when 'warship_details'
				text = shoporusni_gentext
				bot.api.editMessageText(chat_id: get_chat_id(message), message_id: get_message_id(message), parse_mode: 'HTML', text: text, reply_markup: delete_kb, disable_web_page_preview: true)
				raise BotSkipMessage
			when 'delete_this_message_tribunal_finish'
				user = message.from.id
				admins = get_admins(bot, message)
				if admins.include?(user) or user == $admin_id
					respond(bot, message, "Повідомлення буде видалено через 10 секунд")
					sleep 10
					bot.api.deleteMessage(chat_id: message.message.chat.id, message_id: message.message.message_id)
				else
					respond(bot, message, "Функція тільки для адмінів")
				end
				raise BotSkipMessage
			# when 'update_ping_message'
			# 	bot_update_ping(bot, message)
			when 'spam_notify_yes'
				changed = false
				chat_id = get_chat_id(message)
				user_id = get_initiator_id(message)
				admin = Admin.where({group_id: chat_id, user_id: user_id, type_mv: 'spamban'}).to_a.first
				if admin.nil?
					new_admin = Admin.create({group_id: chat_id, user_id: user_id, type_mv: 'spamban'})
					changed = true
					new_admin.save
					else
						if admin.enabled
							raise BotSkipMessage
						else
							admin.enabled = true
							admin.save
							changed = true
						end
				end
				if changed
					bot.api.answerCallbackQuery(callback_query_id: message.id, text: "Вас додано в список")
					get_settings(bot, message)
				else
					raise BotSkipMessage
				end
			when 'spam_notify_no'
				chat_id = get_chat_id(message)
				user_id = get_initiator_id(message)
				admin = Admin.where({group_id: chat_id, user_id: user_id, type_mv: 'spamban'}).to_a.first
				if admin.nil?
					raise BotSkipMessage
				end
				if admin.enabled == false
					raise BotSkipMessage
				end
				admin.enabled = false
				admin.save
				bot.api.answerCallbackQuery(callback_query_id: message.id, text: "Вас видалено із списку")
				get_settings(bot, message)
			when /spamer_ban/i
				arr = message.data.split(":")
				ban_spamer(arr[1].to_i, message)
				text = 'Тільки для адмінів. В розробці'
				bot.api.answerCallbackQuery(callback_query_id: message.id, text: text)
			end
	when Telegram::Bot::Types::Message
			case message.text
				when /^\/(help|start)/i
					bot_help(bot, message)
				when /^(\/reset(#{$bot_name})?|ресет|htctn)/i
					user_karma_reset(bot, message)
				when /^(\/id(#{$bot_name})?|ід$)/i
					bot_id(bot, message)
				when /^\/todo/i
					bot_todo(bot, message)
				when /^(\/?bot(#{$bot_name})?|\/?ping(#{$bot_name})?|бот)$/i
					bot_ping(bot, message)
				when /^(\/?echo|ехо$|луна$)/i
					bot_echo(bot, message)
				when /^\/ip/i
					ip_address_parse(bot, message)
				when /(?<=(watch\?v=))[^&$]+/i
					youtube_parse(bot, message)
				when /^(\/rights(#{$bot_name})?|правила|ghfdbkf)/i
					detect_rights(message)
				when /^(\/adv(ertising)?(#{$bot_name})?|реклама|реклама)/i
					detect_adv(message)
				when /^(\/?currency(#{$bot_name})?|валюта)\s+#{EmojiRegex::RGIEmoji}/i
					change_currency_emoji_parse(bot, message)
			end
	end
end

def new_currency_parse(bot, message)
	return false unless message.is_a?(Telegram::Bot::Types::Message)
	return false if message.text.nil?
	chat_id = get_chat_id(message)
	# return false unless test_groups(chat_id)
	user_id = get_author_id(message)
	emoji = emojis(message.text).first
	return false if emoji.nil?
	set_currency_symbol(chat_id, user_id, emoji)
	return emoji
end

def carma_parse(bot, message)
	from = message.from
	begin
	to = message.reply_to_message.from
	rescue
		to = from
		#raise BotSkipMessage
	end
	case message
		when Telegram::Bot::Types::CallbackQuery
			#case message.data
			#
			#end
		when Telegram::Bot::Types::Message
			#dislike = /^(👎|💔|😡|🤬|👿|\-)/i
			dislike = /^(((👎|💔|\-)(\d+)?)$|(мінус|дизлайк|dislike)\)*$)/i
			#like = /^(👍|❤️|😂|🙂|😏|😁|😈|\+)(\d+)/i
			like = /^(((👍|❤|\+)(\d+)?)$|(плюс|дякую|спасибі|лайк|like|thank)\)*$)/i
			case message.text
				#when /^(👍|❤️|😂|🙂|😏|😁|😈|\+)/i
				#when like
				when  like
					set_talan = false
					begin
						#reward = message.text.scan(like)[0][1].to_i
						value = message.text.scan(like)[0][3]
						if value.nil? or value.empty?
							set_talan = true
							reward = 1
						else
							set_talan = false
							reward = value.to_i
						end
					rescue
						set_talan = true
						reward = 1
					end
					user_like(bot, message, reward, set_talan)
				#when dislike
				when dislike
					set_talan = false
					begin
						value = message.text.scan(dislike)[0][3]
						if value.nil? or value.empty?
							set_talan = true
							penalty = 1
						else
							set_talan = false
							penalty = value.to_i
						end
					rescue
						set_talan = true
						penalty = 1
					end
					user_dislike(bot, message, penalty, set_talan)
				when /^(\/?(karma|dolya)|карма$|rfhvf$|доля$|ljkz$|dolya$)/i
				# 	user_karma(bot, message)
				# when /^(тест|test$)/i
					member_karma(bot, message)
				when /^(\/?balance|баланс$|rfhvf$)/i
					user_balance(bot, message)
			end
	end
end

def counter_parse(bot, message)
	if message.is_a?(Telegram::Bot::Types::CallbackQuery)
		return
	end
	# chat_id = get_chat_id(message)
	# user_id = get_author_id(message)
	message_id = get_message_id(message)
	chat = Group.create_or_find_by({id: get_chat_id(message)})
	#update last activity timestamp in chat
	chat.touch
	if chat.last < message_id or chat.last == 0
		chat.last = message_id
	else
		raise BotSkipMessage
	end
	update_user_name(bot, message)
	update_group_counter(bot, message)
	update_user_counter(bot, message)
	#update_group_name(bot, message)
	return false unless message.is_a?(Telegram::Bot::Types::Message)
	case message.text
		when /^(\/?stat|стата$|cnfnf?$)/i
			group = Group.create_or_find_by({id: message.chat.id})
			unless message.reply_to_message.nil?
				user = User.create_or_find_by({id: message.reply_to_message.from.id})
				replyid = message.reply_to_message.message_id
			else
				user = User.create_or_find_by({id: message.from.id})
				replyid = message.message_id
			end
			text = []
			if user.id != $bot_id
				member = get_chat_member(group.id, user.id)
				total = member.messages #ready
				# total = group.messages[user.id]
				# level = get_level(total, group.max_messages)
				level = member.level
				# level = get_level(group.messages[user.id], group.max_messages)
				nextlevel_id = level.id + 1
				# nextlevel_id = 20 if nextlevel_id == 21
				nextlevel = Level.create_or_find_by({id: nextlevel_id})
				#Округлюємо до цілого в більшу сторону методом ceil
				# puts group.max_messages.class
				# puts relation.class
				if nextlevel.id != 21
					relation = level_to_relation(nextlevel)
					destination_count = (group.max_messages * relation).ceil
					text.push("В цій групі: #{symbol_message}<code>[#{total}/#{destination_count}]</code>")
					else
					text.push("В цій групі: #{symbol_message}<code>#{total}</code>")
				end
				user_total = user.messages #ready
				# total = user.messages
				text.push("Всього: #{symbol_message}<code>#{user_total}</code>")
			end
			text.push("Баланс: #{symbol_carma(message.chat.id, message.from.id)}<code>#{user.carma}</code>")
			text.push("Доля: #{symbol_talan}<code>#{user.talan}</code>")
			text.push("Звання: #{level.id} - #{level.name}") if user.id != $bot_id
			if member.max_level != level.id
				max_level = Level.find(member.max_level)
				text.push("Пік: #{max_level.id} - #{max_level.name}") if user.id != $bot_id
			end
			# bot.api.sendMessage(chat_id: message.chat.id, reply_to_message_id: message.message_id, text: "У мене в базі #{total} повідомлень")
			result = Hash.new
			result[:reply_to_message_id] = replyid
			result[:chat_id] = message.chat.id
			result[:parse_mode] = "HTML"
			result[:text] = text.join("\n")
			result[:reply_markup] = delete_kb
			bot.api.sendMessage(result)
		when /\A(\/?top(#{$bot_name})?|топ|njg)\Z/i
			# ######################################################################################################################################################################
			# return
			group = Group.create_or_find_by({id: message.chat.id})
			members = ChatMember.where({chat: message.chat.id}).order(mess_total: :desc).first(20)
			# total = group.messages #ready
			result = members.reject{|x| x.messages == 0} #ready
			# result = total.to_a.sort{ |a, b| b[1] <=> a[1] }.reject{|x| x[1] == 0}
			text = []
			result.each do |x|
				user = User.create_or_find_by({id: x.user})
				# user = User.create_or_find_by({id: x[0]})
				unless user.nil?
					# text.push([user.fullname(bot: bot, message: message), "✉️#{x[1]}"])
					# text.push([user.fullname(bot: bot, message: message), "#{x[1]}"])
					member = get_chat_member(group.id, user.id)
					total = member.messages #ready
					# total = group.messages[user.id]
					# level = get_level(total, group.max_messages)
					level = member.level
					nextlevel_id = level.id + 1
					nextlevel = Level.create_or_find_by({id: nextlevel_id})
					if nextlevel.id != 21
						relation = level_to_relation(nextlevel)
						destination_count = (group.max_messages * relation).ceil
					else
						destination_count = nil
					end
				end
				text.push([x.user, x.messages, destination_count])
			end
			text = text.map do |x|
				member = get_chat_member(group.id, x[0])
				level = member.level
				name = mention(x[0], false)
				score = x[1].to_i
				destination_count = x[2]
				# level = get_level(score, group.max_messages)
				unless destination_count.nil?
					score_desc = "<i>#{level.name}</i> ✉️[<code>#{score}</code>/<code>#{destination_count}</code>]"
				else
					score_desc = "<i>#{level.name}</i> ✉️<code>#{score}</code>"
				end
				[name, score_desc]
			end
			offset = 1
			(0...text.size).each do |x|
				name = "<b>#{x+offset}.</b> #{text[x][0]}"
				score = text[x][1]
				text[x] = [name, score]
			end
			text = text.map{|x| x = x.join(": ")}.join("\n")
			bot.api.sendMessage(chat_id: message.chat.id, parse_mode: "HTML", reply_markup: delete_kb, reply_to_message_id: message.message_id, text: text)
		when /^(((вареники|сало|бюджет|\/?budget)$)|(\/?budget))/i
			# bot.api.sendMessage(chat_id: message.chat.id, reply_to_message_id: message.message_id, text: "Тут буде список багатіїв чату")
			group = Group.create_or_find_by({id: message.chat.id})
			members = get_members(message.chat.id)
			users = members.map do |x|
				x.user
			end
			text = []
			# users.compact!

			users.each do |x|
				if x == 0
					next
				end
				user = User.create_or_find_by({id: x})
				# Костиль
				unless user.carma == 1000
				text.push([x, user.carma])
				end
			end
			text = text.sort{|a,b| b[1] <=> a[1]}.first(20)

			text.map! do |x|
				[mention(x[0], false), x[1]]
			end
			if text == []
				text = "пусто"
			else
				# text = text.sort{|a, b| b[1].to_i <=> a[1].to_i }.reject{|x| x[1] == 0}.first(20)
				text.map! do |x|
					[x[0], "#{symbol_carma(message.chat.id, message.from.id)}<code>#{x[1]}</code>"]
				end
				offset = 1
				(0...text.size).each do |x|
					name = "<b>#{x+offset}.</b> #{text[x][0]}"
					score = text[x][1]
					text[x] = [name, score]
				end
				text = text.map{|x| x = x.join(" : ")}.join("\n")
			end
			bot.api.sendMessage(chat_id: message.chat.id, parse_mode: "HTML", reply_markup: delete_kb, reply_to_message_id: message.message_id, text: text)
	end
end

def emoji_parse(bot, message)
	case message
		when Telegram::Bot::Types::CallbackQuery
			#case message.data
			#
			#end
		when Telegram::Bot::Types::Message
			chat_id = get_chat_id(message)
			user_id = get_author_id(message)
			return false if user_id == false
			level = get_member_level(chat_id, user_id)
			return if level.id > 3
			member = get_chat_member(chat_id, user_id)
			count = emojis(message.text).size
			if count >= 5
				emoji_spam_inform(bot,message)

			end
	end
end
