#!/usr/bin/ruby
def get_user_like_params(member)
	# Один користувач може зробити не більше 7 лайків за 5 хвилин
	limit = 7
	timeout = 300
	return limit, timeout
end

def get_relation_like_params(relation)
	# Один користувач може зробити не більше 3 лайків за 5 хвилин конкретному користувачу
	limit = 3
	timeout = 300
	return limit, timeout
end

def get_user_stole_params(user)
	# Один користувач може обікрасти не більше 5 користувачів за годину (3600 секунд)
	limit = 5
	timeout = 3600
	return limit, timeout
end

def get_member_stole_params(member)
	# Один учасник може красти в чаті не більше 3 користувачів за годину (3600 секунд)
	limit = 3
	timeout = 3600
	return limit, timeout
end

def get_relation_stole_params(relation)
	# Один користувач може обкрадати іншого користувача не частіше, ніж 1 раз за 1 годину
	limit = 1
	timeout = 3600
	return limit, timeout
end

def test_groups(chat_id)
	[-1001312886836, -1001356491818].include?(chat_id)
end

def change_chat_currency_emoji_min_level(bot, message)
	# Користувач може змінювати символ валюти групи, лише якщо в нього рівень 5 і вище
	return 5
end

def change_user_currency_emoji_min_level(bot, message)
	# Користувач може змінювати свій символ валюти, лише якщо в нього рівень 1 і вище
	return 1
end

def change_chat_currency_emoji_price(bot, message)
	# Вартіть зміни символа валюти в групі
	return 5_000
end

def change_user_currency_emoji_price(bot, message)
	# Вартість зміни свого символа валюти
	return 3_000
end

def pair_of_day_last_message_timeout(bot, message)
	# В конкурсі "пара дня" беруть участь лише особи, що писали в чат останні 24 години
	# 24 * 60 * 60
	86400
end

def pair_of_day_min_level(bot, message)
	# В конкурсі "пара дня" беруть участь лише особи, що що досягли рівня 3
	3
end

def pair_of_day_min_level_initiator(bot, message)
	# В конкурсі "пара дня" беруть участь лише особи, що що досягли рівня 3
	5
end

def pair_of_day_timeout(bot, message)
	# Скільки триматиметься пара дня (24 години)
	86400
end

def tribunal_auto_expire(message)
	# Автоматичне закриття суду через 5 хвилин після останнього голосу
	300
end

def wedding_he_price
	15000
end

def wedding_she_price
	25000
end

def wedding_ring_price
	10000
end

def wedding_document_price
	7000
end

def wedding_flowers_price
	10000
end

def vata_branch_price
	14000
end

def ware_price(name, bot = nil, message = nil)
	case name
		when 'wedding he'
			# Костюм нареченого
			return 15000
		when 'wedding_she'
			# Сукня нареченої
			return 25000
		when 'wedding_ring'
			# Весільна обручка
			return 10000
		when 'wedding_document'
			# Свідоцтво про шлюб
			return 7000
		when 'wedding_flowers'
			# Букет квітів
			return 10000
		when 'vata_branch'
			# Гілляка
			return 14000
	end
end

def ware_emoji(name, bot = nil, message = nil)
	case name
		when 'wedding he'
			# Костюм нареченого
			return '🤵🏻'
		when 'wedding_she'
			# Сукня нареченої
			return '👰🏻'
		when 'wedding_ring'
			# Весільна обручка
			return '💍'
		when 'wedding_document'
			# Свідоцтво про шлюб
			return '📃'
		when 'wedding_flowers'
			# Букет квітів
			return '💐'
		when 'vata_branch'
			# Гілляка
			return '🎋'
			return '🌴'
	end
end

def ware_name(bname, bot = nil, message = nil)
	case bname
		when 'wedding_he'
			'костюм нареченого'
		when 'wedding_she'
			'сукня нареченої'
		when 'wedding_ring'
			'весільна обручка'
		when 'wedding_document'
			'незаповнений документ'
		when 'wedding_flowers'
			'весільний букет'
		when 'vata_branch'
			'гілляка'
	end
end

def ware_rname(name, bot = nil, message = nil)
	case name
		when 'wedding_he'
			'костюм нареченого'
		when 'wedding_she'
			'сукню нареченої'
		when 'wedding_ring'
			'весільну обручка'
		when 'wedding_document'
			'незаповнений документ'
		when 'wedding_flowers'
			'весільний букет'
		when 'vata_branch'
			'гілляку'
	end
end

def ware_array
	['wedding_he', 'wedding_she', 'wedding_ring', 'wedding_document', 'wedding_flowers', 'vata_branch']
end
