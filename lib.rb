#!/usr/bin/ruby
class NilClass
	def message_id
		if $DEBUG_MODE
			puts "Запитуємо message_id у nil"
			pp caller.inspect
		end
		raise BotSkipMessage
	end
end



def bot_banned(bot, message)
	case message
		when Telegram::Bot::Types::CallbackQuery
			#case message.data
			#end
			chat_id = get_chat_id(message)
			ruletka = Roulette.create_or_find_by({id: chat_id})
			ruletka.destroy
			puts "Видаляю запис про рулетку"
			krokodile = Krokodile.create_or_find_by({id: chat_id})
			krokodile.destroy
			puts "Видаляю запис про крокодила"
		when Telegram::Bot::Types::Message
			#case message.text
			#end
	end
end

def ban_spamer(user_id, message)
	fwd_mess = bot.api.forwardMessage(chat_id: $admin_id, from_chat_id: message.message.chat.id, message_id: message.message.reply_to_message.message_id)
	fwd_id = get_message_id(fwd_mess)
	chat = bot.api.getChat(chat_id: message.message.chat.id)
	text=[]
	text[1] = "#{mention(message.from)} хоче забанити #{mention(user_id)}"
	if (message.message.chat.id < 0)
		text[0] = "Чат: <a href=\"#{chat['result']['invite_link']}\">#{chat['result']['title']}</a>"
	else
		text[0] = "Чат: пп з <a href=\"tg://user?id=#{message.from.id}\">#{message.from.fullname}</a>"
	end
	bot.api.sendMessage(chat_id: $admin_id, parse_mode: "HTML", reply_to_message_id: fwd_id, text: text.join("\n"))
end

def emoji_spam_inform(bot,message)
	chat_id = get_chat_id(message)
	user_id = get_author_id(message)
	return false if user_id == false
	level = get_member_level(chat_id, user_id)
	return if level.id > 3
	member = get_chat_member(chat_id, user_id)
	count = emojis(message.text).size
	if member.messages < 5
		fwd_mess = bot.api.forwardMessage(chat_id: $admin_id, from_chat_id: message.chat.id, message_id: message.message_id)
		fwd_id = get_message_id(fwd_mess)
		chat = bot.api.getChat(chat_id: message.chat.id)
		text=[]
		text[0] = "Спам від москаля"
		if (message.chat.id < 0)
			if chat['result']['invite_link'] == nil
				text[1] = "Чат: <a href=\"https://t.me/#{chat['result']['username']}\">#{chat['result']['title']}</a>"
			else
				text[1] = "Чат: <a href=\"#{chat['result']['invite_link']}\">#{chat['result']['title']}</a>"
			end
			text[2] = "Юзер: <a href=\"tg://user?id=#{message.from.id}\">#{message.from.fullname}</a>"
		else
			text[1] = "Чат: пп з <a href=\"tg://user?id=#{message.from.id}\">#{message.from.fullname}</a>"
		end
		text=text.join("\n")
		bot.api.sendMessage(chat_id: $admin_id, parse_mode: "HTML", reply_to_message_id: fwd_id, text: text)
		call_spam_admins(message, true)
		# return true if chat_id != -1001009767907
		# rights = '📃'
		# adv = '💰'
		# admin = '👮🏻'
		# kb = [
		# 	[Telegram::Bot::Types::InlineKeyboardButton.new(text: "#{rights}Правила", url: 'https://telegra.ph/Pravila-chatu-Telegram-Ukraine-01-24'),
		# 	 Telegram::Bot::Types::InlineKeyboardButton.new(text: "#{adv}Реклама", url: 'https://telegra.ph/Reklama-v-chat%D1%96-08-25'),
		# 	 Telegram::Bot::Types::InlineKeyboardButton.new(text: "#{admin}Бан", callback_data: "spamer_ban:#{message.from.id}")]
		# ]
		# markup = Telegram::Bot::Types::InlineKeyboardMarkup.new(inline_keyboard: kb)
		# alex = '<a href="tg://user?id=115530526">Алекс</a>'
		# olena = '<a href="tg://user?id=53966589">Олена</a>'
		# stas = '<a href="tg://user?id=52085336">Стас</a>'
		# text = "#{alex} #{olena} #{stas} тут хтось хоче в дупу собі смайлики свої запхати 🌚"
		# bot.api.sendMessage(chat_id: message.chat.id, reply_markup: markup, parse_mode: "HTML", reply_to_message_id: message.message_id, disable_web_page_preview: true, text: text)
		return true
	end
	Lemur.api.sendMessage(chat_id: message.chat.id, parse_mode: "HTML", reply_to_message_id: message.message_id, disable_web_page_preview: true, text: 'Угаманіс чорд')
	fwd_mess = bot.api.forwardMessage(chat_id: $admin_id, from_chat_id: message.chat.id, message_id: message.message_id)
	fwd_id = get_message_id(fwd_mess)
	chat = bot.api.getChat(chat_id: message.chat.id)
	text=[]
	text[0] = "Спам від джури"
	if (message.chat.id < 0)
		if chat['result']['invite_link'] == nil
			text[1] = "Чат: <a href=\"https://t.me/#{chat['result']['username']}\">#{chat['result']['title']}</a>"
		else
			text[1] = "Чат: <a href=\"#{chat['result']['invite_link']}\">#{chat['result']['title']}</a>"
		end
		text[2] = "Юзер: <a href=\"tg://user?id=#{message.from.id}\">#{message.from.fullname}</a>"
	else
		text[1] = "Чат: пп з <a href=\"tg://user?id=#{message.from.id}\">#{message.from.fullname}</a>"
	end
	text=text.join("\n")
	bot.api.sendMessage(chat_id: $admin_id, parse_mode: "HTML", reply_to_message_id: fwd_id, text: text)
end

def dbg(text)
	if $DBGPUTS
		puts text
	end
end

def mention(user, link = true, type = 'html')
	name = nil
	case user
		when Telegram::Bot::Types::User
			id = user.id.to_s
			name = user.fullname unless user.id == $bot_id
		when User
			id = user.id.to_s
			name = user.fullname unless user.id == $bot_id
		when Integer
			id = user.to_s
			db_user = User.find_by({id: user})
			# if db_user.names_arr == ""
			# 	name = id
			# end
			if db_user.nil?
				name = user.to_s
			else
				name = db_user.fullname
			end
	end
	name = "Лемур" if id == $bot_id.to_s
	if link
		return '<a href="tg://user?id=' + id + '">' + name + "</a>"
	else
		return "<b>#{name}</b>"
	end
end

def get_chat_id(message)
	case message
		when Telegram::Bot::Types::CallbackQuery
			group = message.message.chat.id
		when Telegram::Bot::Types::Message
			group = message.chat.id
		when Telegram::Bot::Types::Chat
			group = message.id
		when Hash
			group = message['result']['chat']['id']
		end
	return group
end

def get_author_id(message)
	case message
		when Telegram::Bot::Types::CallbackQuery
			author_id = message.message.from.id
		when Telegram::Bot::Types::Message
			return false if message.from.nil?
			author_id = message.from.id
		when Hash
			author_id = message['result']['from']['id']
	end
	return author_id
end

def get_initiator_id(message)
	case message
		when Telegram::Bot::Types::CallbackQuery
			initiator_id = message.from.id
		when Telegram::Bot::Types::Message
			initiator_id = message.from.id
		when Hash
			initiator_id = message['result']['from']['id']
	end
	return initiator_id
end


def get_message_id(message)
	case message
		when Telegram::Bot::Types::CallbackQuery
			id = message.message.message_id
		when Telegram::Bot::Types::Message
			id = message.message_id
		when Hash
			#id = message['result']['message']['message_id']
			id = message['result']['message_id']
	end
	return id
end

def get_admins(bot,chat)
	chat_id = get_chat_id(chat)
	admins_hash = bot.api.getChatAdministrators(chat_id: chat_id)
	admins = []
	admins_hash['result'].each do |x|
		admins.push(x['user']['id'])
	end
	return admins
end

def get_flooders_show_limit(message)
	today = 5
	yesterday = 3
	return [ today, yesterday ]
end

def get_date(message)
	case message
		when Telegram::Bot::Types::CallbackQuery
			if message.message.edit_date.nil?
				message.message.date
			else
				message.message.edit_date
			end
		when Telegram::Bot::Types::Message
			if message.edit_date.nil?
				message.date
			else
				message.edit_date
			end
		when Hash
			if message['result']['message']['edit_date'].nil?
				message['result']['message']['date']
			else
				message['result']['message']['edit_date']
			end
	end
end

def respond(bot, message, text)
	group = get_chat_id(message)
	message_id = get_message_id(message)
	case message
		when Telegram::Bot::Types::CallbackQuery
			 bot.api.answerCallbackQuery(callback_query_id: message.id, text: text)
		when Telegram::Bot::Types::Message
			garbage = bot.api.sendMessage(chat_id: group, reply_to_message_id: message_id, text: text)
			roulette_add_garbage(garbage)
		when Hash
			garbage = bot.api.sendMessage(chat_id: group, reply_to_message_id: message_id, text: text)
			roulette_add_garbage(garbage)
	end
	return message
rescue => e
	puts "Не можу надіслати відповідь на повідомлення"
	puts e.cause
end

class Telegram::Bot::Types::User
	def fullname
		if self.last_name.nil?
			then
				self.first_name
			else
				self.first_name + " " + self.last_name
		end
	end
end

class Array
	def fullname
		if self[1].nil?
		then
			self[0]
		else
			self[0] + " " + self[1]
		end
	end
end

class Hash
	def fullname
		user = self['result']['user']
		if user['last_name'].nil?
		then
			user['first_name']
		else
			user['first_name'] + " " + user['last_name']
		end
	end
end

class Hash
	def fullname
		user = self['result']['user']
		if user['last_name'].nil?
		then
			user['first_name']
		else
			user['first_name'] + " " + user['last_name']
		end
	end
end

class Telegram::Bot::Types::Chat
	def fullname
		self.title
	end
end

def fullname(bot, user, group)
	user = bot.api.getChatMember(chat_id: group, user_id: user)
	user.fullname
end

def word_parse(bot, message)
	case message
		when Telegram::Bot::Types::CallbackQuery
			# nothing
		when Telegram::Bot::Types::Message
			case message.text
				when /\bхуйло\b/i
					bot.api.sendMessage(chat_id: message.chat.id, reply_to_message_id: message.message_id, text: 'ла-ла-ла-ла-ла-ла-ла-ла')
				when /пут[іи]н/i
					bot.api.sendMessage(chat_id: message.chat.id, reply_to_message_id: message.message_id, text: 'Правильно казати "хуйло"👆🏻')
				when /^хрюкни$/i
					bot.api.sendMessage(chat_id: message.chat.id, reply_to_message_id: message.message_id, text: 'слава расії')
				when /(?<!com)putin/i
					bot.api.sendMessage(chat_id: message.chat.id, reply_to_message_id: message.message_id, text: 'Correct pronunciation is "khuylo"👆🏻')
				when /(?<!пету|ло)(шар[іи][йяє])/i
					bot.api.sendSticker(chat_id: message.chat.id, reply_to_message_id: message.message_id, sticker: "CAACAgIAAx0CTkEQNAACH0JfEtwwmjhJBgd8jw0y-kvLbkctOAACghwAAkli9xm_f20Kynj5DBoE")
				when /\bна\b\s+укра(їні|ине)\b/i
					bot.api.sendMessage(chat_id: message.chat.id, reply_to_message_id: message.message_id, text: 'Правильно казати "в Україні"👆🏻')
				when /(в|у) рос+(ії|ии)/i
					bot.api.sendMessage(chat_id: message.chat.id, reply_to_message_id: message.message_id, text: 'Правильно казати "на росії"👆🏻')
				when /\bkiev\b/i
					bot.api.sendMessage(chat_id: message.chat.id, reply_to_message_id: message.message_id, text: 'Kyiv, not Kiev👆🏻')
				when /^[щш]о\s*по\s*(орках|русні|свинособаках)\??$/i
					result = shoporusni_gentext_yesterday
					bot.api.sendMessage(chat_id: message.chat.id, reply_to_message_id: message.message_id, parse_mode: "HTML", text: result, reply_markup: warship_details_kb)
			end
		when Hash
			# nothing
	end

end

def youtube_get_secret(bot, message)
	youtube_regexp = /(?<=(watch\?v=))(?<secret>[^&$]+)/i
	return nil if message.text.match(youtube_regexp).nil?
	secret = message.text.match(youtube_regexp)['secret']
	return secret
end

def youtube_parse(bot, message)
	return if message.from.id != $admin_id
	secret = youtube_get_secret(bot, message)
	filepath = "/tmp/youtube-#{secret}"
	Dir.mkdir(filepath) unless File.exists?(filepath)
	return if secret.nil?
	code = "/home/pi/lemur/youtube.sh #{secret}"
	Thread.new do
		Open3.popen3(code, chdir: filepath) do |stdin, stdout, stderr, thread|
			fullpath = "#{filepath}/#{stdout.gets}"
			mimetype = 'audio/mpeg'
			begin
				bot.api.sendDocument(chat_id: message.chat.id, reply_to_message_id: message.message_id, document: Faraday::UploadIO.new(fullpath, mimetype))
			rescue
				bot.api.sendDocument(chat_id: message.chat.id, document: Faraday::UploadIO.new(fullpath, mimetype))
			end
		end
	end
end

def ip_address_parse(bot, message)
	return if message.from.id != $admin_id

	text = `ip a`
	bot.api.sendMessage(chat_id: message.chat.id, reply_to_message_id: message.message_id, text: text)
end

def emojis(text)
	return [] if text.nil?
	text.scan(EmojiRegex::RGIEmoji)
end

def sec_to_text(sec)
	if sec < 60
		return "#{sec} сек"
	end
	hours = sec / 3600
	minutes = sec % 3600 / 60
	secundes = sec % 60
	if minutes == 0 and secundes == 0
		return "#{hours} год"
	end
	if hours == 0 and secundes == 0
		return "#{minutes} хв"
	end
	arr = []
	arr.push(hours) if hours > 0
	arr.push(minutes) if minutes > 0
	arr.push(secundes)
	arr.map! do |x|
		if x < 10
			"0#{x}"
		else
			x.to_s
		end
	end
	arr.join(":")
end

def get_exp_arr(err_response_body)
	exp_arr = JSON.load(err_response_body)
	# status = exp_arr['ok'] #false or true
	# error_code = exp_arr['error_code'] #3-digits integer like "400"
	# desc = exp_arr['description'] #text string like "Bad Request: reply message not found"
	return exp_arr
end

def check_time(&block)
	start = Time.now.to_i
	if block_given?
		yield
	else
		puts "No block given!"
		return false
	end
	finish = Time.now.to_i
	return finish - start
end
def detect_rights(message)
	message_id = get_message_id(message)
	chat_id = get_chat_id(message)
	user_id = get_author_id(message)
	group = Group.create_or_find_by({ id: chat_id})
	url = message.text.match(URI::regexp).to_s
		if url.empty?
			if group.group_rules == nil
				raise BotSkipMessage
			else
				text = group.group_rules
				Lemur.api.sendMessage(chat_id: chat_id, reply_to_message_id: message_id, text: text)
			end
		else
			tg_admins = Lemur.api.getChatAdministrators(chat_id: chat_id)['result']
			admins_arr = []
			tg_admins.each do |x|
				admins_arr.push(x['user']['id'])
			end
			if admins_arr.include?(user_id) or user_id == $admin_id
				group.group_rules = url
				group.save
				Lemur.api.sendMessage(chat_id: chat_id, reply_to_message_id: message_id, reply_markup: delete_kb, text: "Посилання для правил #{url} збережено")
			end
		end
end

def detect_adv(message)
	message_id = get_message_id(message)
	chat_id = get_chat_id(message)
	user_id = get_author_id(message)
	group = Group.create_or_find_by({ id: chat_id})
	url = message.text.match(URI::regexp).to_s
	if url.empty?
		if group.group_adv == nil
			raise BotSkipMessage
		else
			text = group.group_adv
			Lemur.api.sendMessage(chat_id: chat_id, reply_to_message_id: message_id, text: text)
		end
	else
		tg_admins = Lemur.api.getChatAdministrators(chat_id: chat_id)['result']
		admins_arr = []
		tg_admins.each do |x|
			admins_arr.push(x['user']['id'])
		end
		if admins_arr.include?(user_id) or user_id == $admin_id
			group.group_adv = url
			group.save
			Lemur.api.sendMessage(chat_id: chat_id, reply_to_message_id: message_id, reply_markup: delete_kb, text: "Посилання для реклами #{url} збережено")
		end
	end
end

# Open3.popen3(code, chdir: '/home/pi') do |stdin, stdout, stderr, thread|
# 	pid = thread.pid
# 	text = "<b>STDOUT</b>\n<pre>#{CGI.escapeHTML(stdout.read.chomp)}</pre>"
# 	unless stdout.read == ""
# 		text = "<b>STDOUT</b>\n<pre>#{CGI.escapeHTML(stdout.read.chomp)}</pre>"
# #               text = CGI.escapeHTML(stdout.read.chomp)
# 	end
# 	unless stderr.read == ""
# 		text += "\n\n<b>STDERR</b>\n```#{CGI.escapeHTML(stderr.read.chomp)}```" unless stderr.read.chomp == ""
# 	end
# 	#if text == "" and thread.value.exitstatus == 0
# 	#text=$OKAY
# 	#end
# 	unless thread.value.exitstatus == 0
# 		text += "\n\n<b>EXITSTATUS</b>\n#{CGI.escapeHTML(thread.value.exitstatus.to_s)}" unless thread.value.exitstatus == ""
# 	end
# 	text
# end

# mimetype = %x[mimetype --output-format %m "#{document}"|tr -d '\n']
# if mimetype == "" or mimetype == nil
# then
# 	mimetype = 'text/plain'
# end
# bot.api.sendChatAction(chat_id: message.chat.id, action: "upload_document")
# bot.api.sendDocument(chat_id: message.chat.id, reply_to_message_id: message.message_id, document: Faraday::UploadIO.new(document, mimetype)) rescue puts "doc
# ument = #{document}, mimetype = #{mimetype}"  #bot.api.sendMessage(chat_id: message.chat.id, reply_to_message_id: message.message_id,  text: "Не удалось отправить файл <b>#{document}</b>", parse_mode: "HTML")
# end
