#!/usr/bin/ruby

def roulette_get_garbage(group_id)
	game = Roulette.create_or_find_by({id: group_id})
	JSON.load(game.garbage)
end

def roulette_add_garbage(message)
	case message
		when Hash
			group_id = message['result']['chat']['id']
			message_id = message['result']['message_id']
		when Telegram::Bot::Types::Message
			group_id = message.chat.id
			message_id = message.message_id
		when Telegram::Bot::Types::CallbackQuery
			group_id = message.message.chat.id
			message_id = message.message.message_id
		else
			return
	end
	game = Roulette.create_or_find_by({id: group_id})
	current_garbage = roulette_get_garbage(group_id)
	new_garbage = current_garbage.push(message_id).uniq
	game.garbage = new_garbage
	game.save
end

def roulette_reset_garbage(group_id)
	game = Roulette.create_or_find_by({id: group_id})
	game.garbage=Array.new.to_json
	game.save
end

def roulette_clear_garbage(bot, group_id)
	Thread.new do
		garbage = roulette_get_garbage(group_id)
		garbage.each do |x|
			begin
				bot.api.deleteMessage(chat_id: group_id, message_id: x)
			rescue
				next
			end
		end
		roulette_reset_garbage(group_id)
	end
end
