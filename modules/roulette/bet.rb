#!/usr/bin/ruby

class Roulette
	class Bet
		attr_accessor :category, :type, :quantity, :text, :double
		@@categories = [:color, :bigly, :parity, :kvar, :raw, :zero]

		def double?
			@double
		end

		def get_text
			return @type if @type.is_a?(Integer)

			case @type
			when 'black'
				'чорний'
			when 'red'
				'червоний'
			when 'green'
				'зеро (0)'
			when 'big'
				'велике (19-36)'
			when 'small'
				'маленьке (1-18)'
			when 'odd'
				'непарне (1,3,5...)'
			when 'even'
				'парне (2,4,6...)'
			when 'trio1'
				'першу трійку (1-12)'
			when 'trio2'
				'другу трійку (13-24)'
			when 'trio3'
				'третю трійку (25-36)'
			when 'kvar1'
				'першу чверть (1-9)'
			when 'kvar2'
				'другу чверть (10-18)'
			when 'kvar3'
				'третю чверть (19-27)'
			when 'kvar4'
				'четверту чверть (28-36)'
			else
				raise 'Як ти додумався сюди потрапити?'
			end
		end

		def dot
			case self.type
				when 'black'
					$symbol_black_dot
				when 'red'
					$symbol_red_dot
				when 'green'
					$symbol_green_dot
				else
					nil
			end
		end

		class Category
			attr_accessor :category

			def initialize(type)
				case type.to_s
					when /black|red/
						@category = :color
					when /big|small/
						@category = :bigly
					when /even|odd/
						@category = :parity
					when /trio1|trio2|trio3/
						@category = :trio
					when /trio1|trio2|trio3/
						@category = :trio
					when /kvar1|kvar2|kvar3|kvar4/
						@category = :kvar
					when /([1-9]|[12][0-9]|3[0-6])/
						@category = :raw
					when /green/
						@category = :zero
				end
			end
		end

		def initialize(type, quantity)
			new_category = Bet::Category.new(type)
			@category = new_category.category
			#(?<!(kvar|trio)) потрібно, щоб не детектило kvar1 як ставку на число 1
			if type.match(/(?<!kvar)(?<!trio)([1-9]|[12][0-9]|3[0-6])/)
				#Якщо ставимо на число, то в "тип" запишемо Integer замість String
				@type = type.to_i
			else
				@type = type
			end
			@quantity = quantity.to_i
			@text = self.get_text
		end
	end
end

def detect_bet(message)
	case message
		when Telegram::Bot::Types::CallbackQuery
			case message.data
				when /bet_(red|black|green)_(\d+|double)/
					roulette_add_garbage(message)
					matchdata = message.data.match(/bet_(red|black|green)_(\d+|double)/)
					text_arr = matchdata.captures unless matchdata.nil?
					# bet = text_arr[1].to_i
					#костиль
					raise BotSkipMessage if text_arr.nil?
					case text_arr[1]
						when 'double'
							double = true
						when /\d+/
							double = false
						else
							raise BotException("Як ти сюди потрапив?")
					end
					result = Roulette::Bet.new(text_arr[0], text_arr[1])
					result.double = double
					return result
			end
		when Telegram::Bot::Types::Message
			#case message.text
			#end
			text = message.text
			return false if text.nil?
			result = text.scan(/(^\d)/)
			return false if result[0].nil?

			black_regexp_text = 'black|xjhybq|чорн(ий|е)|ч'
			red_regexp_text = 'red|xthdjybq|червон(ий|е)|к'
			green_regexp_text = 'green|zero|ptktybq|pthj|зелен(ий|е)|зеро|з|(?<!\d)0'
			color_regexp_text = "#{black_regexp_text}|#{red_regexp_text}|#{green_regexp_text}"

			big_regexp_text = 'big|dtkbrt|велик(ий|е)|в'
			small_regexp_text = 'small|vfktymrt|vfkt|мале|малий|маленьк(ий|е)|м'
			size_regexp_text = "#{big_regexp_text}|#{small_regexp_text}"

			even_regexp_text = 'even|gfhyt|парн(ий|е)|п'
			odd_regexp_text = 'odd|ytgfhyt|непарн(ий|е)|н'
			parity_regexp_text = "#{even_regexp_text}|#{odd_regexp_text}"

			trio1_regexp_text = '1-12|(перш(у|а|е)|first|1)\s+(трійка|трійку|тріо|trio|3)'
			trio2_regexp_text = '13-24|(другу(у|а|е)|second|2)\s+(трійка|трійку|тріо|trio|3)'
			trio3_regexp_text = '25-36|(трет(ю|я|є)|third|3)\s+(трійка|трійку|тріо|trio|3)'
			trio_regexp_text = "#{trio1_regexp_text}|#{trio2_regexp_text}|#{trio3_regexp_text}"

			kvar1_regexp_text = '1-9|(перш(у|а|ий)|first|1)\s+(чверть|четвірку|сквад|squad|4)'
			kvar2_regexp_text = '10-18|(другу(у|а|й)|second|2)\s+(чверть|четвірку|сквад|squad|4)'
			kvar3_regexp_text = '19-27|(трет(ю|я|ій)|third|3)\s+(чверть|четвірку|сквад|squad|4)'
			kvar4_regexp_text = '28-36|(четверт(у|а|ий)|fourth|4)\s+(чверть|четвірку|сквад|squad|4)'
			kvar_regexp_text = "#{kvar1_regexp_text}|#{kvar2_regexp_text}|#{kvar3_regexp_text}|#{kvar4_regexp_text}"

			raw_regexp_text = '([1-9]|[12][0-9]|3[0-6])'

			all_regexp_text = "#{raw_regexp_text}|#{color_regexp_text}|#{size_regexp_text}|#{parity_regexp_text}|#{trio_regexp_text}|#{kvar_regexp_text}"
			bet_regexp = /^(?<quantity>\d+)\s+((на|yf|on)\s+)?(?<type>#{all_regexp_text})$/i
			bet_match = text.match(/#{bet_regexp}/)
			return false if bet_match.nil?
			type_text = bet_match['type']
			quantity = bet_match['quantity']
			case type_text
				when Regexp.new(black_regexp_text + '$')
					type = 'black'
				when Regexp.new(red_regexp_text + '$')
					type = 'red'
				when Regexp.new(green_regexp_text + '$')
					type = 'green'
				when Regexp.new(big_regexp_text + '$')
					type = 'big'
				when Regexp.new(small_regexp_text + '$')
					type = 'small'
				when Regexp.new(odd_regexp_text + '$')
					type = 'odd'
				when Regexp.new(even_regexp_text + '$')
					type = 'even'
				when Regexp.new(trio1_regexp_text + '$')
					type = 'trio1'
				when Regexp.new(trio2_regexp_text + '$')
					type = 'trio2'
				when Regexp.new(trio3_regexp_text + '$')
					type = 'trio3'
				when Regexp.new(kvar1_regexp_text + '$')
					type = 'kvar1'
				when Regexp.new(kvar2_regexp_text + '$')
					type = 'kvar2'
				when Regexp.new(kvar3_regexp_text + '$')
					type = 'kvar3'
				when Regexp.new(kvar4_regexp_text + '$')
					type = 'kvar4'
				when Regexp.new(raw_regexp_text + '$')
					type = type_text
			end
			result = Roulette::Bet.new(type, quantity)
			result.double = false
			result
	end
end
