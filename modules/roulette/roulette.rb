class Roulette < ActiveRecord::Base
	def upd
		self.deadline = Time.now.to_i + $roulette_deadline
		self.save
	end

	def timeout?
		Time.now.to_i > self.deadline.to_i
	end

	def gamers
		result = []
		types = []
		["black", "red", "green"].each{|x|types.push(x)}
		["big", "small", "odd", "even"].each{|x|types.push(x)}
		["trio1", "trio2", "trio3"].each{|x|types.push(x)}
		["kvar1", "kvar2", "kvar3", "kvar4"].each{|x|types.push(x)}
		types.each do |type|
			get_bet(type: type).each_key{|user| result.push(user)}
		end
		raw.each do |number, inhash|
			inhash.each_key{|user| result.push(user)}
		end
		result.sort.uniq
	end

	def black(user = nil)
		if user.nil?
			JSON.load(self.bets_black_arr)
		else
			JSON.load(self.bets_black_arr)[user.to_s].to_i
		end
	end

	def red(user = nil)
		if user.nil?
			JSON.load(self.bets_red_arr)
		else
			JSON.load(self.bets_red_arr)[user.to_s].to_i
		end
	end

	def green(user = nil)
		if user.nil?
			JSON.load(self.bets_green_arr)
		else
			JSON.load(self.bets_green_arr)[user.to_s].to_i
		end
	end

	def big(user = nil)
		if user.nil?
			JSON.load(self.bets_big_arr)
		else
			JSON.load(self.bets_big_arr)[user.to_s].to_i
		end
	end

	def small(user = nil)
		if user.nil?
			JSON.load(self.bets_small_arr)
		else
			JSON.load(self.bets_small_arr)[user.to_s].to_i
		end
	end

	def even(user = nil)
		if user.nil?
			JSON.load(self.bets_even_arr)
		else
			JSON.load(self.bets_even_arr)[user.to_s].to_i
		end
	end

	def odd(user = nil)
		if user.nil?
			JSON.load(self.bets_odd_arr)
		else
			JSON.load(self.bets_odd_arr)[user.to_s].to_i
		end
	end

	def trio1(user = nil)
		if user.nil?
			JSON.load(self.bets_trio1_arr)
		else
			JSON.load(self.bets_trio1_arr)[user.to_s].to_i
		end
	end

	def trio2(user = nil)
		if user.nil?
			JSON.load(self.bets_trio2_arr)
		else
			JSON.load(self.bets_trio2_arr)[user.to_s].to_i
		end
	end

	def trio3(user = nil)
		if user.nil?
			JSON.load(self.bets_trio3_arr)
		else
			JSON.load(self.bets_trio3_arr)[user.to_s].to_i
		end
	end

	def kvar1(user = nil)
		if user.nil?
			JSON.load(self.bets_kvar1_arr)
		else
			JSON.load(self.bets_kvar1_arr)[user.to_s].to_i
		end
	end

	def kvar2(user = nil)
		if user.nil?
			JSON.load(self.bets_kvar2_arr)
		else
			JSON.load(self.bets_kvar2_arr)[user.to_s].to_i
		end
	end

	def kvar3(user = nil)
		if user.nil?
			JSON.load(self.bets_kvar3_arr)
		else
			JSON.load(self.bets_kvar3_arr)[user.to_s].to_i
		end
	end

	def kvar4(user = nil)
		if user.nil?
			JSON.load(self.bets_kvar4_arr)
		else
			JSON.load(self.bets_kvar4_arr)[user.to_s].to_i
		end
	end

	def raw(user = nil, type: nil)
		# Якщо не задано id користувача
		if user.nil?
			# Якщо не задано id користувача і не задано конкретне число (відповідь - хеш {число:користувач:ставка})
			if type.nil?
				JSON.load(self.bets_raw_arr)
			# Якщо не задано id користувача, але задано конкретне число (відповідь - хеш {користувач:ставка})
			else
				result = JSON.load(self.bets_raw_arr)[type.to_s]
				result.nil? ? {} : result
			end
			# Якщо задано id користувача
		else
			# Якщо задано id користувача, але не задано конкретне число (відповідь - сконструйований хеш {число:ставка})
			if type.nil?
				result = {}
				hash = self.raw
				hash.each do |db_type, inshash|
					inshash.each do |db_user, db_bet|
						if db_user == user.to_s
							result[db_type] = db_bet
						end
					end
				end
				result
			# Якщо задано id користувача і задано конкретне число (відповідь - одне число Integer)
			else
				number_bets = JSON.load(self.bets_raw_arr)[type.to_s]
				return 0 if number_bets.nil?
				number_bets[user.to_s]
			end
		end
	end
	def raw_sum(user = nil)
		if user.nil?
			sum = 0
			raw_all_bets = raw
			raw_all_bets.each_key do |type|
				raw(type: type).each_value { |db_bet| sum += db_bet}
			end
		else
			raw_bets = raw(user)
			sum = 0
			raw_bets.each_value do |db_bet|
				sum = sum + db_bet
			end
		end
		sum
	end

	def get_bet(user = nil, type:)
		case type.to_s
			when 'red'
				self.red(user)
			when 'black'
				self.black(user)
			when 'green'
				self.green(user)
			when 'big'
				self.big(user)
			when 'small'
				self.small(user)
			when 'even'
				self.even(user)
			when 'odd'
				self.odd(user)
			when 'trio1'
				self.trio1(user)
			when 'trio2'
				self.trio2(user)
			when 'trio3'
				self.trio3(user)
			when 'kvar1'
				self.kvar1(user)
			when 'kvar2'
				self.kvar2(user)
			when 'kvar3'
				self.kvar3(user)
			when 'kvar4'
				self.kvar4(user)
			when /([1-9]|[12][0-9]|3[0-6])/
				self.raw(user, type: type)
		end
	end

	def black_set(user, value)
		#bugfix
		arr = JSON.load(self.bets_black_arr)
		arr[user.to_s] = value
		self.bets_black_arr = arr.to_json
		self.save
	end

	def red_set(user, value)
		arr = JSON.load(self.bets_red_arr)
		arr[user.to_s] = value
		self.bets_red_arr = arr.to_json
		self.save
	end

	def green_set(user, value)
		arr = JSON.load(self.bets_green_arr)
		arr[user.to_s] = value
		self.bets_green_arr = arr.to_json
		self.save
	end

	def big_set(user, value)
		arr = JSON.load(self.bets_big_arr)
		arr[user.to_s] = value
		self.bets_big_arr = arr.to_json
		self.save
	end

	def small_set(user, value)
		arr = JSON.load(self.bets_small_arr)
		arr[user.to_s] = value
		self.bets_small_arr = arr.to_json
		self.save
	end

	def even_set(user, value)
		arr = JSON.load(self.bets_even_arr)
		arr[user.to_s] = value
		self.bets_even_arr = arr.to_json
		self.save
	end

	def odd_set(user, value)
		arr = JSON.load(self.bets_odd_arr)
		arr[user.to_s] = value
		self.bets_odd_arr = arr.to_json
		self.save
	end

	def trio1_set(user, value)
		arr = JSON.load(self.bets_trio1_arr)
		arr[user.to_s] = value
		self.bets_trio1_arr = arr.to_json
		self.save
	end

	def trio2_set(user, value)
		arr = JSON.load(self.bets_trio2_arr)
		arr[user.to_s] = value
		self.bets_trio2_arr = arr.to_json
		self.save
	end

	def trio3_set(user, value)
		arr = JSON.load(self.bets_trio3_arr)
		arr[user.to_s] = value
		self.bets_trio3_arr = arr.to_json
		self.save
	end

	def kvar1_set(user, value)
		arr = JSON.load(self.bets_kvar1_arr)
		arr[user.to_s] = value
		self.bets_kvar1_arr = arr.to_json
		self.save
	end

	def kvar2_set(user, value)
		arr = JSON.load(self.bets_kvar1_arr)
		arr[user.to_s] = value
		self.bets_kvar1_arr = arr.to_json
		self.save
	end

	def kvar3_set(user, value)
		arr = JSON.load(self.bets_kvar3_arr)
		arr[user.to_s] = value
		self.bets_kvar3_arr = arr.to_json
		self.save
	end

	def kvar4_set(user, value)
		arr = JSON.load(self.bets_kvar4_arr)
		arr[user.to_s] = value
		self.bets_kvar4_arr = arr.to_json
		self.save
	end

	def raw_set(user, value, type)
		arr = JSON.load(self.bets_raw_arr)
		arr[type.to_s] = { user.to_s => value }
		self.bets_raw_arr = arr.to_json
		self.save
	end

	def total_no_zero(user)
		arr = []
		arr.push(self.black(user))
		arr.push(self.red(user))
		arr.push(self.big(user))
		arr.push(self.small(user))
		arr.push(self.odd(user))
		arr.push(self.even(user))
		arr.push(self.trio1(user))
		arr.push(self.trio2(user))
		arr.push(self.trio3(user))
		arr.push(self.kvar1(user))
		arr.push(self.kvar2(user))
		arr.push(self.kvar3(user))
		arr.push(self.kvar4(user))
		arr.push(self.raw_sum(user))
		arr.inject(:+)
				# self.odd(user) + self.even(user) +
				# self.trio1(user) + self.trio2(user) + self.trio3(user) +
				# self.kvar1(user) + self.kvar2(user) + self.kvar3(user) + self.kvar4(user))
	end
	def total(user)
		self.green(user) + total_no_zero(user)
	end

	def ad_log(number = nil)
		limit = 50
		if number.nil?
			JSON.load(self.log)
		else
			arr = self.ad_log
			arr.push(number)
			self.log = arr.last(limit).to_json
			self.save
		end
	end

	def flog(limit = nil)
		if limit.nil?
			log = self.ad_log
		else
			log = self.ad_log.last(limit)
		end
		arr = []
		log.each do |x|
			foo = RouletteSpin.new(x)
			arr.push("#{foo.number}#{foo.dot}")
		end
		arr
	end
end