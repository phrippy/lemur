#Spin begin

class RouletteSpin
	@@quantity = 36

	def number
		@number
	end

	#[:color, :bigly, :parity, :kvar, :raw, :zero]
	def bigly
		case @number
			when 1..18
				return 'small'
			when 19..36
				return 'big'
			when 0
				return false
			else
				raise BotException.new("Як ти сюди потрапив")
		end
	end
	
	def parity
		return false if @number == 0
		return 'odd' if @number.odd?
		return 'even' if @number.even?
		raise BotException.new("Як ти сюди потрапив")
	end

	def number=(number)
		check_number(number)
		@number = number.to_i
	end

	def trio
		case @number
			when 1..12
				return 'trio1'
			when 13..24
				return 'trio2'
			when 25..36
				return 'trio3'
		end
	end

	def kvar
		case @number
			when 1..9
				return 'kvar1'
			when 10..18
				return 'kvar2'
			when 19..27
				return 'kvar3'
			when 28..36
				return 'kvar4'
		end
	end
	
	def check_number(number)
		unless number.is_a?(Integer)
			raise BotRouletteNotInteger.new(number)
		end
		unless (0..@@quantity).include?(number)
			raise BotRouletteWrongRange.new(number)
		end
	end

	def initialize(number = nil)
		if number.nil?
			result = rand(@@quantity + 1)
			check_number(result)
			@number = result
		else
			@number = number
		end
	end

	def color
		case @number
			when 0
				return 'green'
			when 2, 4, 6, 8, 10, 11, 13, 15, 17, 20, 22, 24, 26, 28, 29, 31, 33, 35
				return 'black'
			when 1, 3, 5, 7, 9, 12, 14, 16, 18, 19, 21, 23, 25, 27, 30, 32, 34, 36
				return 'red'
			else
				raise BotRouletteUnknownError.new(@number)
		end
	end

	def ukr_color
		case self.color
			when 'black'
				"чорний"
			when 'red'
				"червоний"
			when 'green'
				"зеро"
		end
	end

	def dot
		case self.color
			when 'black'
				$symbol_black_dot
			when 'red'
				$symbol_red_dot
			when 'green'
				$symbol_green_dot
			else
				nil
		end
	end

	def zero?
		@number == 0
	end

	def even?
		return nil if @number.zero?
		@number.even?
	end

	def odd?
		return nil if @number.zero?
		@number.odd?
	end

	def big?
		return nil if @number.zero?
		bigrange = ((@@quantity/2)..@@quantity)
		bigrange.include?(@number)
	end

	def small?
		return nil if @number.zero?
		smallrange = (1..(@@quantity/2))
		smallrange.include?(@number)
	end
end