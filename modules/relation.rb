#!/usr/bin/ruby
class MemberRelation < ActiveRecord::Base

	def check_like_relation
		return 'restart' if timeout <= Time.now.to_i
		return false if self.like_count >= limit
		return true
	end

	def check_stole_relation
		limit, timeout = get_relation_stole_params(self)
		now = Time.now.to_i
		# puts "timeout <= Time.now.to_i: #{self.stole_first_in_cycle + timeout} <= #{now}"
		# puts "count = #{self.stole_count}"
		self.restart_stole_relation if self.stole_first_in_cycle + timeout <= now
		wait = (self.stole_first_in_cycle + timeout) - now
		return wait if self.stole_count >= limit
		return 0
	end

	def restart_stole_relation
		self.stole_count = 0
		self.stole_first_in_cycle = Time.now.to_i
		self.save
	end

end

def get_chat_relation(chat_id, master_id, slave_id)
	relation = MemberRelation.find_by({ chat_id: chat_id, master_id: master_id, slave_id:  slave_id })
	if relation.nil?
		relation = MemberRelation.create({ chat_id: chat_id, master_id: master_id, slave_id:  slave_id })
	end
	return relation
end