#!/usr/bin/ruby

def talan_set_timeout(user_id, counter, timestamp)
	user = User.create_or_find_by({id: user_id})
	user.talan_timeout = Base64.strict_encode64([counter.to_i, timestamp.to_i].to_json)
	user.save
end

def talan_get_timeout(user_id)
	user = User.create_or_find_by({id: user_id})
	begin
		arr = JSON.load(Base64.strict_decode64(user.talan_timeout))
		return [arr[0].to_i, arr[1].to_i]
	rescue
		talan_reset_timeout(user_id)
		retry
	end
end


def talan_reset_timeout(user_id)
	counter = 0
	timestamp = Time.now.to_i + 300
	talan_set_timeout(user_id, counter, timestamp)
end

def talan_update_timeout(user_id)
	arr = talan_get_timeout(user_id)
	talan_set_timeout(user_id, arr[0] + 1, arr[1])
end

def talan_check_timeout(user_id)
	limit = 3
	arr = talan_get_timeout(user_id)
	if Time.now.to_i > arr[1]
		talan_reset_timeout(user_id)
		return true
	end
	return true if arr[0] < limit
	return false
end
#
# ActiveRecord::Base.establish_connection( :adapter => "mysql2", :host => "localhost", :username => "pi", :password => "secret", :database => "lemur" )
# class User < ActiveRecord::Base ; end
