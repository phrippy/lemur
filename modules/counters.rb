#!/usr/bin/ruby
def group_check_yesterday_reverse(group)
	today = Date.today.to_s
	#noinspection RubyResolve
	today == group.today_date
end
def update_group_counter(bot, message)
	#noinspection RubyResolve
	case message
	when Telegram::Bot::Types::CallbackQuery
			return false
	when Telegram::Bot::Types::Message
			group = Group.create_or_find_by({id: message.chat.id})
			user = User.create_or_find_by({id: message.from.id})
			member = get_chat_member(message.chat.id, message.from.id)
			#noinspection RubyResolve
			return if member.last_message == message.message_id
			member.last_message = message.message_id
			member.save
			# todo delete
			# user_total_in_group = group.messages(message.from.id)
			# user_total_in_group = member.messages #ready
			# current_level = get_level(user_total_in_group, group.maximum)
			current_level = member.level
			if current_level.id > member.max_level
				take_reward = true
				member.max_level = current_level.id
				member.save
			else
				take_reward = false
			end
			old = member.level.id
			increment_group_counter(user, group)
			new = member.level.id
			# puts "#{old}-#{new}(#{member.messages}/#{group.maximum})"
			# puts "#{member.max_level}-#{take_reward}"
			return unless take_reward
			response = level_updated(message, current_level, group)
			if response.is_a?(Level) and group.maximum >= 1000
				arr = []
				arr.push("Вітаю, #{mention(message.from)}. Ви досягли звання #{response.id} - #{response.name}")
				# arr.push("Ви заробили:\n#{symbol_carma}<code>#{reward_carma}</code>\n#{symbol_talan}<code>#{reward_talan}</code>")
				if take_reward
					reward_carma = (level_to_relation(response) * 1000).to_i
					user.carma += reward_carma
					arr.push("Ви заробили: #{symbol_carma(group.id, user.id)}<code>#{reward_carma}</code>")
				end
				#noinspection RubyResolve
				bot.api.sendMessage(chat_id: message.chat.id, parse_mode: 'HTML', text: arr.join("\n"))
			end
	else
		# type code here
	end
	if group.salutator_id.to_i == message.from.id and group.yesterday_saluted == false
		salute_yesterday_flooder(bot, message)
	end
end

def update_user_counter(bot, message)
	case message
		when Telegram::Bot::Types::CallbackQuery
			return false
		when Telegram::Bot::Types::Message
			return  false if message.chat.id == -1001312886836
			return  false if message.chat.id >= 0
			increment_user_counter(message.from.id)
	end
end

def get_flooders(message, group_id = false)
	text = []
	limit = get_flooders_show_limit(message)
	if group_id == false
		group = Group.create_or_find_by({id: message.chat.id})
		send_to = message.chat.id
	else
		group = Group.create_or_find_by({id: group_id})
		chat = Lemur.api.getChat(chat_id: group_id)
		if (group_id < 0)
			if chat['result']['invite_link'] == nil
				text.push = "Чат: <a href=\"https://t.me/#{chat['result']['username']}\">#{chat['result']['title']}</a>"
			else
				text.push = "Чат: <a href=\"#{chat['result']['invite_link']}\">#{chat['result']['title']}</a>"
			end
		else
			text.push "Чат: пп з <a href=\"tg://user?id=#{message.from.id}\">#{message.from.fullname}</a>"
		end
		send_to = $admin_id
	end
	user = User.create_or_find_by({id: message.from.id})
	member = get_chat_member(message.chat.id, message.from.id)
	today_flood = JSON.load(group.today_flood).sort_by{|k,v|v}.reverse.first(limit[0])
	yesterday_flood = JSON.load(group.yesterday_flood).sort_by{|k,v|v}.reverse.first(limit[1])

	text.push "Список балакунів за сьогодні:"
	today_flood.each do |k,v|
		userlink = mention(k.to_i, false)
		value = v
		result = "#{userlink}: ✉️#{value}"
		text.push(result)
	end
	text.push "За вчора:"
	yesterday_flood.each do |k,v|
		userlink = mention(k.to_i, false)
		value = v
		result = "#{userlink}: ✉️#{value}"
		text.push(result)
	end
	text = text.join("\n")
	if group_id == false
		Lemur.api.sendMessage(chat_id: send_to, reply_to_message_id: message.message_id, parse_mode: "HTML", text: text, reply_markup: delete_kb)
	else
		Lemur.api.sendMessage(chat_id: send_to, parse_mode: "HTML", text: text, reply_markup: delete_kb)
	end
end

def increment_group_counter(user, group)
	# user and group are database items
	member = get_chat_member(group.id, user.id)
	member.increment_message
	# group.increment_user(user.id)
	member = get_chat_member(group.id, user.id)
	user_total_in_group = member.messages #ready
	# todo delete
	# user_total_in_group = group.messages(user.id)
	if user_total_in_group > group.max_messages
		group.maximum = user_total_in_group
	end
	yesterday = group_check_yesterday_reverse(group) #db item
	if yesterday
		group.yesterday_flood = group.today_flood
		today_flood_hash = Hash.new
		today_flood_hash[user.id] = 1
		group.today_flood = today_flood_hash.to_json
		group.yesterday_date = group.today_date
		group.today_date = Date.today.to_s
		group.yesterday_saluted = false

		# Метод max_by для хешу повертає масив у вигляді ["1234567", 42]
		# Де "1234567" - це ідентифікатор користувача, а 42 - кількість повідомлень, які він написав
		# Нам потрібен лише ідентифікатор, тому беремо перше значення
		yesterday_flood = JSON.load(group.yesterday_flood)
		group.salutator_id = yesterday_flood.max_by{|k,v| v}.first
		group.save
		get_flooders(message, group.id.to_i)
	else
		yesterday_flood = JSON.load(group.yesterday_flood)
		group.salutator_id = yesterday_flood.max_by{|k,v| v}.first
		current_daily_messages = JSON.load(group.today_flood)
		userid = user.id.to_s
		current_daily_user_counter = current_daily_messages[userid].to_i
		current_daily_messages[userid] = current_daily_user_counter + 1
		group.today_flood = current_daily_messages.to_json
		group.save
	end
end

def increment_user_counter(user_id)
	user = User.create_or_find_by({id: user_id})
	user.increment_message
end

def update_group_name(bot, message)
	#blank
end

def update_user_name(bot, message)
	# Треба запаковувати імена в base64. А поки що - нічого не робимо
	return false if message.from.nil?
	db_user = User.create_or_find_by({ id: message.from.id })
	firstname = message.from.first_name
	lastname = message.from.last_name
	# simple_user = SimpleUser.new(message.from.first_name, message.from.last_name)
	# db_user.name_update(simple_user)
	db_user.name_update(firstname, lastname)
end

def get_spam_notify_admins(group_id)
	result = Admin.where({group_id: group_id, type_mv: 'spamban', enabled: true}).to_a.sort
	return result
end

def get_spam_notify_text(bot,message)
	group = Group.create_or_find_by({id: get_chat_id(message)})
	admins = get_spam_notify_admins(group.id)
	if admins.empty?
		adminlist = '<i>(пусто)</i>'
	else
		admins.map!{|x| "<code>#{mention(x.user_id, false)}</code>"}
		adminlist = admins.join(" | ")
	end
	text = "Сповіщати про спам:\n#{adminlist}"
	return text
end

def get_settings(bot, message)
	text = get_spam_notify_text(bot,message)
	message_id = get_message_id(message)
	chat_id = get_chat_id(message)
	case message
	when Telegram::Bot::Types::CallbackQuery
		bot.api.editMessageText(chat_id: chat_id, message_id: message_id, reply_markup: settings_kb, parse_mode: "HTML", text: text)
	when Telegram::Bot::Types::Message
		bot.api.sendMessage(chat_id: chat_id, reply_to_message_id: message_id, reply_markup: settings_kb, parse_mode: "HTML", text: text)
	end
end

