#!/usr/bin/ruby
# Плюсонути в карму - один із символів "👍❤️😂🙂😏😁😈+".
# Мінуснути в карму - один із символів "👎💔😡🤬👿-".

def bot_help (bot, message)
	chat_id = get_chat_id(message)
	user_id = get_author_id(message)
	str = <<EOF
Плюсонути в долю - один із символів "👍❤+".
Або словами: плюс, дякую, спасибі, лайк, like, thank
Мінуснути в долю - один із символів "👎💔-".
Або словами: мінус, дизлайк, dislike
Дізнатися долю членів групи - доля або /dolya
Дізнатися бюджет членів групи - бюджет або /budget
Дізнатися свою статистику - стата або /stat
Статистика повідомлень за день - балакуни або /flooders
Налаштування для адмінів - налаштування або /settings
Запустити гру "крокодил" - крокодил або /game
Запустити гру "рулетка" - рулетка або /ruletka
Екстренно зупинити гру "крокодил" - стоп або /stop
Інформація про поточний стан гри "крокодил" - статус або /status
Інформація про повідомлення - луна або /echo
Ідентифікатори - ід або /id
Допомога - /help або /start
Резервне копіювання - /backup або бекап
Товари, які можна придбати - магазин або /store
Список ваших товарів - інвентар або /inventory

Змінити символ валюти в чаті: валюта або /currency + емоджі
Вартість: #{symbol_carma(chat_id, user_id)}<code>#{change_chat_currency_emoji_price(bot, message)}</code>
Доступно з рівня #{change_chat_currency_emoji_min_level(bot, message)}

Конкурс "пара дня": пара дня або /shipping
EOF
	bot.api.sendMessage(chat_id: message.chat.id, reply_markup: delete_kb, parse_mode: "HTML", reply_to_message_id: message.message_id,  text: str)
end
def bot_todo(bot, message)
	str = <<'EOF'
test
EOF
			bot.api.sendMessage(chat_id: message.chat.id, reply_to_message_id: message.message_id, text: str)
end
def krokodile_status (bot, message)
	game = Krokodile.create_or_find_by({id: message.chat.id})
	if game.ingame
		bot.api.sendMessage(chat_id: message.chat.id, reply_to_message_id: message.message_id,  text: "Гра запущена")
		#bot.api.sendMessage(chat_id: message.chat.id, reply_to_message_id: message.message_id,  text: "Поточне слово - #{game.word}")
	else
		bot.api.sendMessage(chat_id: message.chat.id, reply_to_message_id: message.message_id,  text: "Гра не запущена")
	end
end

def bot_id (bot, message)
	str = <<EOF
Користувач <code>#{message.from.id}</code>

Група <code>#{message.chat.id}</code>

Повідомлення <code>#{message.message_id}</code>
EOF
	bot.api.sendMessage(chat_id: message.chat.id, reply_to_message_id: message.message_id, text: str, parse_mode: "HTML")
end

def bot_ping(bot, message)
	time_system = Time.now.to_f
	time_message = message.date
	difference = time_system - time_message
	# inline_kb = [[Telegram::Bot::Types::InlineKeyboardButton.new(text: "#{symbol_restart} Оновити", callback_data: 'update_ping_message')],
	inline_kb = [[Telegram::Bot::Types::InlineKeyboardButton.new(text: "#{symbol_delete} Закрити", callback_data: 'delete_this_message')]]
	kb = Telegram::Bot::Types::InlineKeyboardMarkup.new(inline_keyboard: inline_kb)
	#bot.api.sendMessage(chat_id: message.chat.id, reply_markup: delete_kb, reply_to_message_id: message.message_id, text: text.shuffle.first, parse_mode: "HTML")
	text = "<i>#{Time.now}</i>\nВідгук: <b>#{difference.round(5)}</b> сек\nПовторити: /ping"
	bot.api.sendMessage(chat_id: message.chat.id, reply_markup: kb, reply_to_message_id: message.message_id, text: text, parse_mode: "HTML")
end

def bot_update_ping(bot, message)
	time_system = Time.now.to_f
	time_message = get_date(message)
	difference = time_system - time_message
	inline_kb = [[Telegram::Bot::Types::InlineKeyboardButton.new(text: "#{symbol_restart} Оновити", callback_data: 'update_ping_message')],
							 [Telegram::Bot::Types::InlineKeyboardButton.new(text: "#{symbol_delete} Закрити", callback_data: 'delete_this_message')]]
	kb = Telegram::Bot::Types::InlineKeyboardMarkup.new(inline_keyboard: inline_kb)
	#bot.api.sendMessage(chat_id: message.chat.id, reply_markup: delete_kb, reply_to_message_id: message.message_id, text: text.shuffle.first, parse_mode: "HTML")
	text = "#{Time.now}\nВідгук #{difference} сек"
	bot.api.editMessageText(chat_id: get_chat_id(message), reply_markup: kb, message_id: get_message_id(message), parse_mode: 'HTML', text: text)
	# bot.api.sendMessage(chat_id: message.chat.id, reply_markup: kb, reply_to_message_id: message.message_id, text: text, parse_mode: "HTML")
end

def bot_echo(bot, message, original_message = false)
	if original_message == false
		if message.reply_to_message.nil?
			return bot_echo(bot, message, message)
		else
			if message.from.id == $admin_id
				# puts message.reply_to_message.inspect
				return bot_echo(bot, message.reply_to_message, original_message)
			else
				bot.api.sendMessage(chat_id: message.chat.id, reply_markup: delete_kb, reply_to_message_id: message.message_id, text: "Тільки для адмінів!", parse_mode: "HTML")
			end
		end
		else
	end
	require 'yaml'
	# result = CGI.escapeHTML(message.to_yaml)
	#
	data = message.clone.to_h
	data.each do |key,value|
		if value.nil? or value == '' or value == Hash.new or value == Array.new
			data.delete(key)
		end
	end
	from = data[:from]
	data[:from] = {}
	from.to_h.each do |key, value|
		unless value.nil? or value == '' or value == Hash.new or value == Array.new
			data[:from][key] = value
		end
	end
	chat = data[:chat]
	data[:chat] = {}
	chat.to_h.each do |key, value|
		unless value.nil? or value == '' or value == Hash.new or value == Array.new
			data[:chat][key] = value
		end
	end
	# entities = data[:entities]
	# data[:entities] = {}
	# # data[:entities] = data[:entities].to_a
	#  entities.to_h.each do |key, value|
	#  	unless value.nil? or value == '' or value == Hash.new or value == Array.new
	#  		data[:entities][key] = value
	#  	end
	#  end
	# puts data.inspect
	result = data.to_yaml

	result = "<code>#{result}</code>"
	bot.api.sendMessage(chat_id: message.chat.id, reply_markup: delete_kb, reply_to_message_id: message.message_id, text: result, parse_mode: "HTML")
end