#!/usr/bin/ruby
class Pair < ActiveRecord::Base ; end

def pairing_parse(bot, message)
	chat_id = get_chat_id(message)
	user_id = get_author_id(message)
	case message
		when Telegram::Bot::Types::CallbackQuery
			#case message.data
			#
			#end
		when Telegram::Bot::Types::Message
			# return unless test_groups(chat_id)
			case message.text
				when /^\A((пара\s+дня|gfhf\s+lyz)|(\/?shipping(#{$bot_name})?))\Z/i
					inintiator_min_level = pair_of_day_min_level_initiator(bot, message)
					inintiator = ChatMember.where({ chat: chat_id, user:  user_id}).to_a.first
					if inintiator.level.id < inintiator_min_level
						text = "Недостатній рівень. Досягніть як мінімум рівня #{inintiator_min_level}"
						bot.api.sendMessage(chat_id: message.chat.id, parse_mode: "HTML", reply_markup: delete_kb, reply_to_message_id: message.message_id, text: text)
						return false
					end
					check = check_pair_of_day(chat_id)
					# puts check
					# if check
					# 	puts "check == true"
					# end
					if check == true
						counter = 0
						begin
							last_pair = Pair.find_by({chat_id: chat_id, expired: false})
							pair = get_pair_of_day(bot, message)
							return unless pair
							pair.map! do |x|
								User.create_or_find_by({ id: x.user })
							end
							master = pair[0].id
							slave = pair[1].id
							unless last_pair.nil?
								last_pair.expired = true
								last_pair.save
							end
							set_pair_of_day(bot, message, chat_id, master, slave)
							text = [mention(master, true), mention(slave, true)]
							text = ["#{text[0]} + #{text[1]} = ❤️"]
							# unless last_pair.nil?
							# 	prev = [mention(last_pair.master, true), mention(last_pair.slave, true)]
							# 	prev_text = "Попередня пара: #{prev[0]} + #{prev[1]} = ❤️"
							# 	text.push(prev_text)
							# end
							bot.api.sendMessage(chat_id: message.chat.id, parse_mode: "HTML", reply_to_message_id: message.message_id, text: text.join("\n"))
						rescue
							counter += 1
							if counter < 4
								retry
							end
						end
					else
						pair = Pair.find_by({chat_id: chat_id, expired: false})
						text = ["Пара дня вже визначена:",
										"#{mention(pair.master, true)} + #{mention(pair.slave, true)} = ❤️",
										"Нову можна буде вибрати через #{sec_to_text(check)}"]
						bot.api.sendMessage(chat_id: message.chat.id, parse_mode: "HTML", reply_markup: delete_kb, reply_to_message_id: message.message_id, text: text.join("\n"))
					end
				else
					# nothing
			end
		end
end

def get_pair_of_day(bot, message)
	chat_id = get_chat_id(message)
	user_id = get_author_id(message)
	# members = ChatMember.where({chat: chat_id}).order(updated_at: :desc).to_a
	timeout = pair_of_day_last_message_timeout(bot, message)
	min_level = pair_of_day_min_level(bot, message)
	members = ChatMember.where({ chat: chat_id }).to_a
	if members.size < 2
		text = "Недостатньо учасників. Спробуйте пізніше"
		bot.api.sendMessage(chat_id: message.chat.id, parse_mode: "HTML", reply_markup: delete_kb, reply_to_message_id: message.message_id, text: text)
		return false
	end
	members = members.reject do |member|
		member.message_last + timeout < Time.now.to_i
	end
	members = members.reject do |member|
		member.level.id < min_level
	end
	# last_pair = Pair.where(id: chat_id)
	last_pair = Pair.find_by(chat_id: chat_id, expired: false)
	# puts "chat is #{chat_id}"
	# puts "Last pair class is #{last_pair.class}"
	# puts "Last pair inspect is #{last_pair.inspect}"
	# puts "Last pair: #{last_pair.to_a}"
	# puts "Last pair is nil" if last_pair.nil?
	# puts "members size is #{members.size}"
	# puts "pairs of chat count: #{Pair.all.to_a.size}"
	# puts Pair.all.to_a.first.inspect
	if last_pair.is_a?(Pair)
		members = members.reject do |member|
			member.user == last_pair.master or member.user == last_pair.slave
		end
	end
	if members.size < 2
		text = "Недостатній актив в чаті. Спробуйте пізніше"
		bot.api.sendMessage(chat_id: message.chat.id, parse_mode: "HTML", reply_markup: delete_kb, reply_to_message_id: message.message_id, text: text)
		return false
	end
	# puts last_pair.inspect
	# member1 = members.shuffle.pop
	# if member1.user == last_pair.master
	# 	members.reject!{|x| x.user == last_pair.slave}
	# end
	# if member1.user == last_pair.slave
	# 	members.reject!{|x| x.user == last_pair.master}
	# end
	# member2 = members.shuffle.first
	# return [member1, member2]
	return members.shuffle.first(2)
end

def check_pair_of_day(chat)
	pair = Pair.find_by({chat_id: chat, expired: false})
	return true if pair.nil?
	# pair = Pair.create_or_find_by({id: message.message.chat.id})
	if pair.expired_at < Time.now.to_i
		# pair.expired = true
		# pair.save
		return true
	end
	if pair.master.nil? or pair.slave.nil?
		# pair.expired = true
		# pair.save
		return true
	end
	return pair.expired_at - Time.now.to_i
end

def set_pair_of_day(bot, message, chat, master, slave)
	timeout = pair_of_day_timeout(bot, message)
	pair = Pair.new({chat_id: chat})
	pair.master = master
	pair.slave = slave
	pair.expired_at = Time.now.to_i + timeout
	pair.save
end
