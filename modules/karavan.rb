#!/usr/bin/ruby
class Karavan < ActiveRecord::Base

end


def karavan_start_kb
	eye = '👁'
	disable = '❌'
	grab='🤚🏻'
	kb = [
		[Telegram::Bot::Types::InlineKeyboardButton.new(text: "Підібрати#{grab}", callback_data: 'karavan_grab')]
	]
	markup = Telegram::Bot::Types::InlineKeyboardMarkup.new(inline_keyboard: kb)
	return markup
end

def karavan_disabled_kb
	eye = '👁'
	disable = '❌'
	grab='🤚🏻'
	kb = [
			[Telegram::Bot::Types::InlineKeyboardButton.new(text: "#{disable}", callback_data: 'karavan_disabled')]
	]
	markup = Telegram::Bot::Types::InlineKeyboardMarkup.new(inline_keyboard: kb)
	return markup
end

def karavan_finish(bot, karavan)
	bot.api.deleteMessage(chat_id: karavan.chat_id, message_id: karavan.start_mess_id)
end

def karavan_grab(bot, message)
	chat_id = get_chat_id(message)
	user_id = message.from.id
	message_id = get_message_id(message)
	karavan = Karavan.where(chat_id: chat_id, start_mess_id: message_id, grabbed: false).first
	if karavan.nil?
		return 1
	end
	user = User.create_or_find_by({id: user_id})
	value = karavan.coins
	user.carma += value

	mention = mention(message.from)
	text = "#{mention} підібрав #{symbol_carma(chat_id, user_id)}<code>#{value}</code>"
	# g_message = bot.api.sendMessage(chat_id: chat_id, text: text, parse_mode: "HTML", reply_markup: delete_kb)
	g_message = bot.api.sendMessage(chat_id: chat_id, text: text, parse_mode: "HTML")
	g_id = get_message_id(g_message)
	karavan.great_mess_id = g_id
	karavan.catcher_id = user_id
	karavan.finish_at = Time.now.to_i
	karavan.grabbed = true
	karavan.save
	karavan_finish(bot, karavan)
end

def karavan_grab_disabled(bot, message)
	return false
end

def karavan_parse(bot, message)
	case message
	when Telegram::Bot::Types::CallbackQuery
		#case message.data
		#end
		case message.data
			when /^karavan_grab/
				karavan_grab(bot, message)
			when /^karavan_disabled/
				karavan_grab_disabled(bot, message)
		end
	when Telegram::Bot::Types::Message
		#case message.text
		#end

		Thread.new do
			allow = karavan_allowed(bot,message)
			if not allow
				# puts 'not allowed' unless $DISABLE_DEBUGGING
				Thread.exit
			end
			# puts 'allowed!' unless $DISABLE_DEBUGGING
			sleep(rand(20))
			karavan_make(bot,message)
		end
	end
end

def karavan_check_old(bot, message)
	chat_id = get_chat_id(message)
	now = Time.now.to_i
	all = Karavan.find_by_sql("SELECT id FROM karavans WHERE delete_me_at < #{now} AND finish_at is null AND grabbed = false").to_a
	all.each do |x|
		karavan = Karavan.find(x.id)
		begin
			bot.api.deleteMessage(chat_id: karavan.chat_id, message_id: karavan.start_mess_id)
		rescue => e
			# puts e.message
			# puts e.backtrace.inspect
		end
		karavan.finish_at = now
		karavan.save
	end
end

def karavan_make(bot,message)
	value = karavan_get_value(bot,message)
	text = "Хтось загубив #{symbol_carma}<code>#{value}</code>"
	new_message = bot.api.sendMessage(chat_id: message.chat.id, text: text, parse_mode: "HTML", reply_markup: karavan_start_kb)
	karavan = Karavan.new
	karavan.chat_id = get_chat_id(message)
	karavan.catcher_id = nil
	karavan.start_mess_id = get_message_id(new_message)
	karavan.great_mess_id = nil
	timeout = karavan_get_timeout_new(bot,message)
	karavan.delete_me_at = Time.now.to_i + timeout
	karavan.start_at = Time.now.to_i
	karavan.finish_at = nil
	karavan.coins = value
	karavan.save
	return karavan
end

def karavan_get_timeout_new(bot,message)
	300
end

def karavan_allowed(bot,message)
	# disabled = true
	# return true if get_author_id(message) == 892755786
	# if message.from.id != $admin_id and disabled == true
	# 	return false
	# end
	get_today_karavans(bot,message)
	chance_arr = []
	chance_arr.push(chance_past_seconds(bot,message))
	chance_arr.push(chance_last_activity(bot,message))
	chance_arr.push(chance_day_counter(bot,message))
	chance = chance_arr.inject(:*)
	unless $DISABLE_DEBUGGING
		return true #disable forever
		puts "chance_past_seconds: #{chance_arr[0]}"
		puts "chance_last_activity: #{chance_arr[1]}"
		puts "chance_day_counter: #{chance_arr[2]}"
		puts "global chance: #{chance}"

		# puts "chance: #{chance}"
		bar = rand(1000)
	end
	result = rand(1000) < (chance * 1000)
	unless $DISABLE_DEBUGGING
		puts "#{bar} <=> #{chance * 1000}, #{result}"
	end
	return result
end

def chance_day_counter(bot,message)
	day_counter = get_karavan_day_counter(bot,message)
	maximum = karavan_max_per_day(bot,message)
	rest = maximum - day_counter
	if day_counter < maximum
		rest/maximum.to_f
	else
		(day_counter/maximum.to_f)/100
	end
end

def get_karavan_day_counter(bot,message)
	chat_id = get_chat_id(message)
	now = Time.now
	today_start = Time.new(now.year, now.month, now.day).to_i
	today_finish = today_start + 86400
	result = get_today_karavans(bot,message).size
end

def karavan_max_per_day(bot,message)
	return 7
end

def get_today_karavans(bot,message)
	chat_id = get_chat_id(message)
	now = Time.now
	today_start = Time.new(now.year, now.month, now.day).to_i
	today_finish = today_start + 86400
	result = Karavan.find_by_sql("SELECT id FROM karavans WHERE start_at > #{today_start} and start_at < #{today_finish} and chat_id = #{chat_id} and grabbed = true").to_a
	result.map! do |x|
		Karavan.find(x.id)
	end
	return result
end

def chance_last_activity(bot,message)
	past_seconds = chat_get_prev_activity(bot, message)
	past_minutes = past_seconds / 60.0
	if past_minutes > 10.0
		return 1
	end
	chance = past_minutes/10
	return chance
end

def chat_get_prev_activity(bot, message)
	chat_id = get_chat_id(message)
	chat = Group.create_or_find_by({id: chat_id})
	prev_activity = chat.prev_activity
	result = Time.now.to_i - prev_activity
	return result
end

def chance_past_seconds(bot,message)
	past_seconds = karavan_get_last_grab_passed(bot,message)
	past_hors = past_seconds / 3600
	past_seconds_chance = past_seconds / 36000.0
	return past_seconds_chance
end

def karavan_get_last_grab_passed(bot,message)
	all_karavans = Karavan.all.to_a
	return Time.now.to_i if all_karavans.empty?
	last = Karavan.all.max{|x| x.start_at}.start_at
	result = Time.now.to_i - last
	return result
end

def karavan_get_value(bot,message)
	max = karavan_get_max(bot,message)
	min = karavan_get_min(bot,message,maximum=max)
	result = (rand(max+1-min)+min)
	if result < karavan_garanted_value(bot,message)
		result = karavan_garanted_value(bot,message)
	end
	return result
end

def karavan_get_max_per_day(bot,message)
	3_000
end

def karavan_get_max(bot,message)
	current_max = 500
	left = karavan_get_max_per_day(bot,message) - karavan_get_today_grab_sum(bot,message)
	return 0 if left < 0
	left > current_max ? current_max : left # не більше 500 за раз, навіть якщо на день залишилось більше
end

def karavan_get_min(bot,message,maximum=1_000)
	if maximum < 1_000
		return 1
	else
		return 10
	end
end

def karavan_garanted_value(bot,message)
	# Мінімум!
	return 10
end

def karavan_get_today_grab_sum(bot,message)
	chat_id = get_chat_id(message)
	now = Time.now
	today_start = Time.new(now.year, now.month, now.day).to_i
	today_finish = today_start + 86400
	result = get_today_karavans(bot,message).sum{|x| x.coins}
	return result
end

