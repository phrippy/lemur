#!/usr/bin/ruby
require 'cgi'
class Tribunal < ActiveRecord::Base
	def yes(id=nil)
		yes_arr = JSON.load(self.yes_arr)
		if id.nil?
			return yes_arr
		else
			return yes_arr.include?(id)
		end
	end

	def yes=(arr)
		self.yes_arr = arr.to_json
		self.save
	end

	def no(id=nil)
		no_arr = JSON.load(self.no_arr)
		if id.nil?
			return no_arr
		else
			return no_arr.include?(id)
		end
	end

	def no=(arr)
	self.no_arr = arr.to_json
	self.save
	end

	def reason
		if reason_text.nil?
			return nil
		else
			return CGI.unescape(self.reason_text)
		end
	end

	def reason=(text)
		if text.nil?
			self.reason_text = nil
		else
			self.reason_text = CGI.escape(text)
		end
		self.save
	end

	def finished
		self.finished_bool
	end

	def finished=(other)
		if other == false or other == nil
			self.finished_bool = false
		else
			self.finished_bool = true
		end
		self.save
	end

	def limit
		self.limit_votes
	end

	def limit=(number)
		number = number.to_i
		default = tribunal_total_limit
		minimum = 3
		maximum = 255
		if number < minimum or number > maximum
			number = default
		end
		self.limit_votes = number
		self.save
	end

	def update_timeout
		self.expired_at = Time.now.to_i + tribunal_auto_expire(message)
		self.save
	end

	def expired?
		self.expired_at < Time.now.to_i
	end
end

def tribunal_kb
	inline_kb = [[Telegram::Bot::Types::InlineKeyboardButton.new(text: "Винен 👎🏻", callback_data: 'tribunal_yes'),
							# Telegram::Bot::Types::InlineKeyboardButton.new(text: "Не винен 👍🏻", callback_data: 'tribunal_no')],
							# Telegram::Bot::Types::InlineKeyboardButton.new(text: "🤷🏻‍♂️", callback_data: 'tribunal_cancel_vote'),
							Telegram::Bot::Types::InlineKeyboardButton.new(text: "Не винен 😇", callback_data: 'tribunal_no')],
							 [Telegram::Bot::Types::InlineKeyboardButton.new(text: "Скасувати суд 👽", callback_data: 'tribunal_cancel_tribunal')],
						 # [Telegram::Bot::Types::InlineKeyboardButton.new(text: "#{symbol_delete} Закрити", callback_data: 'delete_this_message')]]
	]
	kb = Telegram::Bot::Types::InlineKeyboardMarkup.new(inline_keyboard: inline_kb)
	return kb
end

def tribunal_finish_kb
	inline_kb = [[Telegram::Bot::Types::InlineKeyboardButton.new(text: "#{symbol_delete} Закрити", callback_data: 'delete_this_message')]]
	kb = Telegram::Bot::Types::InlineKeyboardMarkup.new(inline_keyboard: inline_kb)
	return kb
end

def tribunal_parse(bot, message)
	tribunal_vote_parse(bot, message)
	if tribunal_check_auto_expire(bot, message)
		tribunal = Tribunal.find_by({id: get_chat_id(message)})
		text = "Суд сплив за терміном давності"
		if tribunal.reason.nil?
			text = "#{mention(tribunal.plaintiff_id)} хоче засудити #{mention(tribunal.defendant_id)} на #{symbol_carma(message.chat.id, message.from.id)}<code>#{tribunal.penalty_int}</code>, бо він москаль\n\n#{text}"
		else
			text = "#{mention(tribunal.plaintiff_id)} хоче засудити #{mention(tribunal.defendant_id)} на #{symbol_carma(message.chat.id, message.from.id)}<code>#{tribunal.penalty_int}</code> за #{tribunal.reason}\n\n#{text}"
		end
		info_message_id = tribunal.info_message_id
		tribunal.destroy
		bot.api.editMessageText(chat_id: get_chat_id(message), reply_markup: delete_kb_tribunal_finish, message_id: info_message_id, parse_mode: 'HTML', text: text)
	end
end

def tribunal_vote_parse(bot, message)
	case message
		when Telegram::Bot::Types::CallbackQuery
			case message.data
				when 'tribunal_yes'
					# bot.api.answerCallbackQuery(callback_query_id: message.id, text: "Ви проголосували за ")
					tribunal_add_yes(bot, message)
				when 'tribunal_no'
					# bot.api.answerCallbackQuery(callback_query_id: message.id, text: "Ви проголосували проти")
					tribunal_add_no(bot, message)
				when 'tribunal_cancel_vote'
					tribunal_cancel_vote(bot, message)
				when 'tribunal_cancel_tribunal'
					tribunal_cancel_tribunal(bot, message)
			end
		when Telegram::Bot::Types::Message
			case message.text
				when /\A(\/?tribunal|трибунал|суд|віче|cel)((\s*\d)|\Z)/i
					tribunal_begin(bot, message)
				else
					return
			end
	end
	full = tribunal_check_full(bot, message)
	if full
		case message
			when Telegram::Bot::Types::CallbackQuery
				message_id = message.message.message_id
				chat_id = message.message.chat.id
				user_id = message.from.id
			when Telegram::Bot::Types::Message
				message_id = message.message_id
				chat_id = message.chat.id
				user_id = message.from.id
			when Hash
				message_id = message['result']['message_id']
				chat_id = message['result']['chat']['id']
				user_id = message['result']['from']['id']
		end
		tribunal = Tribunal.find_by({id: chat_id})
		guilt = nil
		if tribunal.yes.size > tribunal.no.size
			execution = "винен"
			guilt = true
		else
			execution = "не винен"
			guilt = false
		end
		initiator = User.create_or_find_by({id: tribunal.plaintiff_id})
		victim = User.create_or_find_by({id: tribunal.defendant_id})

		# if victim.id == $bot_id
		# 	mention(victim) = '<a href="tg://user?id='+victim.id.to_s+'">'+"Лемур</a>"
		# else
		# 	mention(victim) = '<a href="tg://user?id='+victim.id.to_s+'">'+"#{victim.last.fullname}</a>"
		# end
		# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
		if guilt
			penalty_raw = [[tribunal.defendant_id, tribunal.penalty_int]]
			reward_raw = [[tribunal.plaintiff_id, (tribunal.penalty_int / 2.0).ceil]]
			jury_reward = (tribunal.penalty_int / 2.0 / tribunal.yes.size).ceil
			tribunal.yes.each do |x|
				reward_raw.push([x,jury_reward])
			end
		else
			penalty_raw = [[tribunal.plaintiff_id, (tribunal.penalty_int / 2.0).ceil]]
			reward_raw = []
			jury_reward = (tribunal.penalty_int / 2.0 / tribunal.no.size).ceil
			tribunal.no.each do |x|
				reward_raw.push([x,jury_reward])
			end
		end

		reward_raw.each do |x|
			user = User.create_or_find_by({id: x[0]})
			user.carma += x[1]
		end

		penalty_raw.each do |x|
			user = User.create_or_find_by({id: x[0]})
			user.carma -= x[1]
		end

		penalty = penalty_raw.map do |x|
			user = mention(x[0])
			value = "-<code>#{x[1]}</code>#{symbol_carma(message.chat.id, message.from.id)}"
			[user, value]
		end

		reward = reward_raw.map do |x|
			user = mention(x[0])
			value = "+<code>#{x[1]}</code>#{symbol_carma(message.chat.id, message.from.id)}"
			[user, value]
		end
		if guilt
			disconcents = tribunal.no
		else
			disconcents = tribunal.yes
		end
		if tribunal.reason.nil?
			text = "#{mention(initiator)} #{guilt ? "засудив" : "не зміг засудити"} #{mention(victim)} на #{symbol_carma(message.chat.id, message.from.id)}<code>#{tribunal.penalty_int}</code>, бо він москаль"
		else
			text = "#{mention(initiator)} #{guilt ? "засудив" : "не зміг засудити"} #{mention(victim)} на #{symbol_carma(message.chat.id, message.from.id)}<code>#{tribunal.penalty_int}</code> за #{tribunal.reason}"
		end

		unless reward.empty?
			reward.map! do |x|
				x.join(" ")
			end
			reward_text = reward.join("\n")
			text = "#{text}\nВинагороди:\n#{reward_text}"
		end

		unless penalty.empty?
			penalty.map! do |x|
				x.join(" ")
			end
			penalty_text = penalty.join("\n")
			text = "#{text}\nШтрафи:\n#{penalty_text}"
		end

		unless disconcents.empty?
			disconcents_mention = []
			disconcents.each do |x|
				user = User.create_or_find_by({id: x})
				text_mention = mention(user)
				disconcents_mention.push(text_mention)
			end
			text = text + "\nПротестуючі: #{disconcents_mention.join(", ")}"
		end

		# unless tribunal.yes.empty?
		# 	yes_mentions = []
		# 	tribunal.yes.each do |x|
		# 		user = User.create_or_find_by({id: x})
		# 		text_mention = mention(user)
		# 		yes_mentions.push(text_mention)
		# 	end
		# 	text = text + "\nЗгодні: #{yes_mentions.join(", ")}"
		# end
		#
		# unless tribunal.no.empty?
		# 	no_mentions = []
		# 	tribunal.no.each do |x|
		# 		user = User.create_or_find_by({id: x})
		# 		text_mention = mention(user)
		# 		no_mentions.push(text_mention)
		# 	end
		# 	text = text + "\nНе згодні: #{no_mentions.join(", ")}"
		# end

		full_text = text + "\n\nТехнічні дані, не показуватимуться після повноцінної реалізації функції👇🏻
(rewrited)
Прочитано із бази даних
Чат: #{tribunal.id}
Час активації: #{Time.at(tribunal.timestamp)}
Проголосували за винен: #{tribunal.yes}
Проголосували за не винен: #{tribunal.no}
ID цього повідомлення: #{tribunal.info_message_id}
Розмір штрафу: #{tribunal.penalty_int}
Позивач: #{tribunal.plaintiff_id}
Відповідач: #{tribunal.defendant_id}
Причина: #{tribunal.reason}"
	# bot.api.editMessageText(chat_id: chat_id, reply_markup: tribunal_finish_kb, message_id: tribunal.info_message_id, parse_mode: 'HTML', text: text)
	# bot.api.editMessageText(chat_id: chat_id, reply_markup: delete_kb, message_id: tribunal.info_message_id, parse_mode: 'HTML', text: full_text)
	# sleep(3)
	bot.api.editMessageText(chat_id: chat_id, reply_markup: delete_kb_tribunal_finish, message_id: tribunal.info_message_id, parse_mode: 'HTML', text: text)
		tribunal.destroy
	end
end

def tribunal_check_full(bot, message)
	case message
		when Telegram::Bot::Types::CallbackQuery
			message_id = message.message.message_id
			chat_id = message.message.chat.id
			user_id = message.from.id
		when Telegram::Bot::Types::Message
			message_id = message.message_id
			chat_id = message.chat.id
			user_id = message.from.id
		when Hash
			message_id = message['result']['message_id']
			chat_id = message['result']['chat']['id']
			user_id = message['result']['from']['id']
	end
	tribunal = Tribunal.find_by({id: chat_id})
	return false if tribunal.nil?
	yes_count = tribunal.yes.size
	no_count = tribunal.no.size
	total_count = yes_count + no_count
	if total_count >= tribunal.limit
		tribunal.finished = true
	end
	return tribunal.finished
end

def tribunal_add_yes(bot, message)
	tribunal = Tribunal.find_by({id: get_chat_id(message)})
	return if tribunal.nil?
	tribunal.update_timeout
	# Якщо хтось примудриться натиснути кнопку голосу при не працюючому суді
	return if tribunal.nil?
	if tribunal.plaintiff_id == message.from.id or tribunal.defendant_id == message.from.id
		respond(bot, message, "Ви не маєте права брати участь у голосуванні")
		return
	end
	result = tribunal_add_yes_db(bot, message)
	if result == true
		tribunal_rewrite(bot, message)
	end
end

def tribunal_add_yes_db(bot, message)
	case message
		when Telegram::Bot::Types::CallbackQuery
			message_id = message.message.message_id
			chat_id = message.message.chat.id
			user_id = message.from.id
		when Telegram::Bot::Types::Message
			message_id = message.message_id
			chat_id = message.chat.id
			user_id = message.from.id
		when Hash
			message_id = message['result']['message_id']
			chat_id = message['result']['chat']['id']
			user_id = message['result']['from']['id']
	end
	tribunal = Tribunal.find_by({id: chat_id})
	return false if tribunal.nil?
	yes_arr = tribunal.yes
	no_arr = tribunal.no

	# Костиль
	if yes_arr != yes_arr.uniq
		tribunal.yes = yes_arr.uniq
		tribunal_rewrite(bot, message)
	end

	if no_arr.include?(user_id)
		no_arr.delete(user_id)
		tribunal.no = no_arr
	end

	if yes_arr.include?(user_id)
		respond(bot, message, "Ви вже проголосували за")
		return false
	end

	yes_arr.push(user_id)
	tribunal.yes = yes_arr
	return true
end

def tribunal_add_no(bot, message)
	tribunal = Tribunal.find_by({id: get_chat_id(message)})
	return if tribunal.nil?
	tribunal.update_timeout
	return if tribunal.nil?
	if tribunal.plaintiff_id == message.from.id or tribunal.defendant_id == message.from.id
		respond(bot, message, "Ви не маєте права брати участь у голосуванні")
		return
	end
	result = tribunal_add_no_db(bot, message)
	if result == true
		tribunal_rewrite(bot, message)
	end
end

def tribunal_add_no_db(bot, message)
	case message
		when Telegram::Bot::Types::CallbackQuery
			message_id = message.message.message_id
			chat_id = message.message.chat.id
			user_id = message.from.id
		when Telegram::Bot::Types::Message
			message_id = message.message_id
			chat_id = message.chat.id
			user_id = message.from.id
		when Hash
			message_id = message['result']['message_id']
			chat_id = message['result']['chat']['id']
			user_id = message['result']['from']['id']
	end
	tribunal = Tribunal.find_by({id: chat_id})
	return false if tribunal.nil?
	yes_arr = tribunal.yes
	no_arr = tribunal.no

	# Костиль
	if no_arr != no_arr.uniq
		tribunal.no = no_arr.uniq
		tribunal_rewrite(bot, message)
	end

	if yes_arr.include?(user_id)
		yes_arr.delete(user_id)
		tribunal.yes = yes_arr
	end

	if no_arr.include?(user_id)
		respond(bot, message, "Ви вже проголосували проти")
		return false
	end

	no_arr.push(user_id)
	tribunal.no = no_arr
	return true
end

def tribunal_cancel_vote(bot, message)
	tribunal = Tribunal.find_by({id: get_chat_id(message)})
	if tribunal.plaintiff_id == message.from.id or tribunal.defendant_id == message.from.id
		respond(bot, message, "Ви не маєте права брати участь у голосуванні")
		return
	end
	result = tribunal_cancel_vote_db(bot, message)
	if result == true
		tribunal_rewrite(bot, message)
	end
end

def tribunal_cancel_tribunal(bot, message)
	case message
		when Telegram::Bot::Types::CallbackQuery
			message_id = message.message.message_id
			chat_id = message.message.chat.id
			user_id = message.from.id
		when Telegram::Bot::Types::Message
			message_id = message.message_id
			chat_id = message.chat.id
			user_id = message.from.id
		when Hash
			message_id = message['result']['message_id']
			chat_id = message['result']['chat']['id']
			user_id = message['result']['from']['id']
	end

	admins = get_admins(bot, message)

	if admins.include?(message.from.id) or message.from.id == $admin_id
		tribunal = Tribunal.create_or_find_by({id: chat_id})
		initiator = User.create_or_find_by({id: tribunal.plaintiff_id})
		victim = User.create_or_find_by({id: tribunal.defendant_id})

		text = "#{mention(message.from)} скасував суд"
		if tribunal.reason.nil?
			text = "#{mention(initiator)} хоче засудити #{mention(victim)} на #{symbol_carma(chat_id, user_id)}<code>#{tribunal.penalty_int}</code>, бо він москаль\n\n#{text}"
		else
			text = "#{mention(initiator)} хоче засудити #{mention(victim)} на #{symbol_carma(chat_id, user_id)}<code>#{tribunal.penalty_int}</code> за #{tribunal.reason}\n\n#{text}"
		end
		info_message_id = tribunal.info_message_id
		tribunal.destroy
		bot.api.editMessageText(chat_id: chat_id, reply_markup: delete_kb_tribunal_finish, message_id: info_message_id, parse_mode: 'HTML', text: text)

	else
		respond(bot, message, "Скасувати суд може тільки адміністрація чату")
	end
end

def tribunal_cancel_vote_db(bot, message)
	case message
		when Telegram::Bot::Types::CallbackQuery
			message_id = message.message.message_id
			chat_id = message.message.chat.id
			user_id = message.from.id
		when Telegram::Bot::Types::Message
			message_id = message.message_id
			chat_id = message.chat.id
			user_id = message.from.id
		when Hash
			message_id = message['result']['message_id']
			chat_id = message['result']['chat']['id']
			user_id = message['result']['from']['id']
	end
	tribunal = Tribunal.find_by({id: chat_id})
	return false if tribunal.nil?
	yes_arr = tribunal.yes
	no_arr = tribunal.no
	
	result = false
	
	if yes_arr.include?(user_id)
		yes_arr.delete(user_id)
		tribunal.yes = yes_arr
		result = true
	end

	if no_arr.include?(user_id)
		no_arr.delete(user_id)
		tribunal.no = no_arr
		result = true
	end

	return result
end

def tribunal_new_record(message, plaintiff, defendant, penalty, reason)
	case message
		when Telegram::Bot::Types::CallbackQuery
			#
		when Telegram::Bot::Types::Message
			tribunal = Tribunal.create_or_find_by({id: message.chat.id})
			tribunal.info_message_id = message.message_id
		when Hash
			tribunal = Tribunal.create_or_find_by({id: message['result']['chat']['id']})
			tribunal.info_message_id = message['result']['message_id']
	end
	tribunal.timestamp = Time.now.to_i
	tribunal.yes = []
	tribunal.no = []
	tribunal.penalty_int = penalty
	tribunal.plaintiff_id = plaintiff
	tribunal.defendant_id = defendant
	tribunal.reason = reason
	tribunal.expired_at = Time.now.to_i + tribunal_auto_expire(message)
	tribunal.save
end

def tribunal_check_auto_expire(bot, message)
	chat_id = get_chat_id(message)
	tribunal = Tribunal.find_by({id: chat_id})
	return if tribunal.nil?
	tribunal.expired?
end


def tribunal_rewrite(bot, message)
	case message
		when Telegram::Bot::Types::CallbackQuery
			message_id = message.message.message_id
			chat_id = message.message.chat.id
			text = message.message.text.split("\n").first
		when Telegram::Bot::Types::Message
			message_id = message.message_id
			chat_id = message.chat.id
			text = message.text.split("\n").first
		when Hash
			message_id = message['result']['message_id']
			chat_id = message['result']['chat']['id']
			text = message['result']['text'].split("\n").first
	end
	tribunal = Tribunal.find_by({id: chat_id})
	if tribunal.nil?
		bot.api.sendMessage(chat_id: message.chat.id, reply_markup: delete_kb, reply_to_message_id: message.message_id, text: 'Судовий процес не активний')
		return false
	end
	initiator = User.create_or_find_by({id: tribunal.plaintiff_id})
	victim = User.create_or_find_by({id: tribunal.defendant_id})

	# if victim.id == $bot_id
	# 	mention(victim) = '<a href="tg://user?id='+victim.id.to_s+'">'+"Лемур</a>"
	# else
	# 	mention(victim) = '<a href="tg://user?id='+victim.id.to_s+'">'+"#{victim.last.fullname}</a>"
	# end
	text = "Розпочато суд!\nПотрібно голосів: #{tribunal.limit}\nПозивач: #{mention(initiator)}\nВідповідач: #{mention(victim)}\nСума: #{symbol_carma}<code>#{tribunal.penalty_int}</code>\nПричина: "
	if tribunal.reason.nil?
		text = "#{text}(не вказано)"
	else
		text = "#{text}#{tribunal.reason}"
	end
	unless tribunal.yes.empty?
		yes_mentions = []
		tribunal.yes.each do |x|
			user = User.create_or_find_by({id: x})
			text_mention = mention(user)
			yes_mentions.push(text_mention)
		end
		text = text + "\nЗгодні: #{yes_mentions.join(", ")}"
	end

	unless tribunal.no.empty?
		no_mentions = []
		tribunal.no.each do |x|
			user = User.create_or_find_by({id: x})
			text_mention = mention(user)
			no_mentions.push(text_mention)
		end
		text = text + "\nНе згодні: #{no_mentions.join(", ")}"
	end
	
	full_text = text + "\n\nТехнічні дані, не показуватимуться після повноцінної реалізації функції👇🏻
(rewrited)
Прочитано із бази даних
Чат: #{tribunal.id}
Час активації: #{Time.at(tribunal.timestamp)}
Проголосували за винен: #{tribunal.yes}
Проголосували за не винен: #{tribunal.no}
ID цього повідомлення: #{tribunal.info_message_id}
Розмір штрафу: #{tribunal.penalty_int}
Позивач: #{tribunal.plaintiff_id}
Відповідач: #{tribunal.defendant_id}
Причина: #{tribunal.reason}"
	bot.api.editMessageText(chat_id: chat_id, reply_markup: tribunal_kb, message_id: message_id, parse_mode: 'HTML', text: text)
end

def tribunal_begin(bot, message)
	old_tribunal = Tribunal.find_by({id: message.chat.id})
	empty_regexp = /^(?<empty_text>(\/?tribunal(#{$bot_name})?|трибунал|суд|віче|cel))$/i
	empty_match = message.text.match(empty_regexp)

	unless old_tribunal.nil?
		# Тобто суд вже запущено
		if empty_match.nil?
			# Якщо суд з параметрами
			text = 'Спочатку завершіть попередній суд! (напишіть просто <code>суд</code> або натисніть сюди /tribunal)'
			bot.api.sendMessage(chat_id: get_chat_id(message), reply_to_message_id: message.message_id,  parse_mode: 'HTML', reply_markup: delete_kb, text: text)
			return
		else
			# Якщо  суд без параметрів
			# Надсилаємо нове повідомлення із старим судом
			result = tribunal_rewrite_old(bot, old_tribunal, message)
			return result
		end
	else
		# Суд не запущено
		# Ідемо далі
	end
	# tribunal_regexp = /^(?<tribunal_text>(\/?tribunal|трибунал|суд|віче|cel)(\s+)?)$/i
	# tribunal_match = message.text.match(tribunal_regexp)
	no_reason_regexp = /^(?<no_reason_text>(\/?tribunal|трибунал|суд|віче|cel))\s+(?<no_reason_value>\d+)(\s+)?$/i
	no_reason_match = message.text.match(no_reason_regexp)
	unless no_reason_match.nil?
		#	Новий суд без причини
		no_reason = true
	else
		# Новий суд з причиною
		no_reason = false
	end
	main_regexp = /^(?<text>(\/?tribunal|трибунал|суд|віче|cel))\s+(?<value>\d+)\s+(?<reason>.+)$/i
	text_match = message.text.match(main_regexp)
	if text_match.nil?
		# Отримано всі параметри
		if no_reason == false
			bot.api.sendMessage(chat_id: message.chat.id, reply_markup: delete_kb, reply_to_message_id: message.message_id, text: 'Напишіть повідомлення у форматі "суд сума причина"')
			return false
		else
			#
		end
	end
	if no_reason
		reason = ", бо він москаль"
		value = no_reason_match['no_reason_value']
	else
		value = text_match['value']
		reason = text_match['reason']
		text = text_match['text']
	end
	# if reason.nil?
	# 	bot.api.sendMessage(chat_id: message.chat.id, reply_markup: delete_kb, reply_to_message_id: message.message_id, text: 'Напишіть повідомлення у форматі "суд сума причина"')
	# 	return false
	# end
	if message.reply_to_message.nil?
		bot.api.sendMessage(chat_id: message.chat.id, reply_markup: delete_kb, reply_to_message_id: message.message_id, text: "Кого судитимемо?")
	else

		initiator = message.from
		group_id = message.chat.id
		group = Group.create_or_find_by({id: group_id})
		initiator_user = User.create_or_find_by({id: initiator.id})
		member = get_chat_member(group_id, initiator.id)

		# initiator_level = get_level(member.messages, group.max_messages) #ready
		initiator_level = member.level
		# initiator_level = get_level(group.messages(initiator.id), group.max_messages)
		# puts "Рівень ініціатора: #{initiator_level.id}"
		needed_level = 3
		if initiator_level.id < needed_level
			text = "Щоб подавати в суд, ваш рівень повинен бути не нижче #{needed_level}"
			bot.api.sendMessage(chat_id: message.chat.id, reply_markup: delete_kb, reply_to_message_id: message.message_id, parse_mode: 'HTML', text: text)
			return
		end
		victim = message.reply_to_message.from
		limit = tribunal_get_limit(group_id)
		case initiator.id
			when victim.id
				if no_reason
					text = "#{mention(initiator)} крейзі і хоче засудити сам себе на #{symbol_carma(message.chat.id, message.from.id)}<code>#{value}</code>, бо він москаль\nОперація неможлива. Запис в базу даних не здійснено"
				else
					text = "#{mention(initiator)} крейзі і хоче засудити сам себе на #{symbol_carma(message.chat.id, message.from.id)}<code>#{value}</code> за #{reason}\nОперація неможлива. Запис в базу даних не здійснено"
				end
				text = text + "\n\nЧи може дозволити судити самого себе😏?"
				bot.api.sendMessage(chat_id: message.chat.id, reply_markup: delete_kb, reply_to_message_id: message.message_id, parse_mode: 'HTML', text: text)
			else
				text = "Розпочато суд!\nПотрібно голосів: #{limit}\nПозивач: #{mention(initiator)}\nВідповідач: #{mention(victim)}\nСума: #{symbol_carma(message.chat.id, message.from.id)}<code>#{value}</code>\nПричина: "
				if no_reason
					text = "#{text}(не вказано)"
				else
					text = "#{text}#{reason}"
					# text = "#{mention(initiator)} хоче засудити #{mention(victim)} на #{symbol_carma}<code>#{value}</code> за #{reason}\nПотрібно голосів: #{tribunal_total_limit}"
				end
				taked_message = bot.api.sendMessage(chat_id: message.chat.id, reply_markup: tribunal_kb, reply_to_message_id: message.message_id, parse_mode: 'HTML', text: text)
				if no_reason
					tribunal_new_record(taked_message, initiator.id, victim.id, value, nil)
				else
					tribunal_new_record(taked_message, initiator.id, victim.id, value, reason)
				end

				tribunal = Tribunal.find_by({id: message.chat.id})
		end
	end
end

def tribunal_rewrite_old(bot, tribunal, message)
	if tribunal.plaintiff_id.nil?
		tribunal.destroy
		return
	end
	initiator = User.create_or_find_by({id: tribunal.plaintiff_id})
	victim = User.create_or_find_by({id: tribunal.defendant_id})
	value = tribunal.penalty_int
	# mention(initiator) = '<a href="tg://user?id='+initiator.id.to_s+'">'+"#{initiator.fullname}</a>"
	text = ""
	unless tribunal.yes.empty?
		yes_mentions = []
		tribunal.yes.each do |x|
			user = User.create_or_find_by({id: x})
			text_mention = mention(user)
			yes_mentions.push(text_mention)
		end
		text = text + "\nЗгодні: #{yes_mentions.join(", ")}"
	end

	unless tribunal.no.empty?
		no_mentions = []
		tribunal.no.each do |x|
			user = User.create_or_find_by({id: x})
			text_mention = mention(user)
			no_mentions.push(text_mention)
		end
		text = text + "\nНе згодні: #{no_mentions.join(", ")}"
	end
	case initiator.id
		when victim.id
			text = "#{mention(initiator)} крейзі і хоче засудити сам себе на #{symbol_carma(message.chat.id, message.from.id)}<code>#{value}</code> за #{reason}\nОперація неможлива. Запис в базу даних не здійснено"
			bot.api.sendMessage(chat_id: message.chat.id, reply_markup: delete_kb, reply_to_message_id: message.message_id, parse_mode: 'HTML', text: text)
		else
			# if tribunal.defendant_id == $bot_id
			# 	mention(victim) = '<a href="tg://user?id='+victim.id.to_s+'">'+"Лемур</a>"
			# else
			# 	mention(victim) = '<a href="tg://user?id='+victim.id.to_s+'">'+"#{victim.fullname}</a>"
			# end
			# text = "<a href="tg://user?id=123456789">inline mention of a user#{initiator.fullname}</a> хоче засудити #{victim.fullname} на #{value} за #{reason}"
			text = "Розпочато суд!\nПотрібно голосів: #{tribunal.limit}\nПозивач: #{mention(initiator)}\nВідповідач: #{mention(victim)}\nСума: #{symbol_carma(message.chat.id, message.from.id)}<code>#{value}</code>\nПричина: "
			if tribunal.reason.nil?
				text = "#{text}(не вказано)"
			else
				text = "#{text}#{tribunal.reason}"
			end
			taked_message = bot.api.sendMessage(chat_id: message.chat.id, reply_markup: tribunal_kb, reply_to_message_id: message.message_id, parse_mode: 'HTML', text: text)
			if taked_message.is_a?(Hash)
				bot.api.deleteMessage(chat_id: tribunal.id, message_id: tribunal.info_message_id)
			end
			# tribunal_new_record(taked_message, initiator.id, victim.id, value, reason)
			tribunal = Tribunal.find_by({id: message.chat.id})
			tribunal.info_message_id = taked_message['result']['message_id']
			tribunal.save
			full_text = "#{text}

Технічні дані, не показуватимуться після повноцінної реалізації функції👇🏻
Чат: #{tribunal.id}
Час активації: #{Time.at(tribunal.timestamp)}
Проголосували за винен: #{tribunal.yes}
Проголосували за не винен: #{tribunal.no}
ID цього повідомлення: #{tribunal.info_message_id}
Розмір штрафу: #{tribunal.penalty_int}
Позивач: #{tribunal.plaintiff_id}
Відповідач: #{tribunal.defendant_id}
Причина: #{tribunal.reason}"
			# bot.api.sendMessage(chat_id: message.chat.id, reply_markup: tribunal_kb, reply_to_message_id: message.message_id, parse_mode: 'HTML', text: text)
			# bot.api.editMessageText(chat_id: message.chat.id, reply_markup: tribunal_kb, message_id: taked_message['result']['message_id'], parse_mode: 'HTML', text: text)
	end

end

def tribunal_total_limit
	return 3
end

def tribunal_get_limit(group_id)
	group = Group.create_or_find_by({id: group_id})
	total = group.max_messages
	users = ChatMember.where(chat: group_id).to_a
	users = users.reject do |x|
		x.message_last + 300 < Time.now.to_i
	end
	counter = 0
	users.each do |x|
		member_messages = x.messages
		# member_level = get_level(member_messages, total)
		member_level = x.level
		if member_level.id > 5
			counter +=1
		end
	end
	return tribunal_total_limit
	number = counter - 2
	number -= 1 if number.even?
	default = tribunal_total_limit
	minimum = 3
	maximum = 255
	if number < minimum or number > maximum
		number = default
	end
	return counter
	#temporary!
	# tribunal_total_limit
end