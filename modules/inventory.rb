#!/usr/bin/ruby
class Inventory < ActiveRecord::Base
	#
	# def wedding_he
	# 	self.wedding_he
	# end
	#
	# def wedding_he=(other)
	# 	if other < 0
	# 		return false
	# 	end
	# 	self.wedding_he = other.to_i
	# 	self.save
	# end
	#
	# def wedding_she
	# 	self.wedding_she
	# end
	#
	# def wedding_she=(other)
	# 	if other < 0
	# 		return false
	# 	end
	# 	self.wedding_she = other.to_i
	# 	self.save
	# end
	#
	# def wedding_ring
	# 	self.wedding_ring
	# end
	#
	# def wedding_ring=(other)
	# 	if other < 0
	# 		return false
	# 	end
	# 	self.wedding_ring = other.to_i
	# 	self.save
	# end
	#
	# def wedding_document
	# 	self.wedding_document
	# end
	#
	# def wedding_document=(other)
	# 	if other < 0
	# 		return false
	# 	end
	# 	self.wedding_document = other.to_i
	# 	self.save
	# end
	#
	# def wedding_flowers
	# 	self.wedding_flowers
	# end
	#
	# def wedding_flowers=(other)
	# 	if other < 0
	# 		return false
	# 	end
	# 	self.wedding_flowers = other.to_i
	# 	self.save
	# end
	#
	# def vata_branch
	# 	self.vata_branch
	# end
	#
	# def vata_branch=(other)
	# 	if other < 0
	# 		return false
	# 	end
	# 	self.vata_branch = other.to_i
	# 	self.save
	# end

end
def shop_buy(bot, message, bname)
	chat_id = get_chat_id(message)
	user_id = message.from.id
	user = User.create_or_find_by({id: user_id})
	inv = Inventory.create_or_find_by({id: message.from.id})
	ware = get_ware(bname)
	callback_reply = true
	text_reply = true
	callback_reply_not_enought = true
	text_reply_not_enought = false
	if user.enought(ware.price)
		eval "inv.#{ware.bname} += 1"
		inv.save
		user.carma -= ware.price
		if callback_reply
			callback_text = "Ви купили #{ware.emoji}#{ware.rname} за #{symbol_carma(chat_id, user_id)}#{ware.price}"
			bot.api.answerCallbackQuery(callback_query_id: message.id, text: callback_text)
		end
		if text_reply
			text = "#{mention(message.from, true)} придбав #{ware.emoji}#{ware.rname} за #{symbol_carma(chat_id, user_id)}<code>#{ware.price}</code>"
			bot.api.sendMessage(chat_id: get_chat_id(message), reply_to_message_id: message.message.message_id,  parse_mode: 'HTML', reply_markup: delete_kb, text: text)
		end
	else
		if callback_reply_not_enought
			callback_text = "Не вистачає #{symbol_carma(chat_id, user_id)}#{ware.price - user.carma}"
			bot.api.answerCallbackQuery(callback_query_id: message.id, text: callback_text)
		end
		if text_reply_not_enought
			text = "Не вистачає #{symbol_carma(chat_id, user_id)}<code>#{ware.price - user.carma}</code>"
			bot.api.sendMessage(chat_id: get_chat_id(message), reply_to_message_id: message.message.message_id,  parse_mode: 'HTML', reply_markup: delete_kb, text: text)
		end
	end
end

def shop_parse(bot, message)
	return 0
	chat_id = get_chat_id(message)
	user_id = message.from.id
	message_id = get_message_id(message)
	case message
		when Telegram::Bot::Types::CallbackQuery
			if ware_array.include?(message.data)
				shop_buy(bot, message, message.data)
			end
			if message.data == 'inventory_reload'
				inv = Inventory.create_or_find_by({id: message.from.id})
				if all_wares.size == 0
					# text = ["#{mention(user_id,true)}, ви ще нічого не купили"]
					# do nothing
				else
					text = []
					text.push("#{mention(user_id,true)}, ваш інвентар:")
					all_wares.each do |ware|
						text_eval = "inv.#{ware.bname}"
						count = eval(text_eval)
						foo = "[#{count}]#{ware.emoji}#{ware.name}"
						if count > 0
							text.push(foo)
						end
					end
					new = text.last(text.size - 1).join("\n")
					old_arr = message.message.text.split("\n")
					old = old_arr.last(old_arr.size - 1).join("\n")
					unless old == new
						begin
							bot.api.editMessageText(chat_id: chat_id, reply_markup: inventory_kb, message_id: message_id, parse_mode: 'HTML', text: text.join("\n"))
						rescue
							# nothing
						end
					end
				end
				begin
					bot.api.answerCallbackQuery(callback_query_id: message.id, text: "Оновлено")
				rescue
					# nothing
				end
			end
		when Telegram::Bot::Types::Message
			# return unless test_groups(chat_id)
			case message.text
				when /^\A((магазин)|(\/?store(#{$bot_name})?))\Z/i
					text = []
					text.push("Ласкаво просимо в магазин, він же /store.")
					text.push("Щоб переглянути список своїх покупок, напишіть \"інвентар\" або /inventory")
					all_wares.each do |ware|
						text.push("#{ware.emoji}#{ware.name} - #{symbol_carma(chat_id, user_id)}<code>#{ware.price}</code>")
					end
					bot.api.sendMessage(chat_id: get_chat_id(message), reply_to_message_id: message.message_id,  parse_mode: 'HTML', reply_markup: store_kb, text: text.join("\n"))
				when /^\A((інвентар)|(\/?inventory(#{$bot_name})?))\Z/i
					if message.reply_to_message.nil?
						inv = Inventory.create_or_find_by({id: message.from.id})
						reply_id = message.message_id
					else
						inv = Inventory.create_or_find_by({id: message.reply_to_message.from.id})
						reply_id = message.reply_to_message.message_id
						user_id = message.reply_to_message.from.id
					end
					if all_wares.size == 0
						text = ["#{mention(user_id,true)}, ви ще нічого не купили"]
					else
						text = []
						text.push("#{mention(user_id,true)}, ваш інвентар:")
						all_wares.each do |ware|
							text_eval = "inv.#{ware.bname}"
							count = eval(text_eval)
							foo = "[#{count}]#{ware.emoji}#{ware.name}"
							if count > 0
								text.push(foo)
							end
						end
					end
					bot.api.sendMessage(chat_id: get_chat_id(message), reply_to_message_id: reply_id,  parse_mode: 'HTML', reply_markup: inventory_kb, text: text.join("\n"))
			end
	end
end

def store_kb
	inline_kb = [[Telegram::Bot::Types::InlineKeyboardButton.new(text: "#{wedding_he}", callback_data: 'wedding_he'),
	Telegram::Bot::Types::InlineKeyboardButton.new(text: "#{wedding_she}", callback_data: 'wedding_she'),
	Telegram::Bot::Types::InlineKeyboardButton.new(text: "#{wedding_ring}", callback_data: 'wedding_ring')],
	[Telegram::Bot::Types::InlineKeyboardButton.new(text: "#{wedding_document}", callback_data: 'wedding_document'),
	Telegram::Bot::Types::InlineKeyboardButton.new(text: "#{wedding_flowers}", callback_data: 'wedding_flowers'),
	Telegram::Bot::Types::InlineKeyboardButton.new(text: "#{vata_branch}", callback_data: 'vata_branch')],
	[Telegram::Bot::Types::InlineKeyboardButton.new(text: "#{symbol_delete}Закрити", callback_data: 'delete_this_message')]
	]
	kb = Telegram::Bot::Types::InlineKeyboardMarkup.new(inline_keyboard: inline_kb)
	return kb
end

def inventory_kb
	inline_kb =  [[Telegram::Bot::Types::InlineKeyboardButton.new(text: "#{symbol_restart}Оновити", callback_data: 'inventory_reload')],
	[Telegram::Bot::Types::InlineKeyboardButton.new(text: "#{symbol_delete}Закрити", callback_data: 'delete_this_message')]]
	kb = Telegram::Bot::Types::InlineKeyboardMarkup.new(inline_keyboard: inline_kb)
	return kb
end

def wedding_he
	# Костюм нареченого
	return '🤵🏻'
end

def wedding_she
	# Костюм нареченої
	return '👰🏻'
end

def wedding_ring
	# Весільна обручка
	return '💍'
end

def wedding_document
	# Свідоцтво про шлюб
	return '📃'
end

def wedding_flowers
	# Букет квітів
	return '💐'
end

def vata_branch
	# Гілляка
	return '🎋'
	return '🌴'
end

def witness_he
	return '👨🏻'
end

def witness_she
	return '👩🏼'
end

def wedding_guest
	return '👀'
end

def wedding_registrator
	return '🧑🏻‍💼'
	end

def wedding_signature
	return '🖊'
end

def wedding_abort
	return '💔'
end
