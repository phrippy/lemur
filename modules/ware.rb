#!/usr/bin/ruby
class Ware < ActiveRecord::Base

	def bname
		self.bname_db
	end

	def bname=(other)
		return if other.nil?
		self.bname_db = other
		self.save
	end

	def name
		return self.name if ware_name(self.bname.nil?)
		return nil if self.name_db.nil?
		Base64.strict_decode64(self.name_db).force_encoding('UTF-8')
	end

	def name=(other)
		return if other.nil?
		self.name_db = Base64.strict_encode64(other)
		self.save
	end

	def rname
		return self.name if self.rname_db.nil?
		return nil if self.rname_db.nil?
		Base64.strict_decode64(self.rname_db).force_encoding('UTF-8')
	end

	def rname=(other)
		return if other.nil?
		self.rname_db = Base64.strict_encode64(other)
		self.save
	end

	def emoji
		manual = ware_emoji(self.name)
		if manual.nil?
			return nil if self.emoji_db.nil?
			Base64.strict_decode64(self.emoji_db).force_encoding('UTF-8')
		else
			manual
		end
	end

	def emoji=(other)
		return if other.nil?
		self.emoji_db = Base64.strict_encode64(other)
		self.save
	end

	def price
		manual = ware_price(self.name)
		if manual.nil?
			return nil if self.price_db.nil?
			self.price_db
		else
			manual
		end
	end

	def price=(other)
		return if other.nil?
		self.price_db = other.to_i
		self.save
	end

end

def get_ware(bname)
	ware = Ware.find_by(bname_db: bname)
	if ware.nil?
		return Ware.create(bname_db: bname)
	else
		return ware
	end
end

def all_wares
	Ware.all.to_a
end

