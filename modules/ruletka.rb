#!/usr/bin/ruby
$roulette_deadline = 30 # 5 секунд - для тесту
require File.expand_path(File.dirname(__FILE__) + '/roulette/roulette.rb')
require File.expand_path(File.dirname(__FILE__) + '/roulette/bet.rb')

require './modules/roulette/garbage.rb'
$symbol_eyes = '👀'
$symbol_eye = '👁'
$symbol_red_dot = '🔴'
$symbol_black_dot = '⚫️'
#$symbol_green_dot='🟢'
$symbol_green_dot = '💚'
$symbol_log = '📜'
$symbol_go = '🎲'

def symbol_restart
	#Рандом зі вказуванням імовірнойстей. Чим більше число - тим більша імовірність
	#rubydoc.info/gems/pickup
	arrows_counterclockwise = '🔄'
	return arrows_counterclockwise

	yin_yang = '☯️'
	recycle = '♻️'
	cyclone = '🌀'
	arr = { arrows_counterclockwise => 10, yin_yang => 4, recycle => 7, cyclone => 2 }
	pickup = Pickup.new(arr)
	pickup.pick(1)
end

def symbol_delete
	delete = '❌'
	return delete
end

def delete_kb
	inline_kb = [Telegram::Bot::Types::InlineKeyboardButton.new(text: "#{symbol_delete}Закрити", callback_data: 'delete_this_message')]
	kb = Telegram::Bot::Types::InlineKeyboardMarkup.new(inline_keyboard: inline_kb)
	return kb
end

def settings_kb
	inline_kb = [
		[
			Telegram::Bot::Types::InlineKeyboardButton.new(text: "Додати мене", callback_data: 'spam_notify_yes'),
			Telegram::Bot::Types::InlineKeyboardButton.new(text: "Видалити мене", callback_data: 'spam_notify_no')
		],
		[Telegram::Bot::Types::InlineKeyboardButton.new(text: "#{symbol_delete}Закрити", callback_data: 'delete_this_message')]
	]
	kb = Telegram::Bot::Types::InlineKeyboardMarkup.new(inline_keyboard: inline_kb)
	return kb
end

def delete_kb_tribunal_finish
	inline_kb = [Telegram::Bot::Types::InlineKeyboardButton.new(text: "#{symbol_delete}Закрити через 10 секунд", callback_data: 'delete_this_message_tribunal_finish')]
	kb = Telegram::Bot::Types::InlineKeyboardMarkup.new(inline_keyboard: inline_kb)
	return kb
end

$roulette_color = { 'red' => $symbol_red_dot, 'black' => $symbol_black_dot, 'green' => $symbol_green_dot }

# Замість створення об'єкту типу Telegram::Bot::Types::InlineKeyboardButton можна просто передавати хеш.
# Так коротше і зручніше
$roulette_kb = [
		#black
		[Telegram::Bot::Types::InlineKeyboardButton.new(text: "+1#{$symbol_black_dot}", callback_data: 'roulette_bet_black_1'),
		 Telegram::Bot::Types::InlineKeyboardButton.new(text: "+5#{$symbol_black_dot}", callback_data: 'roulette_bet_black_5'),
		 Telegram::Bot::Types::InlineKeyboardButton.new(text: "x2#{$symbol_black_dot}", callback_data: 'roulette_bet_black_double')
		],

		#red
		[Telegram::Bot::Types::InlineKeyboardButton.new(text: "+1#{$symbol_red_dot}", callback_data: 'roulette_bet_red_1'),
		 Telegram::Bot::Types::InlineKeyboardButton.new(text: "+5#{$symbol_red_dot}", callback_data: 'roulette_bet_red_5'),
		 Telegram::Bot::Types::InlineKeyboardButton.new(text: "x2#{$symbol_red_dot}", callback_data: 'roulette_bet_red_double')
		],

		#green
		[Telegram::Bot::Types::InlineKeyboardButton.new(text: "+1#{$symbol_green_dot}", callback_data: 'roulette_bet_green_1'),
		 Telegram::Bot::Types::InlineKeyboardButton.new(text: "+5#{$symbol_green_dot}", callback_data: 'roulette_bet_green_5'),
		 Telegram::Bot::Types::InlineKeyboardButton.new(text: "x2#{$symbol_green_dot}", callback_data: 'roulette_bet_green_double')
		],

		#],

		#[Telegram::Bot::Types::InlineKeyboardButton.new(text: "#{$symbol_eye}Мої ставки", callback_data: 'roulette_mybets')],
		[Telegram::Bot::Types::InlineKeyboardButton.new(text: "#{$symbol_eye}Баланс", callback_data: 'roulette_get_carma')],

		[Telegram::Bot::Types::InlineKeyboardButton.new(text: "#{$symbol_log}Лог", callback_data: 'roulette_log'),
		 Telegram::Bot::Types::InlineKeyboardButton.new(text: "#{$symbol_go}Крутити", callback_data: 'roulette_go')],
]
$roulette_markup = Telegram::Bot::Types::InlineKeyboardMarkup.new(inline_keyboard: $roulette_kb)

class BotRouletteUnknownNumber < BotException; end

class BotRouletteNotInteger < BotRouletteUnknownNumber; end

class BotRouletteWrongRange < BotRouletteUnknownNumber; end

class BotRouletteUnknownError < BotRouletteUnknownNumber; end

def roulette_check_timeout(bot, message)
	group = get_chat_id(message)
	game = Roulette.create_or_find_by({ id: group })
	if game.locked and game.timeout?
		sleep(rand(3..10))
		garbage = bot.api.sendMessage(chat_id: group, text: 'Кручу рулетку... (10 секунд)')
		sleep(10)
		roulette_add_garbage(garbage)
		roulette_go(bot, garbage)
	end
end

require File.expand_path(File.dirname(__FILE__) + '/roulette/roulette_spin.rb')

def roulette_start(bot, message)
	group = get_chat_id(message)
	game = Roulette.create_or_find_by({ id: group })
	text = "Рулетка (30 сек)\nПідтримуються ставки текстом на:\nчорний/червоний/зеро\nвелике/мале\nпарне/непарне\n1-3 трійки\n1-4 чверті\nчисло\nЧим меший діапазон ставки - тим більший виграш. Вгадаєте число - отримаєте в 36 раз більше ставки"
	case message

	#Start roulette from callback
	when Telegram::Bot::Types::CallbackQuery
		if game.locked
		then
			respond(bot, message, 'Рулетка вже запущена')
		else
			game.locked = true
			game.ingame = true
			game.upd
			game.save
			garbage = bot.api.sendMessage(chat_id: group, reply_markup: $roulette_markup, text: text)
			roulette_add_garbage(garbage)
		end

	#Start roulette from text message
	when Telegram::Bot::Types::Message
		if game.locked
			garbage = bot.api.sendMessage(chat_id: group, reply_markup: $roulette_markup, text: 'Рулетка вже запущена')
			roulette_add_garbage(garbage)
		else
			game.locked = true
			game.ingame = true
			game.upd
			game.save
			garbage = bot.api.sendMessage(chat_id: group, reply_markup: $roulette_markup, text: text)
			roulette_add_garbage(garbage)
		end
		roulette_add_garbage(message)
	end
end

def roulette_stop_bets(group, bot, message)
	game = Roulette.create_or_find_by({ id: group })
	game.ingame = false
	game.save
end

def roulette_spin(group, bot, message)
	game = Roulette.create_or_find_by({id: group})
	spin = case get_chat_id(message)
					#мій пп з ботом
					when 240266956
						RouletteSpin.new(24)
					#моя група для тестування
					when -1001312886836
						RouletteSpin.new(15)
					#стандартний рандом
					else
						RouletteSpin.new
					end
	#garbage = bot.api.sendMessage(chat_id: group, text: "Випало: #{spin.number}#{spin.dot}")
	#roulette_add_garbage(garbage)
	game.ad_log(spin.number)
	spin
end

def roulette_show_log(bot, message)
	group = get_chat_id(message)
	game = Roulette.create_or_find_by({ id: group })
	game.upd
	case message
	when Telegram::Bot::Types::CallbackQuery
		limit = 10
		text = game.flog(limit).join("\n")
		text = '(пусто)' if text.empty?
		#bot.api.answerCallbackQuery(callback_query_id: message.id, text: "#{text}") rescue raise BotSkipMessage
		garbage = bot.api.sendMessage(chat_id: group, text: text)
		roulette_add_garbage(garbage)
	when Telegram::Bot::Types::Message
		#no limit, show all 50
		limit = 20
		text = game.flog(limit).join("\n")
		text = '(пусто)' if text.empty?
		garbage = bot.api.sendMessage(chat_id: group, text: text)
		roulette_add_garbage(garbage)
	end
	roulette_add_garbage(message)
	#bot.api.answerCallbackQuery(callback_query_id: message.id, text: "Не реалізовано") rescue raise BotSkipMessage
	#roulette_log(bot, message)
end

def roulette_get_winners(spin, game)
	result = {}
	game.gamers.each do |user|
		result[user] = [0, 0]
	end
	case spin.color
		when 'red'
			winners = game.red
			winners.each do |a, b|
				result[a][0] = b
				result[a][1] = b + b * 1
			end
		when 'black'
			winners = game.black
			winners.each do |a, b|
				result[a][0] = b
				result[a][1] = b + b * 1
			end
		when 'green'
			winners = game.green
			winners.each do |a, b|
				result[a][0] = b
				result[a][1] = b + b * 35
			end
		else
			raise BotException
	end
	#todo
	case spin.bigly
		when 'big'
			winners = game.big
			winners.each do |a, b|
				result[a][0] = b
				result[a][1] = b + b * 1
				end
		when 'small'
			winners = game.small
			winners.each do |a, b|
				result[a][0] = b
				result[a][1] = b + b * 1
				end
	end
	case spin.parity
		when 'odd'
			winners = game.odd
			winners.each do |a, b|
				result[a][0] = b
				result[a][1] = b + b * 1
			end
		when 'even'
			winners = game.even
			winners.each do |a, b|
				result[a][0] = b
				result[a][1] = b + b * 1
			end
	end

	case spin.trio
		when 'trio1'
			winners = game.trio1
			winners.each do |a, b|
				result[a][0] = b
				result[a][1] = b + b * 3
			end
		when 'trio2'
			winners = game.trio2
			winners.each do |a, b|
				result[a][0] = b
				result[a][1] = b + b * 3
			end
		when 'trio3'
			winners = game.trio3
			winners.each do |a, b|
				result[a][0] = b
				result[a][1] = b + b * 3
			end
	end

	case spin.kvar
		when 'kvar1'
			winners = game.kvar1
			winners.each do |a, b|
				result[a][0] = b
				result[a][1] = b + b * 2
			end
		when 'kvar2'
			winners = game.kvar2
			winners.each do |a, b|
				result[a][0] = b
				result[a][1] = b + b * 2
			end
		when 'kvar3'
			winners = game.kvar3
			winners.each do |a, b|
				result[a][0] = b
				result[a][1] = b + b * 2
			end
		when 'kvar4'
			winners = game.kvar4
			winners.each do |a, b|
				result[a][0] = b
				result[a][1] = b + b * 2
			end
	end
	game.raw(type: spin.number).each
	winners = game.raw(type: spin.number)
	winners.each do |a, b|
		result[a][0] = b
		result[a][1] = b + b * 35
	end
	result.each do |user, inhash|
		result.delete(user) if inhash[1] == 0
	end
	result
end

def roulette_reward_winners(bot, group, winners)
	winners.each do |user, value|
		reward = value[1]
		user_add_carma(user, reward)
	end

end


# def roulette_show_winners(spin, group, bot)
# 	winners = roulette_get_winners(spin, group)
# 	winners.each do |user, value|
# 		bet = value[0]
# 		reward = value[1]
# 		garbage = bot.api.sendMessage(chat_id: group, text: "#{fullname(bot, user, group)} поставив #{bet} на #{spin.dot} і виграв #{reward}")
# 		roulette_add_garbage(garbage)
# 	end
# end

def roulette_show_winners_finish(spin, game, bot)
	group = game.id
	winners = roulette_get_winners(spin, game)
	text = []
	return 'Ніхто не виграв' if winners.empty?

	winners.each do |user, value|
		#bet = value[0]
		bet = game.total(user)
		reward = value[1]
		stonk = reward - bet
		stonk = if stonk >= 0
							"+#{stonk}"
						else
								stonk.to_s
						end
		text.push "#{fullname(bot, user, group)} виграв #{reward}#{symbol_carma(group)} (#{stonk})"
	end
	text.reject(&:blank?)
end

def roulette_summary(bot, group, spin)
	game = Roulette.create_or_find_by({ id: group })
	text = []
	hash = game.black
	hash.each do |a, b|
		text.push "#{fullname(bot, a, group)} поставив #{b} на #{$symbol_black_dot}"
	end
	hash = game.red
	hash.each do |a, b|
		text.push "#{fullname(bot, a, group)} поставив #{b} на #{$symbol_red_dot}"
	end
	hash = game.green
	hash.each do |a, b|
		text.push "#{fullname(bot, a, group)} поставив #{b} на #{$symbol_green_dot}"
	end
	hash = game.big
	hash.each do |a, b|
		text.push "#{fullname(bot, a, group)} поставив #{b} на велике (19-36)"
	end
	hash = game.small
	hash.each do |a, b|
		text.push "#{fullname(bot, a, group)} поставив #{b} на маленьке (1-18)"
	end
	hash = game.odd
	hash.each do |a, b|
		text.push "#{fullname(bot, a, group)} поставив #{b} на непарне (1,3,5...)"
	end
	hash = game.even
	hash.each do |a, b|
		text.push "#{fullname(bot, a, group)} поставив #{b} на парне (2,4,6...)"
	end
	hash = game.trio1
	hash.each do |a, b|
		text.push "#{fullname(bot, a, group)} поставив #{b} на першу трійку (1-12)"
	end
	hash = game.trio2
	hash.each do |a, b|
		text.push "#{fullname(bot, a, group)} поставив #{b} на другу трійку (13-24)"
	end
	hash = game.trio3
	hash.each do |a, b|
		text.push "#{fullname(bot, a, group)} поставив #{b} на третю трійку (25-36)"
	end
	hash = game.kvar1
	hash.each do |a, b|
		text.push "#{fullname(bot, a, group)} поставив #{b} на першу чверть (1-9)"
	end
	hash = game.kvar2
	hash.each do |a, b|
		text.push "#{fullname(bot, a, group)} поставив #{b} на другу чверть (10-18)"
	end
	hash = game.kvar3
	hash.each do |a, b|
		text.push "#{fullname(bot, a, group)} поставив #{b} на третю чверть (19-27)"
	end
	hash = game.kvar4
	hash.each do |a, b|
		text.push "#{fullname(bot, a, group)} поставив #{b} на четверту чверть (28-36)"
	end
	hash = game.raw
	hash.each do |number, inhash|
		hash[number].each do |user, db_bet|
		text.push "#{fullname(bot, user, group)} поставив #{db_bet} на #{number}"
		end
	end
	text.push "\nРулетка: #{spin.number}#{spin.dot}\n"
	text.push(roulette_show_winners_finish(spin, game, bot))
	text.reject(&:blank?)
	roulette_new_game_kb = [[Telegram::Bot::Types::InlineKeyboardButton.new(text: "#{symbol_restart}Ще раз", callback_data: 'roulette_new_game')],
													[Telegram::Bot::Types::InlineKeyboardButton.new(text: "#{symbol_delete}Закрити", callback_data: 'delete_this_message')]]
	roulette_new_game_markup = Telegram::Bot::Types::InlineKeyboardMarkup.new(inline_keyboard: roulette_new_game_kb)
	#Або розгорнутіше:
	#text.reject do |x|
	#	x.blank?
	#end
	bot.api.sendMessage(chat_id: group, reply_markup: roulette_new_game_markup, text: text.join("\n"))
end

def roulette_finish(bot, group)
	game = Roulette.create_or_find_by({ id: group })
	game.bets_black_arr = Hash.new.to_json
	game.bets_red_arr = Hash.new.to_json
	game.bets_green_arr = Hash.new.to_json
	game.bets_big_arr = Hash.new.to_json
	game.bets_small_arr = Hash.new.to_json
	game.bets_odd_arr = Hash.new.to_json
	game.bets_even_arr = Hash.new.to_json
	game.bets_trio1_arr = Hash.new.to_json
	game.bets_trio2_arr = Hash.new.to_json
	game.bets_trio3_arr = Hash.new.to_json
	game.bets_kvar1_arr = Hash.new.to_json
	game.bets_kvar2_arr = Hash.new.to_json
	game.bets_kvar3_arr = Hash.new.to_json
	game.bets_kvar4_arr = Hash.new.to_json
	game.bets_raw_arr = Hash.new.to_json

	#garbage = bot.api.sendMessage(chat_id: group, text: "Очищаю ставки")
	#roulette_add_garbage(garbage)
	game.locked = false
	#garbage = bot.api.sendMessage(chat_id: group, text: "Зупиняю гру")
	#roulette_add_garbage(garbage)
	game.save
end

def roulette_go(bot, message)
	#Дізнаюсь ідентифікатор групи
	group = get_chat_id(message) #method from ${bot}/lib.rb
	game = Roulette.create_or_find_by({ id: group })
	initiator_id = get_initiator_id(message)
	if initiator_id == $bot_id
	#Якщо крутіння рулетки ініціював сам бот
	else
		if game.locked
			game.locked = false
			game.save
		else
			respond(bot, message, 'Спочатку треба запустити гру')
			raise BotSkipMessage
		end
		begin
			garbage = respond(bot, message, 'Поїхали!')
			roulette_add_garbage(garbage)
		rescue StandardError
			true
		end
	end
	#Зупиняю прийом ставок
	roulette_stop_bets(group, bot, message)

	#Кручу рулетку
	spin = roulette_spin(group, bot, message)
	winners = roulette_get_winners(spin, game)

	#Нагороджую переможців
	roulette_reward_winners(bot, group, winners)

	#Надсилаю повідомлення з результатами
	roulette_summary(bot, group, spin)

	#Завершую гру
	roulette_finish(bot, group)

	#Видаляю всі сервісні повідомлення
	roulette_clear_garbage(bot, group)
	raise BotSkipMessage
end

def roulette_check (bot, message)
	game = Roulette.create_or_find_by({id: message.chat.id})
	return false unless game.ingame

	if Time.now.to_i > game.deadline
		bot.api.sendMessage(chat_id: message.chat.id, text: 'Термін гри в рулетку сплив')
		roulette_finish(bot, message)
		return false
	end

	return false if message.text.nil?

	return unless message.text.match(/#{game.word}/i)

	if message.from.id == game.explainer_id
		penalty = game.word.size * 100
		user_penalty(bot, message, penalty)
		bot.api.sendMessage(chat_id: message.chat.id, reply_to_message_id: message.message_id, text: "#{$symbol_user}#{message.from.first_name} був оштрафований на #{symbol_carma(message.chat.id, message.from.id)}#{penalty}")
	else
		reward = game.word.size * 10
		user_reward(bot, message, reward)
		eye = '👁'
		disable = '❌'
		restart = '🔄'
		kb = [
				[Telegram::Bot::Types::InlineKeyboardButton.new(text: "#{restart}Перезапустити", callback_data: 'krokodile_restart')],
		]
		markup = Telegram::Bot::Types::InlineKeyboardMarkup.new(inline_keyboard: kb)
		bot.api.sendMessage(chat_id: message.chat.id, reply_to_message_id: message.message_id, reply_markup: markup, text: "#{$symbol_user}#{message.from.first_name} вгадав слово \"#{game.word}\" і отримав #{symbol_carma(message.chat.id, message.from.id)}#{reward}")
		krokodile_finish(bot, message)
	end
end


def make_bet(bot, message, bet, game, user)
	temp_user = User.create_or_find_by({ id: user })
	unless game.locked
		respond(bot, message, 'Спочатку треба запустити гру')
		raise BotSkipMessage
	end
	return false unless temp_user.enought(bet.quantity)
	case bet.type.to_s
		when 'black'
			current = game.black(user)
			updated = if bet.double?
									current * 2
								else
									current + bet.quantity
								end
			game.black_set(user, updated)
		when 'red'
			current = game.red(user)
			updated = if bet.double?
									current * 2
								else
									current + bet.quantity
								end
			game.red_set(user, updated)
		when 'green'
			current = game.green(user)
			updated = if bet.double?
									current * 2
								else
									current + bet.quantity
								end
			game.green_set(user, updated)
		when 'big'
			current = game.big(user)
			updated = if bet.double?
									current * 2
								else
									current + bet.quantity
								end
			game.big_set(user, updated)
		when 'small'
			current = game.small(user)
			updated = if bet.double?
									current * 2
								else
									current + bet.quantity
								end
			game.small_set(user, updated)
		when 'even'
			current = game.even(user)
			updated = if bet.double?
									current * 2
								else
									current + bet.quantity
								end
			game.even_set(user, updated)
		when 'odd'
			current = game.odd(user)
			updated = if bet.double?
									current * 2
								else
									current + bet.quantity
								end
			game.odd_set(user, updated)
		when 'trio1'
			current = game.trio1(user)
			updated = if bet.double?
									current * 2
								else
									current + bet.quantity
								end
			game.trio1_set(user, updated)
		when 'trio2'
			current = game.trio2(user)
			updated = if bet.double?
									current * 2
								else
									current + bet.quantity
								end
			game.trio2_set(user, updated)
		when 'trio3'
			current = game.trio3(user)
			updated = if bet.double?
									current * 2
								else
									current + bet.quantity
								end
			game.trio3_set(user, updated)
		when 'kvar1'
			current = game.kvar1(user)
			updated = if bet.double?
									current * 2
								else
									current + bet.quantity
								end
			game.kvar1_set(user, updated)
		when 'kvar2'
			current = game.kvar2(user)
			updated = if bet.double?
									current * 2
								else
									current + bet.quantity
								end
			game.kvar2_set(user, updated)
		when 'kvar3'
			current = game.kvar3(user)
			updated = if bet.double?
									current * 2
								else
									current + bet.quantity
								end
			game.kvar3_set(user, updated)
		when 'kvar4'
			current = game.kvar4(user)
			updated = if bet.double?
									current * 2
								else
									current + bet.quantity
								end
			game.kvar4_set(user, updated)
		when /([1-9]|[12][0-9]|3[0-6])/
			return false if bet.category != :raw
			current = game.raw(user, type: bet.type)
			updated = if bet.double?
									current * 2
								else
									current + bet.quantity
							end
			game.raw_set(user, updated, bet.type)
	end
	return true
end

def roulette_parse_callback (bot, message)
	case message
		when Telegram::Bot::Types::CallbackQuery
			case message.data
			when /get_carma/
				chat_id = get_chat_id(message)
					bot.api.answerCallbackQuery(callback_query_id: message.id, text: "#{symbol_carma(chat_id, message.from.id)}#{user_get_carma(message.from.id)}")
				when /roulette_go/
					roulette_go(bot, message)
			end
		when Telegram::Bot::Types::Message
			case message.text
				when /^(\/ruletka|рулетка|htktnrf)/i
					bot.api.sendMessage(chat_id: message.chat.id, reply_to_message_id: message.message_id, text: 'Рулетка тимчасово відключена')
			end
			return false
	end
end

def roulette_parse_bet(bot, message)
	user = message.from
	case message
		when Telegram::Bot::Types::CallbackQuery
			group = message.message.chat.id
			game = Roulette.create_or_find_by({ id: group.to_i })
			bet = detect_bet(message)
			unless game.locked
				respond(bot, message, 'Спочатку треба запустити гру')
				raise BotSkipMessage
			end
			return false unless bet
			game.upd
			value = game.get_bet(user.id, type: bet.type)
			result = make_bet(bot, message, bet, game, user.id)
			temp_user = User.create_or_find_by({ id: user.id })
			if result == true
				total_bet = game.get_bet(user.id, type: bet.type)
				if bet.double?
					if total_bet == 0
						bot.api.answerCallbackQuery(callback_query_id: message.id, text: "Щоб подвоїти, спочатку зробіть ставку!")
					else
						bet.quantity = value
						temp_user.penalty(bet.quantity)
						foo = "#{user.fullname} подвоїв ставку(#{value}*2=#{total_bet}) на <b>#{bet.text}</b>#{bet.dot}<b>(</b><code>#{temp_user.carma}</code><b>)</b>"
						bot.api.answerCallbackQuery(callback_query_id: message.id, text: "#{bet.quantity} на #{bet.dot}. Залишилось #{symbol_carma(message.chat.id, message.from.id)}#{temp_user.carma}")
						garbage = bot.api.sendMessage(chat_id: group, parse_mode: 'HTML', text: foo)
						roulette_add_garbage(garbage)
					end
				else
					temp_user.penalty(bet.quantity)
					foo = "#{user.fullname} поставив #{symbol_carma(message.chat.id, message.from.id)}<code>#{total_bet}</code>(+<code>#{bet.quantity}</code>) на <b>#{bet.text}</b>#{bet.dot}<b>(</b><code>#{temp_user.carma}</code><b>)</b>"
					bot.api.answerCallbackQuery(callback_query_id: message.id, text: "#{bet.quantity} на #{bet.dot}. Залишилось #{symbol_carma(message.chat.id, message.from.id)}#{temp_user.carma}")
					garbage = bot.api.sendMessage(chat_id: group, parse_mode: 'HTML', text: foo)
					roulette_add_garbage(garbage)
				end
			else
				#foo = "Не вистачає #{symbol_carma}<code>#{bet - temp_user.carma}</code><b>(</b><code>#{temp_user.carma}</code><b>)</b>"
				bot.api.answerCallbackQuery(callback_query_id: message.id, text: "Не вистачає #{symbol_carma(message.chat.id, message.from.id)}#{bet.quantity - temp_user.carma}[#{temp_user.carma}]")
			end

			game.upd
			unless game.locked
				respond(bot, message, 'Спочатку треба запустити гру')
				raise BotSkipMessage
			end
		when Telegram::Bot::Types::Message
			group = message.chat.id
			game = Roulette.create_or_find_by({ id: group.to_i })
			if game.ingame
				bet = detect_bet(message)
				return false unless bet

				game.upd
				roulette_add_garbage(message)
				unless game.locked
					respond(bot, message, 'Спочатку треба запустити гру')
					raise BotSkipMessage
				end
				result = make_bet(bot, message, bet, game, user.id)
				temp_user = User.create_or_find_by({ id: user.id })
				if result == true
				then
					temp_user.penalty(bet)
					total_bet = game.get_bet(user.id, type: bet.type)
					foo = "#{user.fullname} поставив #{symbol_carma(message.chat.id, message.from.id)}<code>#{total_bet}</code>(+<code>#{bet.quantity}</code>) на <b>#{bet.text}</b>#{bet.dot}<b>(</b><code>#{temp_user.likes}</code><b>)</b>"
					garbage = bot.api.sendMessage(chat_id: message.chat.id, parse_mode: 'HTML', reply_to_message_id: message.message_id, text: foo)
				else
					foo = "Не вистачає #{symbol_carma(message.chat.id, message.from.id)}<code>#{bet.quantity - temp_user.carma}</code><b>(</b><code>#{temp_user.carma}</code><b>)</b>"
					garbage = bot.api.sendMessage(chat_id: message.chat.id, parse_mode: 'HTML', reply_to_message_id: message.message_id, text: foo)
				end
				roulette_add_garbage(garbage)
			else
				 #bot.api.sendMessage(chat_id: message.chat.id, reply_to_message_id: message.message_id,	text: "Рулетка не запущена")
			end
	end
end