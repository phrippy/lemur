#!/usr/bin/ruby
#symbol_carma='🍔' #emoji :hamburger:
$symbol_carma='🥟' #emoji :dumpling:
require File.expand_path(File.dirname(__FILE__) + '/user/simple_user.rb')
class UserNameNotRecorded < StandardError; end
class User < ActiveRecord::Base
	has_many :admins
	def check_stole_user
		limit, timeout = get_user_stole_params(self)
		now = Time.now.to_i
		# puts "timeout <= Time.now.to_i: #{self.stole_first_in_cycle + timeout} <= #{Time.now.to_i}"
		# puts "count = #{self.like_count}"
		self.restart_stole_user if self.stole_first_in_cycle + timeout <= now
		wait = (self.stole_first_in_cycle + timeout) - now
		return wait if self.stole_count >= limit
		return 0
	end

	def restart_stole_user
		self.stole_count = 0
		self.stole_first_in_cycle = Time.now.to_i
		self.save
	end

	def penalty(value)
		case value
			when Roulette::Bet
				total = self.carma - value.quantity
				self.carma = total
				self.save
			else
				total = self.likes.to_i - value.to_i
				self.likes = total.to_s
				self.save
		end
		total
	end

	def talan
		self.talan_int.to_i
	end

	def talan=(value)
		self.talan_int = value.to_i
		self.save
	end

	def carma
		self.likes.to_i
	end

	def carma=(value)
		self.likes = value.to_i
		self.save
	end

	def reward(value)
		total = self.likes.to_i + value.to_i
		self.likes = total.to_s
		self.save
		total
	end

	def enought(value)
		self.carma >= value.to_i
	end

	def messages
		self.mess_total.to_i
	end

	def messages=(other)
		self.mess_total = other.to_s
		self.save
	end

	def increment_message
		self.messages += 1
	end

	def names_all
		if self.names_arr == "'[]'"
			self.names_arr = "[]"
			self.save
		end
		begin
			arr = JSON.load(Base64.strict_decode64(self.names_arr))
		rescue
			return []
		end
		# arr.map! do |x|
		# 	x = %x[echo -n #{x} | base64 -d]
		# 	x = JSON.load(x)
		# end
		# 	puts "#{__LINE__}: arr = #{arr.inspect}"
		# puts "#{__LINE__}: arr = #{arr}"
		arr
	end

	def names_all=(new_arr)
		# new_arr.map! do |x|
		# 	x.compact
		# end
		result = Base64.strict_encode64(new_arr.to_json)
		# self.names_arr = new_arr.map do |x|
		# 	puts x.to_json
		# 	# x = Base64.strict_encode64(x.to_json)
		# 	x = %x{echo -n #{x} | base64 -w 0}
		# end
		# puts new_arr.inspect
		self.names_arr = result
		self.save
	end

	def last
		begin
			self.names_all.last
		rescue
			[CGI.escape("<тут баг>")]
		end
	end

	def last=(arr)
		if self.names_all.nil?
			self.names_all = [arr]
		else
			result = self.names_all.push(arr)
			self.names_all = result
		end
	end

	def name_delete(firstname, lastname)
		result = self.names_all
		result.delete([firstname, lastname])
		self.names_all = result
	end

	def name_check(firstname, lastname)
		self.names_all.each do |user|
			return true if user[0] == firstname and user[1] == lastname
		end
		return false
	end

	def name_add(firstname, lastname)
		self.last = [firstname, lastname]
	end

	def name_update(firstname, lastname)
		last_user = self.last
		unless last_user == nil
			return false if last_user[0] == firstname and last_user[1] == lastname
		end
		unless self.names_all.nil?
			self.name_delete(firstname, lastname) if self.names_all.include?([firstname, lastname])
		end
		self.name_add(firstname, lastname)
	end

	def firstname
		begin
			return self.last[0]
		rescue
			return ""
		end
	end

	def lastname
		begin
			return self.last[1]
		rescue
			return nil
		end
	end

	def fullname(bot: nil, message: nil)
		case self
			when Integer
				user = User.create_or_find_by({id: self})
				return user.fullname
			else
				# continue
		end
		if self.names_all.nil?
			raise UserNameNotRecorded
		end
		return self.firstname if self.lastname.nil?
		"#{self.firstname} #{self.lastname}"
	rescue UserNameNotRecorded
		return nil
		# begin
		# 	hash = bot.api.getChatMember(chat_id: message.chat.id, user_id: self.id)
		# 	firstname = hash['result']['user']['first_name']
		# 	lastname = hash['result']['user']['last_name']
		# 	self.last = [firstname, lastname]
		# 	retry
		# rescue
		# 	"UNKNOWN ERROR"
		# end
	end
end

def set_currency_symbol(chat_id, user_id, emoji)
	return false if emoji.nil?
	currency = CustomCurrency.where(chat_id: chat_id).first
	if currency.nil?
		currency = CustomCurrency.create({chat_id: chat_id})
	end
	currency.emoji = emoji
end


def symbol_carma(chat_id=nil, user_id=nil)
	#Рандом зі вказуванням імовірнойстей. Чим більше число - тим більша імовірність
	#rubydoc.info/gems/pickup
	# CustomCurrency.all.to_a.each{|x| x.destroy}
	# dumpling = '🥟'
	# return dumpling unless test_groups(chat_id)
	currency_group = CustomCurrency.where(chat_id: chat_id).first
	if currency_group.nil?
		currency_group = CustomCurrency.create(chat_id: chat_id)
	end
	return currency_group.emoji
	return '💳' if chat_id == $test_group
	return "<b>Ҝ </b>"
	hamburger = '🍔'
	cherries = "🍒"
	arr = {dumpling => 30, cherries => 5, hamburger => 3}
	extra =	["🦾", "⚓️", "🍄", "🥝", "🍎", "🌰", "🧅", "🍅"]
	extra.each do |x|
		arr[x] = 2
	end
	pickup = Pickup.new(arr)
	pickup.pick(1)
end

def symbol_talan
	crystall_ball = '🔮'
	return crystall_ball
end

def symbol_message
	envelope = '✉️'
	return envelope
end

def user_get_carma (id)
	user = User.create_or_find_by({id: id})
	user.likes.to_i
end

def user_set_carma (id, value)
	user = User.create_or_find_by({id: id})
	user.likes = value.to_s
	user.save
end

def user_add_carma (id, value)
	user = User.create_or_find_by({id: id})
	user.likes = user.likes.to_i + value.to_i
	user.save
end

def user_enought_carma (id, value)
	user = User.create_or_find_by({id: id})
	#Щоб у адміна завжди вистачало монет
	#return true if user.id == $admin_id
	user.enought(value)
end

def user_like (bot, message, reward, set_talan)
	reward = reward.to_i
	unless message.reply_to_message.nil?
		from = message.from
		to = message.reply_to_message.from
		if from.id == to.id and from.id != $admin_id
			bot.api.sendMessage(chat_id: message.chat.id, reply_to_message_id: message.message_id, text: "Не можна плюсувати самого себе😏")
			raise BotSkipMessage
		end
		rewarder = User.create_or_find_by({id: from.id})
		user = User.create_or_find_by({id: to.id})
		#reward
		if set_talan
			if from.id == to.id
				bot.api.sendMessage(chat_id: message.chat.id, reply_to_message_id: message.message_id, text: "Не можна плюсувати самого себе😏")
				raise BotSkipMessage
			end
			if talan_check_timeout(message.from.id)
				user.talan += 1
				bot.api.sendMessage(chat_id: message.chat.id, reply_markup: delete_kb, parse_mode: "HTML", text: "#{mention(from)} покращив долю #{mention(to)} #{symbol_talan}<b>(</b><code>#{user.talan}</code><b>)</b>")
				talan_update_timeout(message.from.id)
				raise BotSkipMessage
			else
				inline_kb = [Telegram::Bot::Types::InlineKeyboardButton.new(text: "#{symbol_delete}Закрити", callback_data: 'delete_this_message')]
				kb = Telegram::Bot::Types::InlineKeyboardMarkup.new(inline_keyboard: inline_kb)
				bot.api.sendMessage(chat_id: message.chat.id, reply_markup: kb, parse_mode: "HTML", text: "Не більше трьох раз за 5 хвилин")
			end

		end
		unless rewarder.id == $admin_id and message.chat.id == $admin_id
			then
			if rewarder.enought(reward)
				then
					rewarder.carma = rewarder.carma - reward
					user.carma = user.carma + reward
					user.save
					#bot.api.sendMessage(chat_id: message.chat.id, parse_mode: "HTML", text: "#{from.first_name} передав #{to.first_name} <b>#{symbol_carma}<code>#{reward}</code> [<code>#{user.likes}</code>]</b>")
					bot.api.sendMessage(chat_id: message.chat.id, reply_markup: delete_kb, parse_mode: "HTML", text: "#{mention(from)} передав #{mention(to)} #{symbol_carma(message.chat.id, message.from.id)}<code>#{reward}</code> <b>(</b><code>#{user.likes}</code><b>)</b>")
				else
					bot.api.sendMessage(chat_id: message.chat.id, reply_markup: delete_kb, parse_mode: "HTML", reply_to_message_id: message.message_id, text: "Не вистачає вареників (#{symbol_carma(message.chat.id, message.from.id)}<code>#{rewarder.carma}</code>)")
				end
			else
				user.likes = (user.likes.to_i + reward.to_i).to_s
				user.save
				bot.api.sendMessage(chat_id: message.chat.id, reply_markup: delete_kb, parse_mode: "HTML", text: "<b>Режим бога:</b>\n#{from.fullname} передав #{to.fullname} #{symbol_carma(message.chat.id, message.from.id)}<code>#{reward}</code> <b>(</b><code>#{user.likes}</code><b>)</b>")
		end
	else
		true
	end
	raise BotSkipMessage
end

def user_dislike (bot, message, penalty, set_talan)
	return if penalty == 0
	unless message.reply_to_message.nil?
		from = message.from
		to = message.reply_to_message.from
		if from.id == to.id and false #quick fix for ignore
				bot.api.sendMessage(chat_id: message.chat.id, reply_to_message_id: message.message_id, text: "Не можна плюсувати самого себе😏")
				raise BotSkipMessage
		end

		user = User.create_or_find_by({id: to.id})
		rewarder = User.create_or_find_by({id: from.id})
		unless rewarder.id == $admin_id and message.chat.id == $admin_id
			then
				if set_talan
					if talan_check_timeout(message.from.id)
						user.talan -=1
						bot.api.sendMessage(chat_id: message.chat.id, reply_markup: delete_kb, parse_mode: "HTML", text: "#{mention(from)} погіршив долю #{mention(to)} #{symbol_talan}<b>(</b><code>#{user.talan}</code><b>)</b>")
						talan_update_timeout(message.from.id)
						raise BotSkipMessage
					else
						inline_kb = [Telegram::Bot::Types::InlineKeyboardButton.new(text: "#{symbol_delete}Закрити", callback_data: 'delete_this_message')]
						kb = Telegram::Bot::Types::InlineKeyboardMarkup.new(inline_keyboard: inline_kb)
						bot.api.sendMessage(chat_id: message.chat.id, reply_markup: kb, parse_mode: "HTML", text: "Не більше трьох раз за 5 хвилин")
					end
				else
					#todo
					# return unless test_groups(get_chat_id(message))
					return if from.id == to.id
				end
				# text = "Функція в розробці. Вареники ні в кого не відбираються!\n"
				text = ""
				# user = User.create_or_find_by({id: to.id})
				user_wait = rewarder.check_stole_user
				if user_wait != 0
					limit, timeout = get_user_stole_params(rewarder)
					# puts [limit, timeout].inspect
					dbg "user_wait: #{user_wait}"
					text = "#{text}Вам не можна грабувати частіше, ніж <code>#{limit}</code> раз за <code>#{sec_to_text(sectimeout)}</code>.Почекайте <code>#{sec_to_text(user_wait)}</code>"
					bot.api.sendMessage(chat_id: message.chat.id, reply_markup: delete_kb, reply_to_message_id: message.message_id, parse_mode: "HTML", text: text)
					return
				end
				member = get_chat_member(message.chat.id, from.id)
				member_wait = member.check_stole_member
				# puts "member_wait = #{member_wait}"
				if member_wait != 0
					limit, timeout = get_member_stole_params(member)
					# puts [limit, timeout].inspect

					# text = CGI.escapeHTML("#{text}\nMember: #{member.stole_count}/#{limit} #{Time.now.to_i - member.stole_first_in_cycle} < #{timeout}")
					dbg "user_wait: #{user_wait}"
					text = "#{text}Не можна грабувати в чаті частіше, ніж <code>#{limit}</code> раз за <code>#{sec_to_text(timeout)}</code>. Почекайте <code>#{sec_to_text(member_wait)}</code>"
					bot.api.sendMessage(chat_id: message.chat.id, reply_markup: delete_kb, reply_to_message_id: message.message_id, parse_mode: "HTML", text: text)
					return
				end
				relation = get_chat_relation(message.chat.id, from.id, to.id)
				relation_wait = relation.check_stole_relation
				# puts "Wait: #{relation_wait}"
				# puts relation.inspect
				# puts relation.check_stole_relation.inspect
				if relation_wait != 0
					limit, timeout = get_relation_stole_params(relation)
					# puts [limit, timeout].inspect
					dbg "user_wait: #{relation_wait}"
					text = "#{text}Не можна грабувати користувача частіше, ніж <code>#{limit}</code> раз за <code>#{sec_to_text(timeout)}</code>. Почекайте <code>#{sec_to_text(relation_wait)}</code>"
					bot.api.sendMessage(chat_id: message.chat.id, reply_markup: delete_kb, reply_to_message_id: message.message_id, parse_mode: "HTML", text: text)
					return
				end
				if user_enought_carma(user.id, penalty)
					then
						stole_db(member, relation, rewarder)
						value = stole(get_chat_id(message), rewarder.id, user.id, penalty)
						rewarder.carma += value
						user.carma -= value
						text = "#{text}#{mention(from)} вкрав у #{mention(to)} #{symbol_carma(message.chat.id, message.from.id)}<code>#{value}</code>"
						bot.api.sendMessage(chat_id: message.chat.id, reply_to_message_id: message.message_id, reply_markup: delete_kb, parse_mode: "HTML", text: text)
				else
						text = "#{text}#{mention(from)} хотів вкрасти у #{mention(to)} #{symbol_carma(message.chat.id, message.from.id)}<code>#{penalty}</code>, але в нього є лише #{symbol_carma(message.chat.id, message.from.id)}<code>#{user.carma}</code>"
						bot.api.sendMessage(chat_id: message.chat.id, reply_markup: delete_kb, reply_to_message_id: message.message_id, parse_mode: "HTML", text: text)
				end
		else
				user.likes = (user.likes.to_i - penalty.to_i).to_s
				user.save
				bot.api.sendMessage(chat_id: message.chat.id, parse_mode: "HTML", text: "<b>Режим бога:</b>\n#{from.fullname} опустив карму #{to.fullname} до #{symbol_carma(message.chat.id, message.from.id)}<code>#{user.carma}</code>")
		end
	end
	raise BotSkipMessage
end

def user_penalty(bot, message, penalty)
	user = User.create_or_find_by({id: message.from.id})
	user.likes = (user.likes.to_i - penalty.to_i).to_s
	user.save
end

def user_reward (bot, message, reward)
			 	user = User.create_or_find_by({id: message.from.id})
	user.likes = (user.likes.to_i + reward.to_i).to_s
	user.save
end

def user_karma_delete_me (bot, message)
	# ######################################################################################################################################################################
	# return
	group = Group.create_or_find_by({id: message.chat.id})
	members = get_members(chat_id)
	total = group.messages.to_a #redy - ignore
	total.map! do |x|
		user = User.create_or_find_by({id: x[0].to_i})
		[x[0], user.talan_int.to_i]
	end
	result = total.to_a.sort{ |a, b| b[1] <=> a[1] }.reject{|x| x[1] == 0}
	text = []
	result.map do |x|
		user = User.create_or_find_by({id: x[0]})
		unless user.nil?
			text.push([mention(x[0], false), "#{symbol_talan}#{x[1]}"])
		end
		text = text.sort{|a, b| b[1].to_i <=> a[1].to_i }.reject{|x| x[1] == 0}.first(20)
		# [x[0], x[1]]
	end
	offset = 1
	(0...text.size).each do |x|
		name = "<b>#{x+offset}.</b> #{text[x][0]}"
		score = text[x][1]
		text[x] = [name, score]
	end
	text = text.map{|x| x = x.join(": ")}.join("\n")
	bot.api.sendMessage(chat_id: message.chat.id, reply_markup: delete_kb, reply_to_message_id: message.message_id, parse_mode: "HTML", text: text)
end

def user_balance (bot, message)
	unless message.reply_to_message.nil?
		user = User.create_or_find_by({id: message.reply_to_message.from.id})
		replyid = message.reply_to_message.message_id
	else
		user = User.create_or_find_by({id: message.from.id})
		replyid = message.message_id
	end
	bot.api.sendMessage(chat_id: message.chat.id, reply_markup: delete_kb, parse_mode: "HTML", reply_to_message_id: replyid,	text: "#{symbol_carma(message.chat.id, message.from.id)}<code>#{user.carma}</code>")
end

def user_karma_reset (bot, message)
	user = User.create_or_find_by({id: message.from.id})
	name = message.from.first_name
	value = 1000
	if user.id == $admin_id
		value = message.text.scan(/^(\/reset|ресет|htctn)\s*(\d+)/i)[0][1].to_i rescue 1000
		if message.reply_to_message
			then
				otherid = message.reply_to_message.from.id
				user = User.create_or_find_by({id: otherid})
				name = message.reply_to_message.from.first_name
			else
				unless message.chat.id == $admin_id
					value = 1000
				end
		end
			user.carma = value
	else
		return false
		user.carma = 1000
	end
	#user.save
	value == 1000 ? text = "" : text = "<b>Режим бога:</b>\n"
	bot.api.sendMessage(chat_id: message.chat.id, parse_mode: "HTML", reply_to_message_id: message.message_id,	text: "#{text}Карма #{name} встановлена на значення #{symbol_carma(message.chat.id, message.from.id)}<code>#{user.carma}</code>")
end
