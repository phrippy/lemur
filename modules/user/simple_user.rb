#!/usr/bin/ruby
class SimpleUser
	attr_accessor :firstname, :lastname, :id, :base64_code, :base64_ready, :decoded
	class UserNameNotDEcoded < StandardError; end
	class Base64NotReady < StandardError; end

	def firstname
		self.decoded?
		@firstname
	end

	def lastname
		self.decoded?
		return nil if @lastname.to_s.empty?
		#return nil if @lastname.nil? # Якщо @lastname == nil, то nil і так повернеться в наступному рядку
		return @lastname
	end

	def id
		@id
	end

	def base64
		self.encode
	end

	def id=(foo)
		@id = foo
	end

	def base64?
		if 	@base64_ready == false
			raise Base64NotReady
		end
	end

	def decoded?
		if @decoded == false
			#puts "Декодую #{self.fullname}"
			self.decode
			@decoded = true
		end
		true
end

	def firstname=(foo)
		@firsrtname = foo
		@decoded = true
		@base64_ready = false
	end

	def lastname=(foo)
		@lasttname = foo
		@decoded = true
		@base64_ready = false
	end

	def encode
		# puts self.class
		# puts self.to_a.inspect
		# puts self.to_a.to_json
		begin
			self.base64?
			return @base64_code
		rescue Base64NotReady
			puts "Кодую #{self.fullname}"
			puts "Кодую #{self.to_a}"
			@base64_code = Base64.strict_encode64(self.to_a.to_json)
			@base64_ready = true
			retry
		end
	end

	def initialize(firstname = nil, lastname = nil, id: nil, base64: nil)
		if base64.nil?
			@decoded = false
			@base64_ready = false
		end
		@base64_code = base64_code
		@firstname = firstname
		@lastname = lastname
		@id = id
	end

	def to_a
		return [@firstname] if @lastname.nil?
		[@firstname, @lastname]
	end

	def fullname
		begin
			return self.firstname if self.lastname.nil?
		rescue
		end
		"#{self.firstname} #{self.lastname}"
	end

	def == other
		return false unless SimpleUser === other # Три знаки рівності. Те ж саме, що і other.is_a(SimpleUser)
		self.firstname == other.firstname and self.lastname == other.lastname
	end

	def decode
		return false if @base64_code.nil?
		decoded = Base64.decode64(@base64_code)
		@firstname = decoded[0]
		@lastname = decoded[1]
		@decoded = true
	end
end