#!/usr/bin/ruby
class ChatMember < ActiveRecord::Base
	# Щоб це працювало, треба переробити назви полів
	# chat - group_id
	# user - user_id
	# Але це поламає всього бота, тому відкладемо це в велику скриньку
	#
	#
	# belongs_to :user
	# belongs_to :group
	def messages
		self.mess_total
	end

	def messages=(other)
		self.mess_total = other.to_i
		self.message_last = Time.now.to_i
		self.save
	end

	def increment_message
		self.messages = self.messages + 1
	end

	def check_stole_member
		limit, timeout = get_member_stole_params(self)
		now = Time.now.to_i
		# puts "Після успішного пограбування в чаті пройшло лише #{now - self.stole_first_in_cycle}"
		# puts "#{self.stole_first_in_cycle + timeout} <= #{now}"
		# puts "Залишилось #{(self.stole_first_in_cycle + timeout) - now}"
		if self.stole_first_in_cycle + timeout <= now
			# puts "restart?"
			self.restart_stole_member
		end
		wait = (self.stole_first_in_cycle + timeout) - now
		# puts "Всього успішних пограбувань: #{self.stole_count} із #{limit}"
		# puts "Limits: #{self.stole_count} >= #{limit}"
		return wait if self.stole_count >= limit
		return 0
	end

	def restart_stole_member
		self.stole_count = 0
		self.stole_first_in_cycle = Time.now.to_i
		self.save
	end

	def level
		group = Group.create_or_find_by({ id: self.chat})
		maximum = group.max_messages
		user = self.messages
		level = get_level(user, maximum)
		return level
	end
end

def get_chat_member(chat_id, user_id)
	member = ChatMember.find_by({ chat: chat_id, user: user_id })
	if member.nil?
		member = ChatMember.create({ chat: chat_id, user: user_id })
	end
	return member
end

def get_members(chat_id)
	ChatMember.where({chat: chat_id}).order(mess_total: :desc).to_a
end


def member_karma (bot, message)
	# ######################################################################################################################################################################
	# return
	group = Group.create_or_find_by({id: message.chat.id})
	members = ChatMember.where({chat: message.chat.id}).order(mess_total: :desc)
	total = members.to_a
	# total = group.messages.to_a
	total.map! do |x|
		db_user = User.create_or_find_by({id: x.user})
		[x.user, db_user.talan_int.to_i]
		# [, user.talan_int.to_i]
	end
	result = total.to_a.sort{ |a, b| b[1] <=> a[1] }.reject{|x| x[1] == 0}
	text = []
	result.map do |x|
		user = User.create_or_find_by({id: x[0]})
		unless user.nil?
			text.push([mention(x[0], false), "#{symbol_talan}#{x[1]}"])
		end
		text = text.sort{|a, b| b[1].to_i <=> a[1].to_i }.reject{|x| x[1] == 0}.first(20)
		# [x[0], x[1]]
	end
	offset = 1
	(0...text.size).each do |x|
		name = "<b>#{x+offset}.</b> #{text[x][0]}"
		score = text[x][1]
		text[x] = [name, score]
	end
	text = text.map{|x| x = x.join(": ")}.join("\n")
	bot.api.sendMessage(chat_id: message.chat.id, reply_markup: delete_kb, reply_to_message_id: message.message_id, parse_mode: "HTML", text: text)
end