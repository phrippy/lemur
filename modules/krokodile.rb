#!/usr/bin/ruby
$symbol_user = '👤' #emoji :bust_in_silhouette:
$symbol_krokodile = '🐊'
$symbol_sad = '😢'
class Krokodile < ActiveRecord::Base ; end
$deadline = 300
def rand_word_get
	file = './db/words'
	#lines_count = %x[wc -l #{file}].to_i
	lines_count = 5812
	line = rand (lines_count + 1)
	result = %x[sed '#{line}!D' #{file}].chop
end

def krokodile_callback_show (bot, message)
	game = Krokodile.create_or_find_by({id: message.message.chat.id})
	if game.ingame
		unless game.explainer_id == message.from.id
			bot.api.answerCallbackQuery(callback_query_id: message.id, text: "Ця кнопка не для вас😏")
			return false
		end
		unless game.showed
			game.deadline = Time.now.to_i + $deadline
			game.showed = true
		end
		bot.api.answerCallbackQuery(callback_query_id: message.id, text: game.word)
	else
		bot.api.answerCallbackQuery(callback_query_id: message.id, text: "гра не запущена")
	end
end

def krokodile_callback_restart (bot, message)
	game = Krokodile.create_or_find_by({id: message.message.chat.id})
		#game.id = message.chat.id
		game.explainer_id = message.from.id
		game.word = rand_word_get
		game.deadline = Time.now.to_i + $deadline
		game.ingame = true
		game.showed = false
		game.save
eye = '👁'
disable = '❌'
		kb = [ 
[Telegram::Bot::Types::InlineKeyboardButton.new(text: "#{eye}Показати", callback_data: 'krokodile_show')],
[Telegram::Bot::Types::InlineKeyboardButton.new(text: "#{disable}Скаcувати", callback_data: 'krokodile_stop')]
		] 
		markup = Telegram::Bot::Types::InlineKeyboardMarkup.new(inline_keyboard: kb) 
		bot.api.sendMessage(chat_id: game.id, reply_markup: markup, text: "#{$symbol_user}#{message.from.first_name} пояснює слово за п'ять хвилин")
	end
	
def krokodile_callback_stop (bot, message)
	game = Krokodile.create_or_find_by({id: message.message.chat.id})
	unless game.ingame
		bot.api.answerCallbackQuery(callback_query_id: message.id, text: "гра не запущена")
		return false
	end
	unless game.explainer_id == message.from.id
		bot.api.answerCallbackQuery(callback_query_id: message.id, text: "зупинити гру може лише ведучий")
		return false
	end
eye = '👁'
disable = '❌'
restart = '🔄'
		kb = [ 
[Telegram::Bot::Types::InlineKeyboardButton.new(text: "#{restart}Перезапустити", callback_data: 'krokodile_restart')],
		] 
		markup = Telegram::Bot::Types::InlineKeyboardMarkup.new(inline_keyboard: kb) 
	bot.api.sendMessage(chat_id: game.id, reply_markup: markup, text: "#{$symbol_user}#{message.from.first_name} пристрелив крокодила#{$symbol_krokodile}#{$symbol_sad}")
	krokodile_finish(bot, message.message)
end

def krokodile_callback_new_word (bot, message)
	game = Krokodile.create_or_find_by({id: message.message.chat.id})
	unless game.ingame
		bot.api.answerCallbackQuery(callback_query_id: message.id, text: "гра не запущена")
		return false
	end
	unless game.explainer_id == message.from.id or $admin_id == message.from.id
		bot.api.answerCallbackQuery(callback_query_id: message.id, text: "кнопка для ведучого")
	else
	                #game.id = message.chat.id
                game.ingame = true
                game.showed = false
                game.explainer_id = message.from.id
                game.word = rand_word_get
                game.deadline = Time.now.to_i + $deadline
                game.save
                bot.api.answerCallbackQuery(callback_query_id: message.id, text: game.word)
	end
		
end

def krokodile_start (bot, message)
	game = Krokodile.create_or_find_by({id: message.chat.id})
	if game.ingame
		bot.api.sendMessage(chat_id: game.id, reply_to_message_id: message.message_id,  text: "#{$symbol_user}#{message.from.first_name}, гра вже запущена")
	else
		#game.id = message.chat.id
		game.ingame = true
		game.showed = false
		game.explainer_id = message.from.id
		game.word = rand_word_get
		game.deadline = Time.now.to_i + $deadline
		game.save
eye = '👁'
disable = '❌'
		kb = [ 
[Telegram::Bot::Types::InlineKeyboardButton.new(text: "#{eye}Показати", callback_data: 'krokodile_show')],
[Telegram::Bot::Types::InlineKeyboardButton.new(text: "#{disable}Скаcувати", callback_data: 'krokodile_stop')]
		] 
		markup = Telegram::Bot::Types::InlineKeyboardMarkup.new(inline_keyboard: kb) 
		bot.api.sendMessage(chat_id: game.id, reply_to_message_id: message.message_id, reply_markup: markup, text: "#{$symbol_user}#{message.from.first_name} пояснює слово за п'ять хвилини")
	end
end

def krokodile_callback_start (bot, message)
	game = Krokodile.create_or_find_by({id: message.chat.id})
	if game.ingame
		bot.api.answerCallbackQuery(callback_query_id: message.id, text: "гра вже запущена")
	else
		#game.id = message.chat.id
		game.word = rand_word_get
		game.deadline = Time.now.to_i + $deadline
		game.save
		bot.api.answerCallbackQuery(callback_query_id: message.id, text: game.word)
	end
end

def krokodile_force_stop (bot, message)
	game = Krokodile.create_or_find_by({id: message.chat.id})

	unless game.ingame
		# bot.api.sendMessage(chat_id: game.id, reply_to_message_id: message.message_id,  text: "#{$symbol_user}#{message.from.first_name}, гра не запущена")
		return false
	end

	unless game.explainer_id == message.from.id  or $admin_id == message.from.id
		bot.api.sendMessage(chat_id: game.id, reply_to_message_id: message.message_id,  text: "Зупинити гру може лише ведучий")
		return false
	end

eye = '👁 '
disable = '❌'
restart = '🔄'
                kb = [ 
[Telegram::Bot::Types::InlineKeyboardButton.new(text: "#{restart}Перезапустити", callback_data: 'krokodile_restart')],
                ]   
                markup = Telegram::Bot::Types::InlineKeyboardMarkup.new(inline_keyboard: kb)
	bot.api.sendMessage(chat_id: game.id, reply_to_message_id: message.message_id, reply_markup: markup, text: "#{$symbol_user}#{message.from.first_name} пристрелив крокодила#{$symbol_krokodile}#{$symbol_sad}")
	krokodile_finish(bot, message)
end

def krokodile_finish (bot, message)
	game = Krokodile.create_or_find_by({id: message.chat.id})
	game.ingame = false
	game.showed = nil
	game.explainer_id = nil
	game.word = nil
	game.deadline = Time.now.to_i
	game.save
end

def krokodile_reward (winner, reward)
	user_like(winner, reward)
end

def krokodile_check (bot, message)
	game = Krokodile.create_or_find_by({id: message.chat.id})
	unless game.ingame
		return false
	else
		if Time.now.to_i > game.deadline
			bot.api.sendMessage(chat_id: message.chat.id, text: "Термін гри сплив. Загадане слово - #{game.word}")
			krokodile_finish(bot, message)
			return false
		end
	end
	return false if message.text.nil?
	if message.text.match(/#{game.word}/i)
		if message.from.id == game.explainer_id
			penalty = game.word.size * 100
			user_penalty(bot, message, penalty)
			bot.api.sendMessage(chat_id: message.chat.id, reply_to_message_id: message.message_id,  text: "#{$symbol_user}#{message.from.first_name} був оштрафований на #{symbol_carma(message.chat.id, message.from.id)}#{penalty}")
		else
			reward = game.word.size * 10
			user_reward(bot, message, reward)
			eye = '👁'
			disable = '❌'
			restart = '🔄'
                kb = [ 
[Telegram::Bot::Types::InlineKeyboardButton.new(text: "#{restart}Перезапустити", callback_data: 'krokodile_restart')],
                ]   
                markup = Telegram::Bot::Types::InlineKeyboardMarkup.new(inline_keyboard: kb) 
        bot.api.sendMessage(chat_id: message.chat.id, reply_to_message_id: message.message_id, reply_markup: markup, text: "#{$symbol_user}#{message.from.first_name} вгадав слово \"#{game.word}\" і отримав #{symbol_carma(message.chat.id, message.from.id)}#{reward}")
                        krokodile_finish(bot, message)
                end
        end
end

