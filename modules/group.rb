#!/usr/bin/ruby
class Admin < ActiveRecord::Base
	belongs_to :user
	belongs_to :group
end

class Group < ActiveRecord::Base
	has_many :users
	has_many :admins
	def have(userid)
		return
		JSON.load(self.members)[userid].nil
	end

	def group_rules
		if self.rules_url_b64 == nil
			return nil
		end
		Base64.strict_decode64(self.rules_url_b64).force_encoding('UTF-8')
	end

	def group_adv
		if self.adv_url_b64 == nil
			return nil
		end
		Base64.strict_decode64(self.adv_url_b64).force_encoding('UTF-8')
	end

	def group_rules=(text)
		self.rules_url_b64 = Base64.strict_encode64(text)
		self.save
	end

	def group_adv=(text)
		self.adv_url_b64 = Base64.strict_encode64(text)
		self.save
	end

	def messages_delete_me(user_id = nil)
		if user_id.nil?
			begin
			result_json = JSON.load(self.members)
			rescue
				result_json = {}
			end
			return {} if result_json.nil?
			result = {}
			result_json.each do |key, value|
				result[key.to_i] = value.to_i
			end
			return result
		else
			result = self.messages(nil) #ready - ignore
			result = result[user_id]
			if result.nil?
				return 0
			else
				return result
			end
		end
		result
	end

	def set_messages_delete_me(user_id, value)
		total = self.messages #ready - ignore
		total[user_id.to_s] = value.to_s
		result = total.to_json
		self.members = result
		self.save
	end

	def increment_user_delete_me(user_id)
		result = self.messages(user_id) #ready - ignore
		result = result.to_i
		result += 1
		set_messages(user_id, result)
	end

	def maximum
		self.max_messages
	end

	def maximum=(other)
		self.max_messages = other
		self.save
	end

	def last
		self.last_message
	end

	def last=(other)
		self.last_message = other.to_i
		self.save
	end

	def touch
		self.prev_activity = self.last_activity
		self.last_activity = Time.now.to_i
		if self.yesterday_saluted.nil?
			self.today_date = Date.today.to_s
			self.yesterday_saluted = true
		end
		self.save
	end
end

def salute_yesterday_flooder(bot, message)
	group = Group.create_or_find_by({id: message.chat.id})
	user = User.create_or_find_by({id: message.from.id})
	#member = get_chat_member(message.chat.id, message.from.id)
	how_many = JSON.load(group.yesterday_flood)[group.salutator_id.to_s]
	dbg "Вітаю, <b>#{user.fullname}</b>, за минулий день ви написали найбільше повідомлень: ✉️#{how_many}"
	bot.api.sendMessage(chat_id: group.id, reply_to_message_id: message.message_id,  parse_mode: 'HTML', text: text)
	group.yesterday_saluted = true
	group.salutator_id = nil
	group.save
end

def call_spam_admins(message, show_link = true)
	rights = '📃'
	adv = '💰'
	admin = '👮🏻'
	chat = Group.create_or_find_by({id: get_chat_id(message)})
	rules_url = chat.group_rules
	adv_url = chat.group_adv
	kb_arr = []
	unless rules_url.nil?
		kb_arr.push(Telegram::Bot::Types::InlineKeyboardButton.new(text: "#{rights}Правила", url: rules_url))
	end
	unless adv_url.nil?
		kb_arr.push(Telegram::Bot::Types::InlineKeyboardButton.new(text: "#{adv}Реклама", url: adv_url))
	end
	kb_arr.push(Telegram::Bot::Types::InlineKeyboardButton.new(text: "#{admin}Бан", callback_data: "spamer_ban:#{message.from.id}"))
	if message.from.id == $admin_id or message.chat.id == -1001009767907
		# kb = [
		# 	[Telegram::Bot::Types::InlineKeyboardButton.new(text: "#{rights}Правила", url: 'https://telegra.ph/Pravila-chatu-Telegram-Ukraine-01-24'),
		# 	 Telegram::Bot::Types::InlineKeyboardButton.new(text: "#{adv}Реклама", url: 'https://telegra.ph/Reklama-v-chat%D1%96-08-25'),
		# 	 Telegram::Bot::Types::InlineKeyboardButton.new(text: "#{admin}Бан", callback_data: "spamer_ban:#{message.from.id}")]
		# ]
		markup = Telegram::Bot::Types::InlineKeyboardMarkup.new(inline_keyboard: [kb_arr])
		# alex = '<a href="tg://user?id=115530526">Алекс</a>'
		# olena = '<a href="tg://user?id=53966589">Олена</a>'
		# stas = '<a href="tg://user?id=52085336">Стас</a>'
		# alex = '<i>Алекс</i>'
		# olena= '<i>Олена</i>'
		# stas = '<i>Стас</i>'
		admins = Admin.where({group_id: message.chat.id, type_mv: 'spamban', enabled: true}).to_a
		if admins.empty?
			admins_mentions = "Адміни,"
		else
			admins_names = []
			if show_link
				admins.each do |admin|
					user = User.create_or_find_by({id: admin.user_id})
					admins_names.push("#{mention(user, true)}")
				end
			else
				admins.each do |admin|
					user = User.create_or_find_by({id: admin.user_id})
					admins_names.push("<b>#{mention(user, false)}</b>")
				end
			end
			admins_mentions = admins_names.join(" | ")
		end
		text = "#{admins_mentions} тут хтось хоче в дупу собі смайлики свої запхати 🌚"
		# text = "#{alex} #{olena} #{stas} тут хтось хоче в дупу собі смайлики свої запхати 🌚"
		Lemur.api.sendMessage(chat_id: message.chat.id, reply_markup: markup, parse_mode: "HTML", reply_to_message_id: message.message_id, disable_web_page_preview: true, text: text)
		fwd_mess = Lemur.api.forwardMessage(chat_id: $admin_id, from_chat_id: message.chat.id, message_id: message.message_id)
		fwd_id = get_message_id(fwd_mess)
		chat = Lemur.api.getChat(chat_id: message.chat.id)
		text=[]
		if (message.chat.id < 0)
			if chat['result']['invite_link'] == nil
				text[0] = "Чат: <a href=\"https://t.me/#{chat['result']['username']}\">#{chat['result']['title']}</a>"
			else
				text[0] = "Чат: <a href=\"#{chat['result']['invite_link']}\">#{chat['result']['title']}</a>"
			end
			text[1] = "Юзер: <a href=\"tg://user?id=#{message.from.id}\">#{message.from.fullname}</a>"
		else
			text[0] = "Чат: пп з <a href=\"tg://user?id=#{message.from.id}\">#{message.from.fullname}</a>"
		end
		text=text.join("\n")
		Lemur.api.sendMessage(chat_id: $admin_id, parse_mode: "HTML", reply_to_message_id: fwd_id, text: text)
		# begin
		#   text = "#спам #новачок <b>#{chat_title}</b>"
		#   bot.api.sendMessage(chat_id: $admin_id, parse_mode: "HTML", reply_to_message_id: fwd_id, text: text)
		#   text = fwd_mess.inspect
		#   bot.api.sendMessage(chat_id: $admin_id, parse_mode: "HTML", text: "#спам #новачок <code>#{text}</code>")
		# rescue
		#   text = fwd_mess.inspect
		#   bot.api.sendMessage(chat_id: $admin_id, parse_mode: "HTML", text: "#спам #новачок <code>#{text}</code>")
		# end
	end
end