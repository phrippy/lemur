#!/usr/bin/ruby
#============================================
#Підключення до БД. Видалити після готовності
require 'active_record'
require 'mysql2'
ActiveRecord::Base.establish_connection( :adapter => "mysql2", :host => "localhost", :username => "pi", :password => "secret", :database => "lemur" )
#============================================

#class MessageCounter < ActiveRecord::Base ; end
class Level < ActiveRecord::Base
	def to_i
		self.id
	end
end

#Обʼєкти типу Telegram::Bot::Types::Message не використовуємо!
#Тільки id користувача!

# def get_level(total_messages)
# #Замість всієї цієї біліберди можна було б перевіряти приналежність до діапазону
# #todo
# 	Level.all.max do |a, b|
# 		if total_messages < a.score
# 			-1
# 		else
# 			a.score <=> b.score
# 		end
# 	end
# end
def relation_to_level(relation)
	relation *= 100
	case relation
		when 0...0.4
			Level.find(0)
		when 0.4...1
			Level.find(1)
		when 1...2
			Level.find(2)
		when 2...3
			Level.find(3)
		when 3...4
			Level.find(4)
		when 4...5
			Level.find(5)
		when 5...10
			Level.find(6)
		when 10...13
			Level.find(7)
		when 13...15
			Level.find(8)
		when 15...20
			Level.find(9)
		when 20...25
			Level.find(10)
		when 25...30
			Level.find(11)
		when 30...35
			Level.find(12)
		when 35...40
			Level.find(13)
		when 40...45
			Level.find(14)
		when 45...50
			Level.find(15)
		when 50...60
			Level.find(16)
		when 60...70
			Level.find(17)
		when 70...80
			Level.find(18)
		when 80...100
			Level.find(19)
		when 100
			Level.find(20)
		else
			return false
	end
end

def level_to_relation(level)
	level = level.id if level.is_a?(Level)
	relation = case level
		when 0
			0
		when 1
			0.4
		when 2
			1
		when 3
			2
		when 4
			3
		when 5
			4
		when 6
			5
		when 7
			10
		when 8
			13
		when 9
			15
		when 10
			20
		when 11
			25
		when 12
			30
		when 13
			35
		when 14
			40
		when 15
			45
		when 16
			50
		when 17
			60
		when 18
			70
		when 19
			80
		when 20
			100
		else
			return false
		 end
	return relation.to_f / 100
end


def get_level_ratio(chat_id, user_id)
	level = get_member_level(chat_id, user_id)
	ratio = level_to_relation(level)
end

def get_member_level(chat_id, user_id)
	# group = Group.create_or_find_by({id: chat_id})
	member = get_chat_member(chat_id, user_id)
	# level = get_level(member.messages, group.maximum)
	return member.level
end

def get_level(current, maximum)
	if maximum == 0
		relation = 0
		else
		relation = current.to_f / maximum
	end
	level = relation_to_level(relation)
	return level
end


def level_updated(message, current_level, group)
	return false if current_level.id == 20
	member = get_chat_member(group.id, message.from.id)
	total = member.messages # ready
	# total = group.messages(message.from.id)
	# level = get_level(total, group.max_messages)
	level = member.level
	if current_level.id != level.id
		return level
	else
		return false
	end
end

def new_carma(level)
	case level
		when 1
			4
		when 2
			14
		when 3
			34
		when 4
			64
		when 5
			104
		when 6
			154
		when 7
			254
		when 8
			384
		when 9
			534
		when 10
			734
		when 11
			984
		when 12
			1284
		when 13
			1634
		when 14
			2034
		when 15
			2484
		when 16
			2984
		when 17
			3584
		when 18
			4284
		when 19
			5084
		when 20
			6084
	end
end

def new_talan(level)
	return 0 if level == 0
	(1..level).inject(:+)
end

