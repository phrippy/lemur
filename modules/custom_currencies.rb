#!/usr/bin/ruby
class  CustomCurrency < ActiveRecord::Base
	def emoji
		dumpling = '🥟'
		return dumpling if self.emoji_text.nil?
		Base64.strict_decode64(self.emoji_text).force_encoding('UTF-8')
	end

	def emoji=(other)
		self.emoji_text = Base64.strict_encode64(other)
		self.save
	end
end
