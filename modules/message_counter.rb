#!/usr/bin/ruby
class Level < ActiveRecord::Base ; end

def stat_get_user (bot, message, daily = true, chat = true)
	#Рейтинги конкретного користувача
	#У відповіді лише числа
	#Можна було б ще зробити рейтинг для кожних груп, в яких написав користувач, але не думаю, що це так сильно потрібно
	#Тому просто рахуватимемо суму
	counter = MessageCounter.create_or_find_by({chat: message.chat.id, user: message.from.id})
	if chat
		#Кількіть всіх повідомлень користувача в чаті
		#За день чи всього

		#123456789
		result = daily ? counter.today : counter.total
	else
		#Те ж саме, але у всіх чатах

		#123456789
		if daily
			result = MessageCounter.where(user: counter.user).inject(0){|sum, x| sum + x.today}
		else
			result = MessageCounter.where(user: counter.user).inject(0){|sum, x| sum + x.total}
		end
	end
	result
end

def stat_get_users (bot, message, daily = true, chat = true)
	#Тут рейтинги користувачів - у відповіді лише масиви!
	counter = MessageCounter.create_or_find_by({chat: message.chat.id, user: message.from.id})
	if chat
		if daily
			#Сума кількості всіх повідомлень всіх користувачів в чаті
			#За день чи всього
			#Переробити в список з рейтингом

			#123456789 + 123456789
			result = MessageCounter.where(chat: counter.chat).inject(0){|sum, x| sum + x.today}
		else
			result = MessageCounter.where(chat: counter.chat).inject(0){|sum, x| sum + x.total}
		end
	else
		if daily
			#Те ж саме, але не з цього чату, а з усіх
			#Теж переробити в рейтинг флудерів
			#В поточному вигляді віддає всі зареєєстровані ботом повідомлення (за день чи всього) 

			#123456789 + 123456789
			result = MessageCounter.where(user: counter.user).inject(0){|sum, x| sum + x.today}
		else
			result = MessageCounter.where(user: counter.user).inject(0){|sum, x| sum + x.total}
		end
	end
	result
end

def stat_get_chat (bot, message, daily = true, chat = true)
	counter = MessageCounter.create_or_find_by({chat: message.chat.id, user: message.from.id})
	if chat
		if daily
			#Сума кількості всіх повідомлень в чаті
			#За день чи всього
			#Готово, не треба нічого перероблювати

			#123456789
			result = MessageCounter.where(chat: counter.chat).inject(0){|sum, x| sum + x.today}
		else
			result = MessageCounter.where(chat: counter.chat).inject(0){|sum, x| sum + x.total}
		end
	else
		if daily
			#Те ж саме, але не з цього чату, а з усіх
			#Переробити в рейтинг груп
			#В поточному вигляді віддає всі зареєєстровані ботом повідомлення (за день чи всього) 
			result = MessageCounter.where(user: counter.user).inject(0){|sum, x| sum + x.today}
		else
			result = MessageCounter.where(user: counter.user).inject(0){|sum, x| sum + x.total}
		end
	end
	result
end
