#!/usr/bin/ruby
require 'active_record'
require 'mysql2'
ActiveRecord::Base.establish_connection( :adapter => "mysql2", :host => "localhost", :username => "pi", :password => "secret", :database => "lemur" )
class Level < ActiveRecord::Base ; end

def nextlevel level
	exponent = 1.1
	baseXP = 100
	baseXP = (level ** exponent)
#	(40*(level**2) - (40*level))/1.5
# 	(4 * (level ** 3))/5
	(4 * (baseXP ** 3))/5
end

arr = []
max = 20
(0..(max+1)).to_a.each do |x|
    unless x-1 < 0
	#puts "Level #{x-1}\t - #{(nextlevel(x)*1.6).round(0) }"
    id = x-1
    score = (nextlevel(x+1)*1.9).round(0)
    # arr.push(id: id, score: score)
    arr.push([id, score])
    end
end
# arr_new = []
# arr.each do |x|
# 	arr_new.push([x['id'],x['score']])
# end
# arr.delete_at(0)
# level0 = Level.create_or_find_by({id: 0})
# level0.destroy
arr_text = arr.map{|x|x.join(":")}.join("\n")
#puts arr_text

#return
arr.each do |x|
    level = Level.create_or_find_by({id: x[0]})
    level.score = x[1]
    level.save
    rescue
        next
end

level0 = Level.create_or_find_by({id: 0})
level0.score = 0
level0.save
# return
array = [
['Москаль', '-'],
['Джура', '*'],
['Козак', '|'],
['Приказник', '||'],
['Молодший урядник', '|||'],

['Урядник', '||||'],
['Старший урядник', '( ||||'],
['Десятник', '( /'],
['Підхорунжий', '( //'],

['Хорунжий', '( ///'],
['Сотник', '( ||'],
['Підосавул', '( ◊'],
['Осавул', '( ◊ ◊'],

['Військовий суддя', '( ◊ ◊ ◊'],
['Писар', '( | ◊'],
['Підполковник', '( | ◊ ◊'],
['Полковник', '( | ◊ ◊ ◊'],

['Похідний Атаман', '( ‡ ◊'],
['Наказний Атаман', '( ‡ ◊ ◊'],
['Повний Атаман', '( ‡ ◊ ◊ ◊'],
['Ясновельможний Гетьман всія чату', '( ‡ ψ ◊ ◊ ◊ ◊']
]

(0..20).each do |x|
	level = Level.create_or_find_by({id: x})
	level.name = array[x][0]
	level.sign = array[x][1]
	level.save
end