#!/bin/bash
#examples
#ALTER TABLE lemur.roulettes ADD bets_numbers_arr_arr MEDIUMTEXT DEFAULT "{}" AFTER bets_small_arr;
#ALTER TABLE lemur.roulettes MODIFY COLUMN bets_odd_arr MEDIUMTEXT DEFAULT "{}" AFTER bets_small_arr;
#UPDATE roulettes SET log='[]', ingame=1 WHERE id=240266956;

user='pi'
host='localhost'
password='secret'
database='lemur'
sudo mariadb << EOF
CREATE USER IF NOT EXISTS '${user}'@'${host}' IDENTIFIED BY 'password';
GRANT ALL PRIVILEGES ON ${database} . * TO '${user}'@'$host';
SET PASSWORD FOR '${user}'@'${host}' = PASSWORD('${password}');
FLUSH PRIVILEGES;
EOF

sudo mariadb << EOF
CREATE DATABASE IF NOT EXISTS lemur;
EOF


sudo mariadb << EOF
CREATE TABLE IF NOT EXISTS ${database}.users (
id BIGINT UNSIGNED, PRIMARY KEY(id),
likes MEDIUMTEXT DEFAULT 1000,
talan_int MEDIUMTEXT DEFAULT 1000,
talan_timeout MEDIUMTEXT DEFAULT '',
mess_total MEDIUMTEXT DEFAULT 0
names_arr mediumtext DEFAULT '',
stole_count BIGINT UNSIGNED DEFAULT 0,
stole_first_in_cycle BIGINT UNSIGNED DEFAULT 0,
message_last BIGINT UNSIGNED DEFAULT 0,
);
EOF

sudo mariadb << EOF
CREATE TABLE IF NOT EXISTS ${database}.chat_members (
id BIGINT UNSIGNED AUTO_INCREMENT, PRIMARY KEY(id),
chat BIGINT,
user BIGINT UNSIGNED,
mess_total BIGINT UNSIGNED DEFAULT 0,
created_at BIGINT UNSIGNED,
updated_at BIGINT UNSIGNED,
last_message BIGINT UNSIGNED DEFAULT 0,
max_level INT UNSIGNED DEFAULT 0,
stole_count BIGINT UNSIGNED DEFAULT 0,
stole_first_in_cycle BIGINT UNSIGNED DEFAULT 0,
stole_last BIGINT UNSIGNED DEFAULT 0,
);

CREATE TABLE IF NOT EXISTS ${database}.member_relations (
id BIGINT UNSIGNED AUTO_INCREMENT, PRIMARY KEY(id),
chat_id BIGINT,
master_id BIGINT UNSIGNED,
slave_id BIGINT UNSIGNED,
like_count BIGINT UNSIGNED DEFAULT 0,
like_last BIGINT UNSIGNED DEFAULT 0,
stole_count BIGINT UNSIGNED DEFAULT 0,
stole_first_in_cycle BIGINT UNSIGNED DEFAULT 0,
stole_last BIGINT UNSIGNED DEFAULT 0,
message_last BIGINT UNSIGNED DEFAULT 0,
);


CREATE TABLE IF NOT EXISTS ${database}.chat_members (
id BIGINT UNSIGNED AUTO_INCREMENT, PRIMARY KEY(id),
chat BIGINT,
user BIGINT UNSIGNED,
mess_total BIGINT UNSIGNED DEFAULT 0,
created_at BIGINT UNSIGNED,
updated_at BIGINT UNSIGNED,
last_message BIGINT UNSIGNED,
max_level TINYINT UNSIGNED,
stole_count BIGINT UNSIGNED,
stole_first_in_cycle BIGINT UNSIGNED,
stole_last BIGINT UNSIGNED,
message_last BIGINT UNSIGNED,
);

CREATE TABLE IF NOT EXISTS ${database}.custom_currencies (
id BIGINT UNSIGNED AUTO_INCREMENT, PRIMARY KEY(id),
chat_id BIGINT NULL,
user_id BIGINT UNSIGNED DEFAULT NULL,
emoji_text TINYTEXT;
created_at BIGINT UNSIGNED,
expired_at BIGINT UNSIGNED,
);

CREATE TABLE IF NOT EXISTS ${database}.pairs (
id BIGINT UNSIGNED AUTO_INCREMENT, PRIMARY KEY(id),
chat_id BIGINT, PRIMARY KEY(id),
master BIGINT UNSIGNED DEFAULT NULL,
slave BIGINT UNSIGNED DEFAULT NULL,
created_at BIGINT UNSIGNED,
expired_at BIGINT UNSIGNED,
expired_bool BOOL DEFAULT false,
);

CREATE TABLE IF NOT EXISTS ${database}.krokodiles (id BIGINT NOT NULL, PRIMARY KEY(id), ingame BOOL DEFAULT false, showed BOOL DEFAULT false, explainer_id BIGINT UNSIGNED, word TINYTEXT, deadline BIGINT UNSIGNED DEFAULT 0);
CREATE TABLE IF NOT EXISTS ${database}.levels (id BIGINT UNSIGNED, PRIMARY KEY(id), score BIGINT UNSIGNED DEFAULT 0);
CREATE TABLE IF NOT EXISTS ${database}.message_counters (id BIGINT UNSIGNED AUTO_INCREMENT, PRIMARY KEY(id), chat BIGINT, user BIGINT UNSIGNED, total BIGINT UNSIGNED, today BIGINT UNSIGNED, today_date DATE);
CREATE TABLE IF NOT EXISTS ${database}.roulettes (
	id bigint NOT NULL,
	PRIMARY KEY (id)
	deadline	bigint(20) unsigned,
	bets_black_arr mediumtext DEFAULT '{}',
	bets_red_arr mediumtext DEFAULT '{}',
	bets_green_arr mediumtext DEFAULT '{}',
	bets_big_arr mediumtext DEFAULT '{}',
	bets_small_arr mediumtext DEFAULT '{}',
	bets_odd_arr mediumtext DEFAULT '{}',
	bets_even_arr mediumtext DEFAULT '{}',
	log mediumtext DEFAULT '[]',
	garbage mediumtext DEFAULT '[]',
	ingame tinyint(1) DEFAULT 0,
	locked tinyint(1) DEFAULT 0,
	bets_trio1_arr mediumtext DEFAULT '{}',
	bets_trio2_arr mediumtext DEFAULT '{}',
	bets_trio3_arr mediumtext DEFAULT '{}',
	bets_kvar1_arr mediumtext DEFAULT '{}',
	bets_kvar2_arr mediumtext DEFAULT '{}',
	bets_kvar3_arr mediumtext DEFAULT '{}',
	bets_kvar4_arr mediumtext DEFAULT '{}',
	bets_raw_arr mediumtext DEFAULT '{}',
);
CREATE TABLE IF NOT EXISTS ${database}.groups (
	id BIGINT, PRIMARY KEY(id),
	#{"user" => "how many messages in group"}
	members mediumtext DEFAULT '{}',
	max_messages BIGINT UNSIGNED DEFAULT 0,
	last_message BIGINT UNSIGNED DEFAULT 0,
	last_activity BIGINT UNSIGNED DEFAULT 0,
	prev_activity BIGINT UNSIGNED DEFAULT 0,
	today_flood mediumtext DEFAULT '{}',
	yesterday_flood mediumtext DEFAULT '{}',
	today_date TINYTEXT,
	yesterday_date TINYTEXT,
	yesterday_saluted BOOL,
	salutator_id BIGINT;

)

CREATE TABLE IF NOT EXISTS ${database}.admins (
  id BIGINT UNSIGNED AUTO_INCREMENT NOT NULL UNIQUE PRIMARY KEY,
  type_mv TINYTEXT DEFAULT 'spamban',
  group_id BIGINT NOT NULL,
  user_id BIGINT UNSIGNED NOT NULL
  enabled bool default true;
  created_at BIGINT UNSIGNED;
  updated_at BIGINT UNSIGNED;
);

ALTER TABLE ${database}.admins
  ADD CONSTRAINT admins_group_id_groups_id
  FOREIGN KEY (group_id) REFERENCES groups(id);
ALTER TABLE ${database}.admins
  ADD CONSTRAINT admins_user_id_users_id
  FOREIGN KEY (user_id) REFERENCES users(id);

CREATE TABLE IF NOT EXISTS ${database}.levels (
	id bigint(20) unsigned , PRIMARY KEY(id),
	score bigint(20) unsigned default 0,
	name mediumtext DEFAULT '',
)
CREATE TABLE IF NOT EXISTS ${database}.tribunals (
	id bigint(20), PRIMARY KEY(id),
	timestamp bigint(20) unsigned,
	yes_arr mediumtext DEFAULT '[]',
	no_arr mediumtext DEFAULT '[]',
	info_message_id bigint(20) unsigned,
	penalty_int bigint(20) unsigned,
	plaintiff_id bigint(20) unsigned,
	defendant_id bigint(20) unsigned,
	finished_bool BOOL DEFAULT false,
	limit_votes TINYINT UNSIGNED default 3,
	updated_at BIGINT UNSIGNED,
	expired_at BIGINT UNSIGNED default 0,
)
CREATE TABLE IF NOT EXISTS ${database}.inventories (
	id bigint(20), PRIMARY KEY(id),
	wedding_he bigint(20) unsigned default 0,
	wedding_she bigint(20) unsigned default 0,
  wedding_ring bigint(20) unsigned default 0,
  wedding_document bigint(20) unsigned default 0,
  wedding_flowers bigint(20) unsigned default 0,
  vata_branch bigint(20) unsigned default 0,
)
CREATE TABLE IF NOT EXISTS ${database}.wares (
	id BIGINT UNSIGNED AUTO_INCREMENT, PRIMARY KEY(id),
	bname_db mediumtext,
	name_db mediumtext,
	rname_db mediumtext,
	emoji_db mediumtext,
	price_db BIGINT UNSIGNED,
)
CREATE TABLE IF NOT EXISTS ${database}.karavans (
	id BIGINT UNSIGNED AUTO_INCREMENT, PRIMARY KEY(id),
	chat_id BIGINT,
	catcher_id BIGINT UNSIGNED,
	start_mess_id BIGINT UNSIGNED,
	great_mess_id BIGINT UNSIGNED,
	delete_me_at BIGINT UNSIGNED,
	start_at BIGINT UNSIGNED,
	finish_at BIGINT UNSIGNED,
	coins BIGINT UNSIGNED,
	grabbed BOOL DEFAULT FALSE,
)
CREATE TABLE IF NOT EXISTS ${database}.weddings (
  id BIGINT UNSIGNED AUTO_INCREMENT, PRIMARY KEY(id),
  chat_id BIGINT,
  init_mess BIGINT UNSIGNED,
  bot_mess BIGINT UNSIGNED,
  fixed BOOL DEFAULT FALSE,
  init_time BIGINT UNSIGNED,
  fix_time BIGINT UNSIGNED,
  he_id BIGINT UNSIGNED,
  she_id BIGINT UNSIGNED,
  he_witness_id BIGINT UNSIGNED,
  she_witness_id BIGINT UNSIGNED,
  reg_id BIGINT UNSIGNED,
  init_id BIGINT UNSIGNED,
  guests_arr_id MEDIUMTEXT,
  expired_at BIGINT UNSIGNED default 0,
  expired_bool BOOL DEFAULT false,
  created_at BIGINT UNSIGNED,
  updated_at BIGINT UNSIGNED,
);
CREATE TABLE IF NOT EXISTS ${database}.karavan_timers (
  id BIGINT UNSIGNED AUTO_INCREMENT, PRIMARY KEY(id),
	text_timestamp MEDIUMTEXT,
  chat_id BIGINT,
  coins BIGINT UNSIGNED,
  done BOOL DEFAULT FALSE,
  created_at BIGINT UNSIGNED,
  updated_at BIGINT UNSIGNED,
  timestamp BIGINT UNSIGNED,
);
EOF
