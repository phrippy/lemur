#!/usr/bin/ruby
=begin
This is a sample of returned data by api
$wdata = { "date" => "2023-02-23",
           "day" => 365,
           "resource" =>
             "https://facebook.com/story.php?story_fbid=pfbid02foAFc9dtU47FLKWub3DLF5xytNv1MNBMNJZ3wjMV3mdo2wJoYqQ6GojVjSJtiBXxl&id=100069073844828&eav=AfbwL-jmksqj5yFLeybYZ5rqBT5-MmEqfuTrQqg1blNfdSW3jlfNXfRmxE-rtP_qnLM&m_entstream_source=timeline&paipv=0",
           "war_status" => { "code" => 1, "alias" => "in_progress" },
           "stats" =>
             { "personnel_units" => 145850,
               "tanks" => 3350,
               "armoured_fighting_vehicles" => 6593,
               "artillery_systems" => 2352,
               "mlrs" => 471,
               "aa_warfare_systems" => 244,
               "planes" => 299,
               "helicopters" => 287,
               "vehicles_fuel_tanks" => 5215,
               "warships_cutters" => 18,
               "cruise_missiles" => 873,
               "uav_systems" => 2029,
               "special_military_equip" => 228,
               "atgm_srbm_systems" => 4 },
           "increase" =>
             { "personnel_units" => 790,
               "tanks" => 16,
               "armoured_fighting_vehicles" => 24,
               "artillery_systems" => 7,
               "mlrs" => 0,
               "aa_warfare_systems" => 1,

               "planes" => 0,
               "helicopters" => 0,
               "vehicles_fuel_tanks" => 3,
               "warships_cutters" => 0,
               "cruise_missiles" => 0,
               "uav_systems" => 3,
               "special_military_equip" => 2,
               "atgm_srbm_systems" => 0 } }
=end

def warship_details_kb
  inline_kb = [[Telegram::Bot::Types::InlineKeyboardButton.new(text: "ℹ️Детальніше", callback_data: 'warship_details')],
               [Telegram::Bot::Types::InlineKeyboardButton.new(text: "#{symbol_delete}Закрити", callback_data: 'delete_this_message')]]
  kb = Telegram::Bot::Types::InlineKeyboardMarkup.new(inline_keyboard: inline_kb)
  return kb
end

def shoporusni
  if $warhip.is_a?(Hash) and Date.parse($warhip['data']['date']) == Date.today
    return true
  end
  url = 'https://russianwarship.rip/api/v2/statistics/latest'
  uri = URI(url)
  begin
    response = Net::HTTP.get_response(uri)
  rescue => e
    puts "Error: #{e.message}"
    return false
  end
  if response.is_a?(Net::HTTPSuccess)
    begin
      $warhip = JSON.parse(response.body)
    rescue => e
      STDERR.puts "Error: #{e.message}"
      return false
    end
  else
    STDERR.puts "HTTP request failed with error code #{response.code}"
    return false
  end
end

def get_warhip
  arr =
    [
      ["Дохлої русні⚰️", $warhip["data"]["stats"]["personnel_units"], $warhip["data"]["increase"]["personnel_units"]],
      ["Танків🛺", $warhip["data"]["stats"]["tanks"], $warhip["data"]["increase"]["tanks"]],
      ["ББМ🚐", $warhip["data"]["stats"]["armoured_fighting_vehicles"], $warhip["data"]["increase"]["armoured_fighting_vehicles"]],
      ["Арт. систем💥", $warhip["data"]["stats"]["artillery_systems"], $warhip["data"]["increase"]["artillery_systems"]],
      ["РСЗВ🚀💥", $warhip["data"]["stats"]["mlrs"], $warhip["data"]["increase"]["mlrs"]],
      ["Засобів ППО🔫", $warhip["data"]["stats"]["aa_warfare_systems"], $warhip["data"]["increase"]["aa_warfare_systems"]],
      ["Літаків✈️", $warhip["data"]["stats"]["planes"], $warhip["data"]["increase"]["planes"]],
      ["Гелікоптерів🚁", $warhip["data"]["stats"]["helicopters"], $warhip["data"]["increase"]["helicopters"]],
      ["Автотехніки та автоцистерн🚚", $warhip["data"]["stats"]["vehicles_fuel_tanks"], $warhip["data"]["increase"]["vehicles_fuel_tanks"]],
      ["Кораблів/катерів⛴️", $warhip["data"]["stats"]["warships_cutters"], $warhip["data"]["increase"]["warships_cutters"]],
      ["Крилатих ракет🚀🎯", $warhip["data"]["stats"]["cruise_missiles"], $warhip["data"]["increase"]["cruise_missiles"]],
      ["БПЛА🛩️", $warhip["data"]["stats"]["uav_systems"], $warhip["data"]["increase"]["uav_systems"]],
      ["Спец. техніки🚒", $warhip["data"]["stats"]["special_military_equip"], $warhip["data"]["increase"]["special_military_equip"]],
      ["Установок ОТРК/ТРК🚀🎯💥", $warhip["data"]["stats"]["atgm_srbm_systems"], $warhip["data"]["increase"]["atgm_srbm_systems"]]
    ]
  result = []
  arr.each do |item|
    text = []
    text.push("#{item[0]}:")
    text.push("<code>#{item[1]}</code>")
    unless item[2] == 0
      text.push("(+<code>#{item[2]}</code>)")
    end
    result.push(text.join(" "))
  end
  return result.join("\n")
end

def get_warship_yesterday
  $warhip["data"]["increase"]["personnel_units"]
end

def shoporusni_gentext_yesterday
  shoporusni
  text = <<-EOF
#{$warhip['data']['date']}, #{$warhip['data']['day']}-й день війни

#{get_warship_yesterday} русачків вчора стали хорошими
  EOF
end

def shoporusni_gentext
  # $warhip = { "data" => $wdata }
  shoporusni
  text = <<-EOF
<a href="#{$warhip['data']['resource']}">#{$warhip['data']['war_status']['code'] == 1 ? "Боротьба триває" : "Війна закінчена"}</a>
#{$warhip['data']['date']}, #{$warhip['data']['day']}-й день війни

#{get_warhip}
  EOF
end