#!/bin/bash
user='pi'
host='localhost'
password='secret'
database='lemur'
sudo mariadb << EOF
CREATE USER IF NOT EXISTS '${user}'@'${host}' IDENTIFIED BY 'password';
GRANT ALL PRIVILEGES ON ${database} . * TO '${user}'@'$host';
SET PASSWORD FOR '${user}'@'${host}' = PASSWORD('${password}');
FLUSH PRIVILEGES;
EOF

sudo mariadb << EOF
CREATE DATABASE IF NOT EXISTS lemur;
EOF

sudo mariadb << EOF
create table ${database}.users (id BIGINT UNSIGNED, PRIMARY KEY(id), username VARCHAR(32), first_name TEXT, last_name TEXT, pseudo TEXT, is_female BOOL, birthday DATE, coins BIGINT UNSIGNED DEFAULT 0, total_likes BIGINT UNSIGNED DEFAULT 0, total_dislikes BIGINT UNSIGNED DEFAULT 0, total_messages BIGINT UNSIGNED DEFAULT 0);
create table ${database}.chats (id BIGINT, PRIMARY KEY(id), title TEXT, username VARCHAR(32), pseudo TEXT, coins MEDIUMTEXT, show_coins BOOL DEFAULT true, likes MEDIUMTEXT, dislikes MEDIUMTEXT, show_likes BOOL DEFAULT true, messages MEDIUMTEXT, show_messages BOOL DEFAULT true, total_messages BIGINT UNSIGNED, expect_data MEDIUMTEXT, expect_users BIGINT, chat_likes BIGINT UNSIGNED, chat_dislikes BIGINT UNSIGNED, reactions MEDIUMTEXT, flood_phrases MEDIUMTEXT);
create table ${database}.bots (id BIGINT UNSIGNED, PRIMARY KEY(id), username VARCHAR(32), token TINYTEXT);
INSERT ${database}.bots(id, username, token) VALUES (580555461, '@zbagoyniybot', '580555461:AAHwQlrpRMSUU1c4vi39e0f2p5Pi2q5_eDM');
EOF
