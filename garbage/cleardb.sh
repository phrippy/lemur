#!/bin/bash
user='pi'
host='localhost'
database='lemur'
sudo mariadb << EOF
DROP USER IF EXISTS '${user}'@'${host}';
DROP DATABASE IF EXISTS ${database};
EOF
