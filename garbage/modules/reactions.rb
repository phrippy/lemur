#!/usr/bin/ruby
class ReactionError < StandardError
end
class EmptyTriggerError < ReactionError
end
class TriggerError < ReactionError
end
class AnswerError < ReactionError
end
class EmptyAnswerError < AnswerError
end
class DecodeAnswerError < AnswerError
end
class EncodeAnswerError < AnswerError
end
class Reaction
	def initialize (trigger = '', answer=[])
		@trigger = trigger
		@answer = answer
	end
	def trigger
		@trigger
	end
	def trigger= (text)
		@trigger = text
	end
	def answer
		@answer
	end
	def answer= array
		@answer = array
	end
	def add text
		@answer.push(text)
	end
	def decode text
		begin
		arr = JSON.parse(text)
		rescue
			raise DecodeAnswerError.new("Помилка структури JSON")
		end
		@trigger = arr.shift
		@answer = arr
	end
	def encode
		if @answer == []
			raise EmptyAnswerError.new("Відповідь реакції не може бути пустою")
		end
		answer = @answer
		unless @trigger.is_a?(Regexp)
			raise TriggerError.new("Триггер повинен бути регулярним виразом")
		end
		answer.unshift(@trigger)
		begin
		result = JSON.generate (answer)
		rescue
			raise EncodeAnswerError("Помилка при генеруванні JSON-структури")
		end
		result
	end
end
