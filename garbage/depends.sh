#!/bin/bash
sudo apt -y install ruby2.5-dev libmysql-ruby libmysqlclient-dev libfile-mimeinfo-perl mariadb-server mariadb-client libmariadb-dev
sudo gem install telegram-bot-ruby activerecord faraday pickup mysql2 emoji_regex faker unicode-emoji
#for raspberry pi:
sudo gem install activemodel:5.2.4.1 activesupport:5.2.4.1 activerecord:5.2.4.1
