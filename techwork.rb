#!/usr/bin/ruby
class EmptyMessageQueue < StandardError; end
class PleaseRetry < StandardError; end

class MessageQueue
  def initialize
    @data = Array.new
    @touch = Time.now.to_i
    @timeout = 3
    @runned = false
    upd
  end
  def upd
    @touch = Time.now.to_i
    puts Time.at(@touch)
  end
  def push(message)
    @data << message
    upd
  end
  def shift
    if @data.size == 0
      raise EmptyMessageQueue
    end
    upd
    result = @data.shift
    return result
  end
  def run(message)
    if @runned == true
      puts "Стаємо в чергу (#{message})"
      @data << message
    else
      @runned = true
      @data << message
      begin
        while @data.size > 0 do
          puts @data.to_s
          #puts @data.shift
          #puts "Чекаємо #{@timeout} сек…"
          @data.shift
          sleep  @timeout
          #TODO
          # Альо, це повинно працювати не так!
          # Після кожного @data.shift оновлюємо час останньої обробки.
          # Чекаємо (@touch + @timeout - Time.now) секунд.
          # Або якщо це число менше чи рівне нуля - оброблюємо наступний обʼєкт в черзі.
        end
        if @data.size > 0
          raise PleaseRetry
        end
      rescue PleaseRetry
        puts "Перезапуск!"
        retry
      ensure
        puts "Черга спустошена! (#{message})"
        @runned = false
      end
    end
  end
end
def main
  q = MessageQueue.new
  threads = []
  threads << Thread.new do
    q.run "AAA"
    sleep 3
    #TODO
    # В цьому потоці кидається виключення!!
    # Виключення треба оброблювати лише всередині потоку!
    # І якогось біса ми чекаємо, поки спустошиться черга, щоб почати нову
    # Треба розібратися, чому черга закриваєтсья.
    # Може поставити таймаут перед перевіркою на пустоту масиву для обробки🤔
  end
  1.upto(100).each do |i|
    #threads << Thread.new do
    q.run i
    #sleep 1.3
    #end
  end
  threads.each do |x|
    x.join
  end
  puts 'Program finished'
rescue EmptyMessageQueue
  puts "Queue is empty!!"
end

def sec_to_text(sec)
  if sec < 60
    return "#{sec} сек"
  end
  hours = sec / 3600
  minutes = sec % 3600 / 60
  secundes = sec % 60
  if minutes == 0 and secundes == 0
    return "#{hours} год"
  end
  if hours == 0 and secundes == 0
    return "#{minutes} хв"
  end
  arr = []
  arr.push(hours) if hours > 0
  arr.push(minutes) if minutes > 0
  arr.push(secundes)
  arr.map! do |x|
    if x < 10
      "0#{x}"
    else
      x.to_s
    end
  end
  arr.join(":")
end


puts sec_to_text(3660)

